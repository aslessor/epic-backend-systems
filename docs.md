<div>
    <h1>Using OAuth 2.0</h1>
    <p>
        Applications must secure and protect the privacy of patients and their data. To help meet this objective, Epic
        supports using the OAuth 2.0 framework to authenticate and authorize applications.
    </p>

    <h2>Why OAuth 2.0?</h2>
    <p>
        OAuth 2.0 enables you to develop your application without having to build a credential management system.
        Instead of exposing login credentials to your application, your application and an EHR's authorization server
        exchange a series of authorization codes and access tokens. Your application can access protected patient data
        stored in an EHR's database after it obtains authorization from the EHR's authorization server.
    </p>

    <h2>Before You Get Started</h2>

    <p>
        To use OAuth 2.0 to authorize your application's access to patient information, some information needs to be
        shared between the authorization server and your application:
    </p>
    <ol>
        <li><code>client_id</code>: The <code>client_id</code> identifies your application to authentication servers
            within the Epic community and allows you to connect to any organization.</li>
        <li><code>redirect_uri</code>: The <code>redirect_uri</code> confirms your identity and is used to validate and
            redirect authentication requests that originate from your application. Epic's implementation allows for
            multiple <code>redirect_uri</code>s. <ul>
                <li><strong>Note</strong> that a <code>redirect_uri</code> is not needed for backend services using the
                    client_credentials grant type.</li>
            </ul>
        </li>
        <li>Credentials: Some apps, sometimes referred to as confidential clients, can use credentials registered for a
            given EHR system to obtain authorization to access the system without a user or a patient implicitly or
            explicitly authorizing the app. Examples of this are apps that use refresh tokens to allow users to launch
            the app outside of an Epic client without needing to log in every time they use the app, and backend
            services that need to access resources without a specific person launching the app, for example, fetching
            data on a scheduled basis.</li>
    </ol>
    <p>
        You can register your application for access to both the sandbox and Epic organizations <a
            href="https://fhir.epic.com/Developer/Apps">here</a>. You'll provide information to us, including one or
        more <code>redirect_uri</code>s, and Epic will generate a <code>client_id</code> for you.
    </p>

    <p>
        Apps can be launched from within an existing EHR or patient portal session, which is called an EHR launch.
        Alternatively, apps can be launched standalone from outside of an existing EHR session. Apps can also be backend
        services where no user is launching the app.
    </p>

    <p>
        <strong>EHR launch (SMART on FHIR)</strong>: The app is launched by the EHR calling a launch URL specified in
        the EHR's configuration. The EHR launches the launch URL and appends a launch token and the FHIR server's
        endpoint URL (ISS parameter) in the query string. The app exchanges the launch token, along with the client
        identification parameters to get an authorization code and eventually the access token.
    </p>
    <div>
        <ul>
            <li>See the <a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;, &quot;EmbeddedOauth2Launch&quot;)"
                    tabindex="0">Embedded Launch section</a> of this guide for more details.</li>
        </ul>
    </div>

    <p>
        <strong>Standalone launch</strong>: The app launches directly to the authorize endpoint outside of an EHR
        session and requests context from the EHR's authorization server.
    </p>
    <div>
        <ul>
            <li>See the <a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;, &quot;standaloneOauth2Launch&quot;)"
                    tabindex="0">Standalone Launch section</a> of this guide for more details.</li>
        </ul>
    </div>

    <p>
        <strong>Backend services</strong>: The app is not authorized by a specific person and likely does not have a
        user interface, and therefore calls EHR web services with system-level authorization.
    </p>
    <div>
        <ul>
            <li>See the <a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;, &quot;BackendOAuth2Guide&quot;)"
                    tabindex="0">Backend Services section</a> of this guide for more details.</li>
        </ul>
    </div>

    <p>
        <strong>Desktop integrations through Subspace</strong>: The app requests access to APIs directly available on
        the EHR’s desktop application via a local HTTP server.
    </p>
    <div>
        <ul>
            <li>See the <a
                    href="https://open.epic.com/Content/specs/staged/Subspace%20Communication%20Framework%20Overview.pdf">Subspace
                    Communication Framework specification</a> for more details.</li>
        </ul>
    </div>

    <p>
        <strong>Considerations for native apps</strong>: Additional security considerations must be taken into account
        when launching a native app to sufficiently protect the <em>authorization code</em> from malicious apps running
        on the same device. There are a few options for how to implement this additional layer of security:
    </p>
    <ul>
        <li>Proof Key for Code Exchange (PKCE): This is a standardized, cross-platform technique for public clients to
            mitigate the threat of authorization code interception. However, it requires additional support from the
            authorization server and app. PKCE is described in <a href="https://tools.ietf.org/html/rfc7636">IETF RFC
                7636</a> and supported starting in the August 2019 version of Epic. Note that the Epic implementation of
            this standard uses the S256 code_challenge_method. The "plain" method is not supported.</li>
        <li>Platform-specific link association: Android, iOS, Windows, and other platforms have introduced a method by
            which a native app can claim an HTTPS redirect URL, demonstrate ownership of that URL, and instruct the OS
            to invoke the app when that specific HTTPS URL is navigated to. Apple's iOS calls this feature "<a
                href="https://developer.apple.com/library/archive/documentation/General/Conceptual/AppSearch/UniversalLinks.html">Universal
                Links</a>"; Android, "<a href="https://developer.android.com/training/app-links/">App Links</a>";
            Windows, "<a href="https://docs.microsoft.com/en-us/windows/uwp/launch-resume/web-to-app-linking">App URI
                Handlers</a>". This association of an HTTPS URL to native app is platform-specific and only available on
            some platforms.
        </li>
    </ul>

    <p>
        Starting in the August 2019 version of Epic, the Epic implementation of OAuth 2.0 supports PKCE and Epic
        recommends using PKCE for native mobile app integrations. For earlier versions or in cases where PKCE cannot be
        used, Epic recommends the use of Universal Links/App Links in lieu of custom redirect URL protocol schemes.
    </p>

    <br>
    <p>
        The app you build will list both a production Client ID and a non-production Client ID. While testing in the
        Epic on FHIR sandbox, use the <strong>non-production Client ID</strong>.
    </p>

    <p>The base URL for the Current Sandbox environment is:</p>
    <pre class="well"><code class="http">https://fhir.epic.com/interconnect-fhir-oauth/</code></pre>

    <h1 id="EmbeddedOauth2Launch" class="section-link"><span class="tutorial-section-header-text" tabindex="-1">EHR
            Launch (SMART on FHIR)</span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=EmbeddedOauth2Launch"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h1>
    <p>
        <strong>Contents</strong>
    </p>
    <ul>
        <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_Initial-Launch&quot;)"
                tabindex="0">Step 1: Your Application is Launched from the Patient Portal or EHR</a></li>
        <li>
            <a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_Conformance-Statement&quot;)"
                tabindex="0">Step 2: Your Application Retrieves the Conformance Statement or SMART Configuration</a>
            <ul>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_Conformance-Statement_Header-Requirements&quot;)"
                        tabindex="0">Additional Header Requirements</a></li>
            </ul>
        </li>
        <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_Request_Auth_Code&quot;)"
                tabindex="0">Step 3: Your Application Requests an Authorization Code</a></li>
        <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_Redirect&quot;)"
                tabindex="0">Step 4: EHR's Authorization Server Reviews The Request</a></li>
        <li>
            <a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_Access-Token-Request&quot;)"
                tabindex="0">Step 5: Your Application Exchanges the Authorization Code for an Access Token</a>
            <ul>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_Access-Token-Request_No_Refresh-Tokens&quot;)"
                        tabindex="0">If You Are Not Using a Client Secret</a></li>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_Access-Token-Request_With-Refresh-Tokens&quot;)"
                        tabindex="0">If You Are Using a Client Secret</a></li>
            </ul>
        </li>
        <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_ID-Tokens&quot;)"
                tabindex="0">OpenID Connect id_tokens</a></li>
        <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_Validating-OIDC-ID-Token&quot;)"
                tabindex="0">Validating the OpenID Connect JSON Web Token</a></li>
        <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_Using-Access-Token&quot;)"
                tabindex="0">Step 6: Your Application Uses FHIR APIs to Access Patient Data</a></li>
        <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_Using-Refresh-Token&quot;)"
                tabindex="0">Step 7: Use a Refresh Token to Obtain a New Access Token</a></li>
    </ul>
    <p></p>
    <h2>How It Works</h2>

    <h3 id="Embedded-Oauth2-Launch_Initial-Launch" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Step 1: <small>Your Application is Launched from the Patient Portal or EHR</small></span><a
            class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Embedded-Oauth2-Launch_Initial-Launch"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>
        Your app is launched by the EHR calling the launch URL which is specified in the EHR's configuration. The EHR
        sends a launch token and the FHIR server's endpoint URL (ISS parameter).
    </p>

    <ul>
        <li>
            <code>launch</code>: This parameter is an EHR generated token that signifies that an active EHR session
            already exists. This token is one-time use and will be exchanged for the authorization code.
            <ul>
                <li>Starting in the May 2020 Epic version, Epic community members can enable a feature that makes all
                    OAuth 2.0 tokens and codes, including launch codes, <a href="https://jwt.io/">JSON Web Tokens
                        (JWTs)</a> instead of opaque tokens. This change allows apps to inspect the tokens directly
                    without needing to call the Introspection endpoint, but it also increases the length of these tokens
                    significantly. This feature is enabled in the fhir.epic.com sandbox. Developers should ensure that
                    app URL handling does not truncate OAuth 2.0 tokens and codes. </li>
            </ul>
        </li>
        <li>
            <code>iss</code>: This parameter contains the EHR's FHIR endpoint URL, which an app can use to find the
            EHR's authorization server.
            <ul>
                <li>Your application should create an allowlist of iss values (AKA resource servers) to protect against
                    <a target="_blank" href="https://datatracker.ietf.org/doc/html/rfc6819#section-4.6.4">phishing
                        attacks</a>. Rather than accepting any iss value in your application, only accept those you know
                    belong to the organizations you integrate with.</li>
            </ul>
        </li>
    </ul>

    <h3 id="Embedded-Oauth2-Launch_Conformance-Statement" class="section-link"><span
            class="tutorial-section-header-text" tabindex="-1">Step 2: <small>Your Application Retrieves the Conformance
                Statement or SMART Configuration</small></span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Embedded-Oauth2-Launch_Conformance-Statement"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>
        To determine which authorize and token endpoints to use in the EHR launch flow, you should make a GET request to
        the metadata endpoint which is constructed by taking the iss provided and appending /metadata. Alternatively,
        when communicating with Epic organizations using the August 2021 version or Epic or later, you can make a GET
        request to the SMART configuration endpoint by taking the <code>iss</code> and appending
        /.well-known/smart-configuration.
    </p>

    <p>
        If no Accept header is sent, the metadata response is XML-formatted and the smart-configuration response is
        JSON-formatted. An Accept header can be sent with a value of application/json, application/xml, and the metadata
        endpoint additionally supports application/fhir+json and application/json+fhir to specify the format of the
        response.
    </p>
    <p><b>Metadata Example</b></p>
    <p>
        Here's an example of what a full metadata request might look like.
    </p>

    <pre class="well"><code class="http">GET https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/metadata HTTP/1.1
Accept: application/fhir+json</code></pre>

    <p>
        Here is an example of what the authorize and token endpoints would look like in the metadata response.
    </p>

    <pre class="well"><code class="http">"extension": [
{
"url": "http://fhir-registry.smarthealthit.org/StructureDefinition/oauth-uris",
"extension": [
  {
    "url": "authorize",
    "valueUri": "https://fhir.epic.com/interconnect-fhir-oauth/oauth2/authorize"
  },
  {
    "url": "token",
    "valueUri": "https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token"
  }
]
}
]
</code></pre>
    <p><b>Metadata Example with Dynamic Client Registration (starting in the November 2021 version of Epic)</b></p>
    <p>
        Here's an example of what a full metadata request might look like. By including an Epic Client ID, clients
        authorized to perform dynamic client registration can then determine which endpoint to use when performing
        dynamic client registration. Note that this capability is only supported for STU3 and R4 requests.
    </p>

    <pre class="well"><code class="http">GET https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/metadata HTTP/1.1
Accept: application/fhir+json
Epic-Client-ID: 0000-0000-0000-0000-0000</code></pre>

    <p>
        Here is an example of what the authorize, token, and dynamic registration endpoints would look like in the
        metadata response.
    </p>

    <pre class="well"><code class="http">"extension": [
{
"url": "http://fhir-registry.smarthealthit.org/StructureDefinition/oauth-uris",
"extension": [
  {
    "url": "authorize",
    "valueUri": "https://fhir.epic.com/interconnect-fhir-oauth/oauth2/authorize"
  },
  {
    "url": "token",
    "valueUri": "https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token"
  },
  {
    "url": "register",
    "valueUri": https://apporchard.epic.com/interconnect-aocurprd-oauth/oauth2/register
  }
]
}
]
</code></pre>

    <p>
        Here's an example of what a smart-configuration request might look like.
    </p>

    <pre class="well"><code class="http">GET https://apporchard.epic.com/interconnect-aocurprd-oauth/api/FHIR/R4/.well-known/smart-configuration HTTP/1.1
Accept: application/json</code></pre>

    <p>
        Here is an example of what the authorize and token endpoints would look like in the smart-configuration
        response.
    </p>

    <pre class="well"><code class="http">"authorization_endpoint": "https://apporchard.epic.com/interconnect-aocurprd-oauth/oauth2/authorize",
"token_endpoint": "https://apporchard.epic.com/interconnect-aocurprd-oauth/oauth2/token",
"token_endpoint_auth_methods_supported": [
    "client_secret_post",
    "client_secret_basic",
    "private_key_jwt"
]</code></pre>

    <h3 id="Embedded-Oauth2-Launch_Conformance-Statement_Header-Requirements" class="section-link"
        style="font-size: 18px;"><span class="tutorial-section-header-text" tabindex="-1">Retrieving Conformance
            Statement: Additional Header Requirements</span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Embedded-Oauth2-Launch_Conformance-Statement_Header-Requirements"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>
        In some cases when making your request to the metadata endpoint, you'll also need to include additional headers
        to ensure the correct token and authorize endpoints are returned. This is because in Epic, it is possible to
        configure the system such that the FHIR server's endpoint URL, provided in the iss parameter, is overridden on a
        per client ID basis. If you do not send the Epic-Client-ID HTTP header with the appropriate production or
        non-production client ID associated with your application in your call to the metadata endpoint, you can receive
        the wrong token and authorize endpoints. Additional CORS setup might be necessary on the Epic community member's
        Interconnect server to allow for this header.
    </p>
    <p>
        Starting in the November 2021 version of Epic, by including an Epic Client ID, clients authorized to perform
        dynamic client registration can then determine which endpoint to use when performing dynamic client
        registration. Note that this capability is supported for only STU3 and R4 requests.
    </p>
    <p>
        Here's an example of what a full metadata request might look like with the additional Epic-Client-ID header.
    </p>

    <pre class="well"><code class="http">GET https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/metadata HTTP/1.1
Accept: application/fhir+json
Epic-Client-ID: 0000-0000-0000-0000-0000
</code></pre>

    <h3 id="Embedded-Oauth2-Launch_Request_Auth_Code" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Step 3: <small>Your Application Requests an Authorization Code</small></span><a
            class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Embedded-Oauth2-Launch_Request_Auth_Code"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>
        Your application now has a <code>launch</code> token obtained from the initial EHR launch
        as well as the authorize endpoint obtained from the metadata query. To
        exchange the launch token for an authorization code, your app needs to either
        make an HTTP GET or POST request to the authorize endpoint that contains the
        parameters below. POST requests are supported for EHR launches in Epic version
        November 2020 and later, and are not currently supported for standalone
        launches.
    </p>
    <p>
        For POST requests, the parameters should be in the body of the request and
        should be sent as Content-Type "application/x-www-form-urlencoded". For GET
        requests, the parameters should be appended in the querystring of the request.
        It is Epic's recommendation to use HTTP POST for EHR launches to overcome any
        size limits on the request URL.
    </p>
    <p>
        In either case, the application should redirect the User-Agent (the user's
        browser) to the authorization server by either submitting an HTML Form to the
        authorization server (for HTTP POST) or by redirecting to a specified URL (for
        HTTP GET). The request should contain the following parameters.
    </p>

    <ul>
        <li><code>response_type</code>: This parameter must contain the value "code".</li>
        <li><code>client_id</code>: This parameter contains your web application's client ID issued by Epic. </li>
        <li><code>redirect_uri</code>: This parameter contains your application's redirect URI. After the request
            completes on the Epic server, this URI will be called as a callback. The value of this parameter needs to be
            URL encoded. This URI must also be registered with the EHR's authorization server by adding it to your app
            listing.</li>
        <li><code>scope</code>: This parameter describes the information for which the web application is requesting
            access. Starting with the Epic 2018 version, a scope of "launch" is required for EHR launch workflows.
            Starting with the November 2019 version of Epic, <a
                href="http://hl7.org/fhir/smart-app-launch/1.0.0/scopes-and-launch-context/index.html#scopes-for-requesting-identity-data"
                target="_blank">OpenID Connect scopes</a> are supported, specifically the "openid" scope is supported
            for all apps and the "fhirUser" scope is supported by apps with the R4 (or greater) SMART on FHIR version
            selected.</li>
        <li><code>launch</code>: This parameter is required for EHR launch workflows. The value to use will be passed
            from the EHR.</li>
        <li><code>aud</code>: Starting in the August 2021 version of Epic, health care organizations can optionally
            configure their system to require the aud parameter for EHR launch workflows if a launch context is included
            in the scope parameter. Starting in the November 2022 version of Epic, this parameter will be required. The
            value to use is the FHIR base URL of the resource server the application intends to access, which is
            typically the FHIR server returned by the iss.</li>
        <li><code>state</code>: This optional parameter is generated by your app and is opaque to the EHR. The EHR's
            authorization server will append it to each subsequent exchange in the workflow for you to validate session
            integrity. While not required, this parameter is recommended to be included and validated with each exchange
            in order to increase security. For more information see <a
                href="https://tools.ietf.org/html/rfc6819#section-3.6">RFC 6819 Section 3.6</a>.
            <ul>
                <li><strong>Note: </strong> Large <code>state</code> values in combination with launch tokens that are
                    JWTs (see above) may cause the query string for the HTTP GET request to the authorization endpoint
                    to exceed the Epic community member web server's max query string length and cause the request to
                    fail. You can mitigate this risk by: <ul>
                        <li>Following the <a
                                href="http://hl7.org/fhir/smart-app-launch/1.0.0/#step-1-app-asks-for-authorization">SMART
                                App Launch Framework's</a> recommendation that the <code>state</code> parameter be <q>an
                                unpredictable value ... with at least 122 bits of entropy (e.g., a properly configured
                                random uuid is suitable)</q>, and not store any actual application state in the
                            <code>state</code> parameter.</li>
                        <li>Using a POST request. If you use this approach, note that you might still need to account
                            for a large state value in the HTTP GET redirect back to your own server.</li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>

    <p>
        Additional parameters for native mobile apps (available starting in the August 2019 version of Epic):
    </p>

    <ul>
        <li><code>code_challenge</code>: This optional parameter is generated by your app and used for PKCE. This is the
            S256 hashed version of the code_verifier parameter, which will be used in the token request.</li>
        <li><code>code_challenge_method</code>: This optional parameter indicates the method used for the code_challenge
            parameter and is required if using that parameter. Currently, only the S256 method is supported.</li>
    </ul>

    <p>
        Here's an example of an authorization request using HTTP GET. You will replace the
        <strong>[redirect_uri]</strong>, <strong>[client_id]</strong>, <strong>[launch_token]</strong>,
        <strong>[state]</strong>, <strong>[code_challenge]</strong>, and <strong>[audience]</strong> placeholders with
        your own values.
    </p>

    <pre
        class="well"><code class="http">https://fhir.epic.com/interconnect-fhir-oauth/oauth2/authorize?scope=launch&amp;response_type=code&amp;redirect_uri=[redirect_uri]&amp;client_id=[client_id]&amp;launch=[launch_token]&amp;state=[state]&amp;code_challenge=[code_challenge]&amp;code_challenge_method=S256&amp;aud=[audience]</code></pre>

    <p>
        This is an example HTTP GET from the SMART on FHIR launchpad:
    </p>

    <pre
        class="well"><code class="http">https://fhir.epic.com/interconnect-fhir-oauth/oauth2/authorize?scope=launch&amp;response_type=code&amp;redirect_uri=https%3A%2F%2Ffhir.epic.com%2Ftest%2Fsmart&amp;client_id=d45049c3-3441-40ef-ab4d-b9cd86a17225&amp;launch=GwWCqm4CCTxJaqJiROISsEsOqN7xrdixqTl2uWZhYxMAS7QbFEbtKgi7AN96fKc2kDYfaFrLi8LQivMkD0BY-942hYgGO0_6DfewP2iwH_pe6tR_-fRfiJ2WB_-1uwG0&amp;state=abc123&amp;aud=https://fhir.epic.com/interconnect-fhir-oauth/api/fhir/dstu2</code></pre>

    <p>
        And here's an example of what the same request would look like as an HTTP Post:
    </p>

    <p>
    </p>
    <pre
        class="well"><code class="http">POST https://fhir.epic.com/interconnect-fhir-oauth/oauth2/authorize HTTP/1.1
Content-Type: application/x-www-form-urlencoded

scope=launch&amp;response_type=code&amp;redirect_uri=https%3A%2F%2Ffhir.epic.com%2Ftest%2Fsmart&amp;client_id=d45049c3-3441-40ef-ab4d-b9cd86a17225&amp;launch=GwWCqm4CCTxJaqJiROISsEsOqN7xrdixqTl2uWZhYxMAS7QbFEbtKgi7AN96fKc2kDYfaFrLi8LQivMkD0BY-942hYgGO0_6DfewP2iwH_pe6tR_-fRfiJ2WB_-1uwG0&amp;state=abc123&amp;aud=https%3A%2F%2Ffhir.epic.com%2Finterconnect-fhir-oauth%2Fapi%2Ffhir%2Fdstu2 </code></pre>
    <p></p>

    <h3 id="Embedded-Oauth2-Launch_Redirect" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Step 4: <small>EHR's Authorization Server Reviews the Request</small></span><a
            class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Embedded-Oauth2-Launch_Redirect"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>
        The EHR's authorization server reviews the request from your application. If approved, the authorization server
        redirects the browser to the redirect URL supplied in the initial request and appends the following querystring
        parameter.
    </p>

    <ul>
        <li>
            <code>code</code>: This parameter contains the authorization code generated by Epic, which will be exchanged
            for the access token in the next step.
            <ul>
                <li>Starting in the May 2020 Epic version, Epic community members can enable a feature that makes all
                    OAuth 2.0 tokens and codes, including authorization codes, <a href="https://jwt.io/">JSON Web Tokens
                        (JWTs)</a> instead of opaque tokens. This change allows apps to inspect the tokens directly
                    without needing to call the Introspection endpoint, but it also increases the length of these tokens
                    significantly. This feature is enabled in the fhir.epic.com sandbox. Developers should ensure that
                    app URL handling does not truncate OAuth 2.0 tokens and codes. </li>
            </ul>
        </li>
        <li>
            <code>state</code>: This parameter will have the same value as the earlier state parameter. For more
            information, refer to Step 3.
        </li>
    </ul>

    <p>
        Here's an example of what the redirect will look like if Epic's authorization server accepts the request:
    </p>

    <pre
        class="well"><code class="http">https://fhir.epic.com/test/smart?code=yfNg-rSc1t5O2p6jVAZLyY00uOOte5KM1y3YUxqsJQnBKEMNsYqOPTyVqcCH3YXaPkLztO9Rvf7bhLqQTwALHcHN6raxpTbR1eVgV2QyLA_4K0HrJO92et3qRXiXPkj7&amp;state=abc123</code></pre>

    <h3 id="Embedded-Oauth2-Launch_Access-Token-Request" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Step 5: <small>Your Application Exchanges the Authorization Code for an Access
                Token</small></span><a class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Embedded-Oauth2-Launch_Access-Token-Request"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>
        After receiving the authorization code, your application trades the code for a JSON object containing an access
        token and contextual information by sending an HTTP POST to the token endpoint using a Content-Type header with
        value of "application/x-www-form-urlencoded". For more information, see <a
            href="https://tools.ietf.org/html/rfc6749#section-4.1.3">RFC 6749 section 4.1.3</a>.
    </p>

    <h3 id="Embedded-Oauth2-Launch_Access-Token-Request_No_Refresh-Tokens" class="section-link"
        style="font-size: 18px;"><span class="tutorial-section-header-text" tabindex="-1">Access Token Request: If You
            Are Not Using a Client Secret</span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Embedded-Oauth2-Launch_Access-Token-Request_No_Refresh-Tokens"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>
        The following parameters are required in the POST body:
    </p>

    <ul>
        <li>
            <code>grant_type</code>: For the EHR launch flow, this should contain the value "authorization_code".
        </li>
        <li>
            <code>code</code>: This parameter contains the authorization code sent from Epic's authorization server to
            your application as a querystring parameter on the redirect URI as described above.
        </li>
        <li>
            <code>redirect_uri</code>: This parameter must contain the same redirect URI that you provided in the
            initial access request. The value of this parameter needs to be URL encoded.
        </li>
        <li>
            <code>client_id</code>: This parameter must contain the application's client ID issued by Epic that you
            provided in the initial request.
        </li>
        <li>
            <code>code_verifier</code>: This optional parameter is used to verify against your code_challenge parameter
            when using PKCE. This parameter is passed as free text and must match the code_challenge parameter used in
            your authorization request once it is hashed on the server using the code_challenge_method. This parameter
            is available starting in the August 2019 version of Epic.
        </li>
    </ul>

    <p>
        Here's an example of what an HTTP POST request for an access token might look like:
    </p>

    <pre
        class="well"><code class="http">POST https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token HTTP/1.1
Content-Type: application/x-www-form-urlencoded

grant_type=authorization_code&amp;code=yfNg-rSc1t5O2p6jVAZLyY00uOOte5KM1y3YUxqsJQnBKEMNsYqOPTyVqcCH3YXaPkLztO9Rvf7bhLqQTwALHcHN6raxpTbR1eVgV2QyLA_4K0HrJO92et3qRXiXPkj7&amp;redirect_uri=https%3A%2F%2Ffhir.epic.com%2Ftest%2Fsmart&amp;client_id=d45049c3-3441-40ef-ab4d-b9cd86a17225 </code></pre>

    <p>
        The authorization server responds to the HTTP POST request with a JSON object that includes an access token. The
        response contains the following fields:
    </p>

    <ul>
        <li>
            <code>access_token</code>: This parameter contains the access token issued by Epic to your application and
            is used in future requests.
            <ul>
                <li>Starting in the May 2020 Epic version, Epic community members can enable a feature that makes all
                    OAuth 2.0 tokens and codes, including access tokens, <a href="https://jwt.io/">JSON Web Tokens
                        (JWTs)</a> instead of opaque tokens. This change allows apps to inspect the tokens directly
                    without needing to call the Introspection endpoint, but it also increases the length of these tokens
                    significantly. This feature is enabled in the fhir.epic.com sandbox. Developers should ensure that
                    app URL handling does not truncate OAuth 2.0 tokens and codes. </li>
            </ul>

        </li>
        <li>
            <code>token_type</code>: In Epic's OAuth 2.0 implementation, this parameter always includes the value
            <code>bearer</code>.
        </li>
        <li>
            <code>expires_in</code>: This parameter contains the number of seconds for which the access token is valid.
        </li>
        <li>
            <code>scope</code>: This parameter describes the access your application is authorized for.
        </li>
        <li>
            <code>id_token</code>: Returned only for applications that have requested an <em>openid</em> scope. See
            below for more info on <a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_ID-Tokens&quot;)"
                tabindex="0">OpenID Connect id_tokens</a>. This parameter follows the guidelines of the <a
                href="https://openid.net/specs/openid-connect-core-1_0.html#IDToken" target="_blank">OpenID Connect
                (OIDC) Core 1.0 specification</a>. It is signed but not encrypted.
        </li>
        <li>
            <code>patient</code>: This parameter identifies provides the FHIR ID for the patient, if a patient is in
            context at time of launch.
        </li>
        <li>
            <code>epic.dstu2.patient</code>: This parameter identifies the DSTU2 FHIR ID for the patient, if a patient
            is in context at time of launch.
        </li>
        <li>
            <code>encounter</code>: This parameter identifies the FHIR ID for the patient’s encounter, if in context at
            time of launch. The encounter token corresponds to the FHIR Encounter resource.
        </li>
        <li>
            <code>location </code>: This parameter identifies the FHIR ID for the encounter department, if in context at
            time of launch. The location token corresponds to the FHIR Location resource.
        </li>
        <li>
            <code>appointment</code>: This parameter identifies the FHIR ID for the patient’s appointment, if
            appointment context is available at time of launch. The appointment token corresponds to the FHIR
            Appointment resource.
        </li>
        <li>
            <code>loginDepartment</code>: This parameter identifies the FHIR ID of the user's login department for
            launches from Hyperspace. The loginDepartment token corresponds to the FHIR Location resource.
        </li>
        <li>
            <code>state</code>: This parameter will have the same value as the earlier state parameter. For more
            information, refer to Step 3.
        </li>
    </ul>

    <p>
        Note that you can include additional fields in the response if needed based on the integration configuration.
        For more information, refer to the <a style="cursor: pointer"
            onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
            onclick="Epic$USCDI$Dev$ShowDocument(&quot;Launching&quot;)" tabindex="0">Launching your app</a> topic.
        Here's an example of what a JSON object including an access token might look like:
    </p>

    <pre class="well"><code class="http">{
"access_token": "Nxfve4q3H9TKs5F5vf6kRYAZqzK7j9LHvrg1Bw7fU_07_FdV9aRzLCI1GxOn20LuO2Ahl5RkRnz-p8u1MeYWqA85T8s4Ce3LcgQqIwsTkI7wezBsMduPw_xkVtLzLU2O",
"token_type": "bearer",
"expires_in": 3240,
"scope": "openid Patient.read Patient.search ",
"id_token": "eyJhbGciOiJSUzI1NiIsImtpZCI6IktleUlEIiwidHlwIjoiSldUIn0.eyJhdWQiOiJDbGllbnRJRCIsImV4cCI6RXhwaXJlc0F0LCJpYXQiOklzc3VlZEF0LCJpc3MiOiJJc3N1ZXJVUkwiLCJzdWIiOiJFbmRVc2VySWRlbnRpZmllciJ9",
"__epic.dstu2.patient": "T1wI5bk8n1YVgvWk9D05BmRV0Pi3ECImNSK8DKyKltsMB",
"patient": "T1wI5bk8n1YVgvWk9D05BmRV0Pi3ECImNSK8DKyKltsMB",
"appointment": "eVa-Ad1SCIenVq8CYQVxfKwGtP-DfG3nIy9-5oPwTg2g3",
"encounter": "eySnzekbpf6uGZz87ndPuRQ3",
"location": "e4W4rmGe9QzuGm2Dy4NBqVc0KDe6yGld6HW95UuN-Qd03",
"loginDepartment": "e4W4rmGe9QzuGm2Dy4NBqVc0KDe6yGld6HW95UuN-Qd03",
"state": "abc123",
}
</code></pre>

    <p>
        At this point, authorization is complete and the web application can access the protected patient data it
        requests using FHIR APIs.
    </p>

    <h3 id="Embedded-Oauth2-Launch_Access-Token-Request_With-Refresh-Tokens" class="section-link"
        style="font-size: 18px;"><span class="tutorial-section-header-text" tabindex="-1">Access Token Request: If You
            Are Using a Client Secret</span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Embedded-Oauth2-Launch_Access-Token-Request_With-Refresh-Tokens"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>
    <p>Refresh tokens are not typically needed for embedded (SMART on FHIR) launches because users are not required to
        log in to Epic during the SMART on FHIR launch process, and the access token obtained from the launch process is
        typically valid for longer than the user needs to use the app.</p>
    <p>Consult the <a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
            onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Standalone-Oauth2-Launch_Access-Token-Request_With-Refresh-Tokens&quot;)"
            tabindex="0">Standalone Launch: Access Token Request for Refresh Token Apps</a> for details on how to obtain
        an access token if your app uses refresh tokens.</p>

    <h3 id="Embedded-Oauth2-Launch_ID-Tokens" class="section-link" style="font-size: 18px;"><span
            class="tutorial-section-header-text" tabindex="-1">OpenID Connect id_tokens</span><a
            class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Embedded-Oauth2-Launch_ID-Tokens"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>
    <p>Epic started supporting the OpenID Connect id_token in the access token response for apps that request the
        <em>openid</em> scope since the November 2019 version of Epic.</p>
    <p>A decoded OpenID Connect id_token JWT will have these headers:</p>

    <table class="table table-hover">
        <thead>
            <tr>
                <th id="jwt-header-val">Header</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="white-space: nowrap;"><code>alg</code></td>
                <td>The JWT authentication algorithm. Currently only RSA 256 is supported in id_token JWTs so this will
                    always be <code>RS256</code>.</td>
            </tr>
            <tr>
                <td style="white-space: nowrap;"><code>typ</code></td>
                <td>This is always set to <code>JWT</code>.</td>
            </tr>
            <tr>
                <td style="white-space: nowrap;"><code>kid</code></td>
                <td>The base64 encoded SHA-256 hash of the public key.</td>
            </tr>
        </tbody>
    </table>

    <p>A decoded OpenID Connect id_token JWT will have this payload:</p>
    <table class="table table-hover">
        <thead>
            <tr>
                <th id="jwt-claims-claim">Claim</th>
                <th id="jwt-claims-description">Description</th>
                <th id="jwt-claims-remarks">Remarks</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="white-space: nowrap;"><code>iss</code></td>
                <td>
                    Issuer of the JWT. This is set to the token endpoint that should be used by the client.
                </td>
                <td>Starting in the May 2020 version of Epic, the <code>iss</code> will be set to the OAuth 2.0 server
                    endpoint, e.g. <strong>https://&lt;Interconnect Server URL&gt;/oauth2</strong>. For Epic versions
                    prior to May 2020, it is set to the OAuth 2.0 token endpoint, e.g. <strong>https://&lt;Interconnect
                        Server URL&gt;/oauth2/token</strong>.</td>
            </tr>
            <tr>
                <td style="white-space: nowrap;"><code>sub</code></td>
                <td>
                    STU3+ FHIR ID for the resource representing the user launching the app.
                </td>
                <td>For Epic integrations, the <code>sub</code> and <code>fhirUser</code> claims reference one of the
                    following resources depending on the type of workflow:
                    <ul>
                        <li>
                            Provider/user workflows: Practitioner resource
                        </li>
                        <li>
                            MyChart self access workflows: Patient resource
                        </li>
                        <li>
                            MyChart proxy access workflows: RelatedPerson resource
                        </li>
                    </ul>
                    <em><strong>Note</strong> apps with a SMART on FHIR version of DSTU2 will still get a STU3+ FHIR
                        FHIR ID for the user. The STU3+ Practitioner.Read or RelatedPerson.Read APIs would be required
                        to read the Practitioner or RelatedPerson FHIR IDs. Patient FHIR IDs can be used across FHIR
                        versions in Epic's implementation.</em>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;"><code>fhirUser</code></td>
                <td>
                    Absolute reference to the FHIR resource representing the user launching the app. See the <a
                        href="http://hl7.org/fhir/smart-app-launch/1.0.0/scopes-and-launch-context/index.html#scopes-for-requesting-identity-data"
                        target="_blank">HL7 documentation</a> for more details.
                </td>
                <td>The <em>fhirUser</em> claim will only be present if app has the R4 (or greater) SMART on FHIR
                    version selected, and requests both the <em>openid</em> and <em>fhirUser</em> scopes in the request
                    to the authorize endpoint. See the remark for the <code>sub</code> claim above for more information
                    about what the resource returned in these claims represents.</td>
            </tr>
            <tr>
                <td style="white-space: nowrap;"><code>aud</code></td>
                <td>
                    Audiences that the ID token is intended for. This will be set to the client ID for the application
                    that was just authorized during the SMART on FHIR launch.
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="white-space: nowrap;"><code>iat</code></td>
                <td>
                    Time integer for when the JWT was created, expressed in seconds since the "Epoch"
                    (1970-01-01T00:00:00Z UTC).
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="white-space: nowrap;"><code>exp</code></td>
                <td>
                    Expiration time integer for this authentication JWT, expressed in seconds since the "Epoch"
                    (1970-01-01T00:00:00Z UTC).
                </td>
                <td>This is set to the current time plus 5 minutes.</td>
            </tr>
        </tbody>
    </table>

    <p>
        Here is an example decoded <code>id_token</code> that could be returned if the app has the R4 (or greater) SMART
        on FHIR version selected, <strong>and</strong> requests both the <em>openid</em> and <em>fhirUser</em> scopes in
        the request to the authorize endpoint:
    </p>
    <pre>{
"alg": "RS256",
"kid": "liCulTIaitUzjfUh2AqNiMro47X9HcVcd9XPi8LDJKA=",
"typ": "JWT"
}
{
"aud": "de5dae1a-4317-4c25-86f1-ed558e85529b",
"exp": 1595956317,
"fhirUser": "https://fhir.epic.com/interconnect-fhir-oauth/oauth2/api/FHIR/R4/Practitioner/exfo6E4EXjWsnhA1OGVElgw3",
"iat": 1595956017,
"iss": "https://fhir.epic.com/interconnect-fhir-oauth/oauth2",
"sub": "exfo6E4EXjWsnhA1OGVElgw3"
}</pre>

    <p>
        Here is an example decoded <code>id_token</code> that could be returned if the app requests at least the
        <em>openid</em> scope in the request to the authorize endpoint, but either doesn't request the <em>fhirUser</em>
        claim or doesn't meet the criteria outlined above for receiving the <em>fhirUser</em> claim:
    </p>
    <pre>{
"alg": "RS256",
"kid": "liCulTIaitUzjfUh2AqNiMro47X9HcVcd9XPi8LDJKA=",
"typ": "JWT"
}
{
"aud": "de5dae1a-4317-4c25-86f1-ed558e85529b",
"exp": 1595956317,
"iat": 1595956017,
"iss": "https://fhir.epic.com/interconnect-fhir-oauth/oauth2",
"sub": "exfo6E4EXjWsnhA1OGVElgw3"
}</pre>

    <p></p>

    <h3 id="Embedded-Oauth2-Launch_Validating-OIDC-ID-Token" class="section-link" style="font-size: 18px;"><span
            class="tutorial-section-header-text" tabindex="-1">Validating the OpenID Connect JSON Web Token</span><a
            class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Embedded-Oauth2-Launch_Validating-OIDC-ID-Token"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>
        When completing the OAuth 2.0 authorization workflow with the openid scope, the JSON Web Token (JWT) returned in
        the id_token field will be cryptographically signed. The public key to verify the authenticity of the JWT can be
        found at <strong>https://&lt;Interconnect Server
            URL&gt;/api/epic/2019/Security/Open/PublicKeys/530005/OIDC</strong>, for example <a
            href="https://fhir.epic.com/interconnect-fhir-oauth/api/epic/2019/Security/Open/PublicKeys/530005/OIDC">https://fhir.epic.com/interconnect-fhir-oauth/api/epic/2019/Security/Open/PublicKeys/530005/OIDC</a>.
    </p>
    <p>
        Starting in the May 2020 version of Epic, metadata about the server's OpenID Connect configuration, including
        the <code>jwks_uri</code> OIDC public key endpoint, can be found at the OpenID configuration endpoint
        <strong>https://&lt;Interconnect Server URL&gt;/oauth2/.well-known/openid-configuration</strong>, for example <a
            href="https://fhir.epic.com/interconnect-fhir-oauth/oauth2/.well-known/openid-configuration">https://fhir.epic.com/interconnect-fhir-oauth/oauth2/.well-known/openid-configuration</a>.
    </p>
    <p>
        Starting in the August 2021 version of Epic, metadata about the server's OpenID Connect configuration can also
        be found at the FHIR endpoint https://&lt;Interconnect Server
        URL&gt;/api/FHIR/R4/.well-known/openid-configuration, for example
        https://apporchard.epic.com/interconnect-aocurprd-oauth/api/FHIR/R4/.well-known/openid-configuration.
    </p>

    <h3 id="Embedded-Oauth2-Launch_Using-Access-Token" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Step 6: <small>Your Application Uses FHIR APIs to Access Patient Data</small></span><a
            class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Embedded-Oauth2-Launch_Using-Access-Token"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>
        With a valid access token, your application can now access protected patient data from the EHR database using
        FHIR APIs. Queries must contain an Authorization header that includes the access token presented as a Bearer
        token.
    </p>

    <p>
        Here's an example of what a valid query looks like:
    </p>

    <pre
        class="well"><code class="http">GET https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Patient/T1wI5bk8n1YVgvWk9D05BmRV0Pi3ECImNSK8DKyKltsMB HTTP/1.1
Authorization: Bearer Nxfve4q3H9TKs5F5vf6kRYAZqzK7j9LHvrg1Bw7fU_07_FdV9aRzLCI1GxOn20LuO2Ahl5RkRnz-p8u1MeYWqA85T8s4Ce3LcgQqIwsTkI7wezBsMduPw_xkVtLzLU2O</code></pre>

    <h3 id="Embedded-Oauth2-Launch_Using-Refresh-Token" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Step 7: <small>Use a Refresh Token to Obtain a New Access Token</small></span><a
            class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Embedded-Oauth2-Launch_Using-Refresh-Token"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>
    <p>Refresh tokens are not typically needed for embedded (SMART on FHIR) launches because users are not required to
        log in to Epic during the SMART on FHIR launch process, and the access token obtained from the launch process is
        typically valid for longer than the user needs to use the app.</p>
    <p>Consult the <a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
            onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Standalone-Oauth2-Launch_Using-Refresh-Token&quot;)"
            tabindex="0">Standalone Launch: Use a Refresh Token to Obtain a New Access Token</a> for details on how to
        use a refresh token to get a new an access token if your app uses refresh tokens.</p>

    <h1 id="standaloneOauth2Launch" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Standalone Launch</span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=standaloneOauth2Launch"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h1>
    <p>
        <strong>Contents</strong>
    </p>
    <ul>
        <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Standalone-Oauth2-Launch_Request_Auth_Code&quot;)"
                tabindex="0"> Step 1: Your Application Requests an Authorization Code</a></li>
        <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Standalone-Oauth2-Launch_Authenticate-User&quot;)"
                tabindex="0"> Step 2: EHR's Authorization Server Authenticates the User and Authorizes Access</a></li>
        <li>
            <a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Standalone-Oauth2-Launch_Access-Token-Request&quot;)"
                tabindex="0"> Step 3: Your Application Exchanges the Authorization Code for an Access Token </a>
            <ul>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Standalone-Oauth2-Launch_Access-Token-Request_No-Refresh-Tokens&quot;)"
                        tabindex="0">If You Are Not Using a Client Secret</a></li>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Standalone-Oauth2-Launch_Access-Token-Request_With-Refresh-Tokens&quot;)"
                        tabindex="0">If You Are Using a Client Secret</a></li>
            </ul>
        </li>
        <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Standalone-Oauth2-Launch_Using-Access-Token&quot;)"
                tabindex="0"> Step 4: Your Application Uses FHIR APIs to Access Patient Data</a></li>
        <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Standalone-Oauth2-Launch_Using-Refresh-Token&quot;)"
                tabindex="0"> Step 5: Use a Refresh Token to Obtain a New Access Token</a></li>
    </ul>
    <p></p>
    <h2>How It Works</h2>

    <h3 id="Standalone-Oauth2-Launch_Request_Auth_Code" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Step 1: <small>Your Application Requests an Authorization Code</small></span><a
            class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Standalone-Oauth2-Launch_Request_Auth_Code"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>
        Your application would like to authenticate the user using the OAuth 2.0 workflow. To initiate this process,
        your app needs to link (using HTTP GET) to the authorize endpoint and append the following querystring
        parameters:
    </p>

    <ul>
        <li><code>response_type</code>: This parameter must contain the value "code".</li>
        <li><code>client_id</code>: This parameter contains your web application's client ID issued by Epic. </li>
        <li><code>redirect_uri</code>: This parameter contains your application's redirect URI. After the request
            completes on the Epic server, this URI will be called as a callback. The value of this parameter needs to be
            URL encoded. This URI must also be registered with the EHR's authorization server by adding it to your app
            listing.</li>
        <li><code>state</code>: This optional parameter is generated by your app and is opaque to the EHR. The EHR's
            authorization server will append it to each subsequent exchange in the workflow for you to validate session
            integrity. While not required, this parameter is recommended to be included and validated with each exchange
            in order to increase security. For more information see <a
                href="https://tools.ietf.org/html/rfc6819#section-3.6">RFC 6819 Section 3.6</a>.</li>
        <li><code>scope</code>: This parameter describes the information for which the web application is requesting
            access. Starting with the November 2019 version of Epic, the "openid" and "fhirUser" <a
                href="http://hl7.org/fhir/smart-app-launch/1.0.0/scopes-and-launch-context/index.html#scopes-for-requesting-identity-data"
                target="_blank">OpenID Connect scopes</a> are supported.</li>
        <li><code>aud</code>: Starting in the August 2021 version of Epic, health care organizations can optionally
            configure their system to require the aud parameter for Standalone EHR launch workflows if a launch context
            is included in the scope parameter. Starting in the November 2022 version of Epic, this parameter will be
            required. The value to use is the base URL of the resource server the application intends to access, which
            is typically the FHIR server.</li>
    </ul>

    <p>
        Additional parameters for native mobile apps (available starting in the August 2019 version of Epic):
    </p>

    <ul>
        <li><code>code_challenge</code>: This optional parameter is generated by your app and used for PKCE. This is the
            S256 hashed version of the code_verifier parameter, which will be used in the token request.</li>
        <li><code>code_challenge_method</code>: This optional parameter indicates the method used for the code_challenge
            parameter and is required if using that parameter. Currently, only the S256 method is supported.</li>
    </ul>

    <p>Here's an example of an authorization request using HTTP GET. You will replace the
        <strong>[redirect_uri]</strong>, <strong>[client_id]</strong>, <strong>[state]</strong>, and
        <strong>[audience]</strong> placeholders with your own values.</p>
    <pre
        class="well"><code class="http">https://fhir.epic.com/interconnect-fhir-oauth/oauth2/authorize?response_type=code&amp;redirect_uri=[redirect_uri]&amp;client_id=[client_id]&amp;state=[state]&amp;aud=[audience]</code></pre>

    <p>This is an example link:</p>
    <pre
        class="well"><code class="http">https://fhir.epic.com/interconnect-fhir-oauth/oauth2/authorize?response_type=code&amp;redirect_uri=https%3A%2F%2Ffhir.epic.com%2Ftest%2Fsmart&amp;client_id=d45049c3-3441-40ef-ab4d-b9cd86a17225&amp;state=abc123&amp;aud=https://fhir.epic.com/interconnect-fhir-oauth/api/fhir/dstu2</code></pre>

    <h3 id="Standalone-Oauth2-Launch_Authenticate-User" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Step 2: <small>EHR's Authorization Server Authenticates the User and Authorizes
                Access</small></span><a class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Standalone-Oauth2-Launch_Authenticate-User"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>
        The EHR's authorization server reviews the request from your application, authenticates the user <a
            href="https://fhir.epic.com/Documentation?docId=testpatients">(sample credentials found here)</a>, and
        authorizes access If approved, the authorization server redirects the browser to the redirect URL supplied in
        the initial request and appends the following querystring parameter.
    </p>

    <ul>
        <li><code>code</code>: This parameter contains the authorization code generated by Epic, which will be exchanged
            for the access token in the next step.</li>
        <ul>
            <li>Starting in the May 2020 Epic version, Epic community members can enable a feature that makes all OAuth
                2.0 tokens and codes, including authorization codes, <a href="https://jwt.io/">JSON Web Tokens
                    (JWTs)</a> instead of opaque tokens. This change allows apps to inspect the tokens directly without
                needing to call the Introspection endpoint, but it also increases the length of these tokens
                significantly. This feature is enabled in the fhir.epic.com sandbox. Developers should ensure that app
                URL handling does not truncate OAuth 2.0 tokens and codes. </li>
        </ul>
        <li><code>state</code>: This parameter will have the same value as the earlier state parameter. For more
            information, refer to Step 1.</li>
    </ul>

    <p>
        Here's an example of what the redirect will look like if Epic's authorization server accepts the request:
    </p>

    <pre
        class="well"><code class="http">https://fhir.epic.com/test/smart?code=yfNg-rSc1t5O2p6jVAZLyY00uOOte5KM1y3YUxqsJQnBKEMNsYqOPTyVqcCH3YXaPkLztO9Rvf7bhLqQTwALHcHN6raxpTbR1eVgV2QyLA_4K0HrJO92et3qRXiXPkj7&amp;state=abc123</code></pre>

    <h3 id="Standalone-Oauth2-Launch_Access-Token-Request" class="section-link"><span
            class="tutorial-section-header-text" tabindex="-1">Step 3: <small>Your Application Exchanges the
                Authorization Code for an Access Token</small></span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Standalone-Oauth2-Launch_Access-Token-Request"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>After receiving the authorization code, your application trades the code for a JSON object containing an access
        token and contextual information by sending an HTTP POST to the token endpoint using a Content-Type header with
        value of "application/x-www-form-urlencoded". For more information, see <a
            href="https://tools.ietf.org/html/rfc6749#section-4.1.3">RFC 6749 section 4.1.3</a>.</p>

    <h3 id="Standalone-Oauth2-Launch_Access-Token-Request_No-Refresh-Tokens" class="section-link"
        style="font-size: 18px;"><span class="tutorial-section-header-text" tabindex="-1">Access Token Request: If You
            Are Not Using a Client Secret</span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Standalone-Oauth2-Launch_Access-Token-Request_No-Refresh-Tokens"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>
        The following parameters are required in the POST body:
    </p>

    <ul>
        <li><code>grant_type</code>: For the EHR launch flow, this should contain the value "authorization_code".</li>
        <li><code>code</code>: This parameter contains the authorization code sent from Epic's authorization server to
            your application as a querystring parameter on the redirect URI as described above.</li>
        <li><code>redirect_uri</code>: This parameter must contain the same redirect URI that you provided in the
            initial access request. The value of this parameter needs to be URL encoded.</li>
        <li><code>client_id</code>: This parameter must contain the application's client ID issued by Epic that you
            provided in the initial request.</li>
    </ul>

    <p>Here's an example of what an HTTP POST request for an access token might look like:</p>


<pre class="well">
    <code class="http">
POST https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token HTTP/1.1
Content-Type: application/x-www-form-urlencoded
grant_type=authorization_code&amp;code=yfNg-rSc1t5O2p6jVAZLyY00uOOte5KM1y3YUxqsJQnBKEMNsYqOPTyVqcCH3YXaPkLztO9Rvf7bhLqQTwALHcHN6raxpTbR1eVgV2QyLA_4K0HrJO92et3qRXiXPkj7&amp;redirect_uri=https%3A%2F%2Ffhir.epic.com%2Ftest%2Fsmart&amp;client_id=d45049c3-3441-40ef-ab4d-b9cd86a17225 
    </code>
</pre>

<p>The authorization server responds to the HTTP POST request with a JSON object that includes an access token. The
        response contains the following fields:</p>

<ul>
    <li><code>access_token</code>: This parameter contains the access token issued by Epic to your application and
        is used in future requests.</li>
    <ul>
        <li>Starting in the May 2020 Epic version, Epic community members can enable a feature that makes all OAuth
            2.0 tokens and codes, including access tokens, <a href="https://jwt.io/">JSON Web Tokens (JWTs)</a>
            instead of opaque tokens. This change allows apps to inspect the tokens directly without needing to call
            the Introspection endpoint, but it also increases the length of these tokens significantly. This feature
            is enabled in the fhir.epic.com sandbox. Developers should ensure that app URL handling does not
            truncate OAuth 2.0 tokens and codes. </li>
    </ul>
    <li><code>token_type</code>: In Epic's OAuth 2.0 implementation, this parameter always includes the value
        <code>bearer</code>.</li>
        <li><code>expires_in</code>: This parameter contains the number of seconds for which the access token is valid.
        </li>
        <li><code>scope</code>: This parameter describes the access your application is authorized for.</li>
        <li><code>id_token</code>: Returned only for applications that have requested an <em>openid</em> scope. See
            above for more info on <a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_ID-Tokens&quot;)"
                tabindex="0">OpenID Connect id_tokens</a>. This parameter follows the guidelines described earlier in
            this document.</li>
        <li><code>patient</code>: For patient-facing workflows, this parameter identifies the FHIR ID for the patient on
            whose behalf authorization to the system was granted. <ul>
                <li><strong>The patient's FHIR ID is not returned for provider-facing standalone launch
                        workflows.</strong></li>
            </ul>
        </li>
        <li>
            <code>epic.dstu2.patient</code>: For patient-facing workflows, this parameter identifies the DSTU2 FHIR ID
            for the patient on whose behalf authorization to the system was granted. <ul>
                <li><strong>The patient's FHIR ID is not returned for provider-facing standalone launch
                        workflows.</strong></li>
            </ul>
        </li>
    </ul>

    <p>Note that you can pass additional parameters if needed based on the integration configuration. Here's an example
        of what a JSON object including an access token might look like:</p>

    <pre class="well"><code class="http">{
"access_token": "Nxfve4q3H9TKs5F5vf6kRYAZqzK7j9LHvrg1Bw7fU_07_FdV9aRzLCI1GxOn20LuO2Ahl5RkRnz-p8u1MeYWqA85T8s4Ce3LcgQqIwsTkI7wezBsMduPw_xkVtLzLU2O",
"token_type": "bearer",
"expires_in": 3240,
"scope": "Patient.read Patient.search ",
"patient": "T1wI5bk8n1YVgvWk9D05BmRV0Pi3ECImNSK8DKyKltsMB"
}</code>
</pre>

<p>At this point, authorization is complete and the web application can access the protected patient data it
    requested using FHIR APIs.
</p>
    <h3 id="Standalone-Oauth2-Launch_Access-Token-Request_With-Refresh-Tokens" class="section-link"
        style="font-size: 18px;"><span class="tutorial-section-header-text" tabindex="-1">Access Token Request: If You
            Are Using a Client Secret:</span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Standalone-Oauth2-Launch_Access-Token-Request_With-Refresh-Tokens"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>After receiving the authorization code, your application trades the code for a JSON object containing an access
        token and contextual information by sending an HTTP POST to the token endpoint using a Content-Type header with
        value of "application/x-www-form-urlencoded". For more information, see <a
            href="https://tools.ietf.org/html/rfc6749#section-4.1.3">RFC 6749 section 4.1.3</a>.</p>

    <p>
        The Epic on FHIR website can generate a client secret (effectively a password) for your app to use to obtain
        refresh tokens, and store the hashed secret for you, or Epic community members can upload a client secret hash
        that you provide them when they activate your app for their system. <strong>If you provide community members a
            client secret hash to upload, you should use a unique client secret per Epic community member and per
            environment type (non-production and production) for each Epic community member.</strong>
    </p>

    <p>
        The following parameters are required in the POST body:
    </p>

    <ul>
        <li><code>grant_type</code>: This should contain the value <code>authorization_code</code>.</li>
        <li><code>code</code>: This parameter contains the authorization code sent from Epic's authorization server to
            your application as a querystring parameter on the redirect URI as described above.</li>
        <li><code>redirect_uri</code>: This parameter must contain the same redirect URI that you provided in the
            initial access request. The value of this parameter needs to be URL encoded.</li>
    </ul>

    <p>
        <strong>Note: </strong> The <code>client_id</code> parameter is not passed in the the POST body if you use
        client secret authentication, which is different from the access token request for apps that do not use refresh
        tokens. You will instead pass an <code>Authorization</code> HTTP header with <code>client_id</code> and
        <code>client_secret</code> URL encoded and passed as a username and password. Conceptually the
        <code>Authorization</code> HTTP header will have this value:
        base64(<code>client_id</code>:<code>client_secret</code>).
    </p>

    <p>
        For example, using the following <code>client_id</code> and <code>client_secret</code>:
    </p>
    <div style="padding-top: 5px; padding-left: 20px; padding-bottom: 5px;">
        <p>
            <b>client_id</b>: d45049c3-3441-40ef-ab4d-b9cd86a17225
        </p>
        <p>
            <b>URL encoded client_id</b>: d45049c3-3441-40ef-ab4d-b9cd86a17225 <em style="margin-left: 20px;">Note:
                base64 encoding Epic's client IDs will have no effect</em>
        </p>
        <p>
            <b>client_secret</b>: this-is-the-secret-2/7
        </p>
        <p>
            <b>URL encoded client_secret</b>: this-is-the-secret-2%2F7
        </p>
    </div>
    <p>
        Would result in this <code>Authorization</code> header:
    </p>
    <pre
        class="well"><code class="http">Authorization: Basic base64Encode{d45049c3-3441-40ef-ab4d-b9cd86a17225:this-is-the-secret-2%2F7}</code></pre>
    <p>
        or
    </p>

    <pre
        class="well"><code class="http">Authorization: Basic ZDQ1MDQ5YzMtMzQ0MS00MGVmLWFiNGQtYjljZDg2YTE3MjI1OnRoaXMtaXMtdGhlLXNlY3JldC0yJTJGNw==</code></pre>

    <p>
        Here's an example of what a valid HTTP POST might look like:
    </p>


<pre class="well">
    <code class="http">POST https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token HTTP/1.1
Content-Type: application/x-www-form-urlencoded 
Authorization: Basic ZDQ1MDQ5YzMtMzQ0MS00MGVmLWFiNGQtYjljZDg2YTE3MjI1OnRoaXMtaXMtdGhlLXNlY3JldC0yJTJGNw==

grant_type=authorization_code&amp;code=yfNg-rSc1t5O2p6jVAZLyY00uOOte5KM1y3YUxqsJQnBKEMNsYqOPTyVqcCH3YXaPkLztO9Rvf7bhLqQTwALHcHN6raxpTbR1eVgV2QyLA_4K0HrJO92et3qRXiXPkj7&amp;redirect_uri=https%3A%2F%2Ffhir.epic.com%2Ftest%2Fsmart
    </code>
</pre>

<p>
        The authorization server responds to the HTTP POST request with a JSON object that includes an access token and
        a refresh token. The response contains the following fields:
</p>

<ul>
        <li><code>refresh_token</code>: This parameter contains the refresh token issued by Epic to your application and
            can be used to obtain a new access token. For more information on how this works, see Step 5.</li>
        <ul>
            <li>Starting in the May 2020 Epic version, Epic community members can enable a feature that makes all OAuth
                2.0 tokens and codes, including refresh tokens, <a href="https://jwt.io/">JSON Web Tokens (JWTs)</a>
                instead of opaque tokens. This change allows apps to inspect the tokens directly without needing to call
                the Introspection endpoint, but it also increases the length of these tokens significantly. This feature
                is enabled in the fhir.epic.com sandbox. Developers should ensure that app URL handling does not
                truncate OAuth 2.0 tokens and codes. </li>
        </ul>
        <li><code>access_token</code>: This parameter contains the access token issued by Epic to your application and
            is used in future requests.</li>
        <ul>
            <li>Starting in the May 2020 Epic version, Epic community members can enable a feature that makes all OAuth
                2.0 tokens and codes, including access tokens, <a href="https://jwt.io/">JSON Web Tokens (JWTs)</a>
                instead of opaque tokens. This change allows apps to inspect the tokens directly without needing to call
                the Introspection endpoint, but it also increases the length of these tokens significantly.</li>
        </ul>
        <li><code>token_type</code>: In Epic's OAuth 2.0 implementation, this parameter always includes the value
            <code>bearer</code>.</li>
        <li><code>expires_in</code>: This parameter contains the number of seconds for which the access token is valid.
        </li>
        <li><code>scope</code>: This parameter describes the access your application is authorized for.</li>
        <li>
            <code>id_token</code>: Returned only for applications that have requested an <em>openid</em> scope. See
            above for more info on <a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Embedded-Oauth2-Launch_ID-Tokens&quot;)"
                tabindex="0">OpenID Connect id_tokens</a>. This parameter follows the guidelines of the <a
                href="https://openid.net/specs/openid-connect-core-1_0.html#IDToken" target="_blank">OpenID Connect
                (OIDC) Core 1.0 specification</a>. It is signed but not encrypted.
        </li>
        <li>
            <code>patient</code>: For patient-facing workflows, this parameter identifies the FHIR ID for the patient on
            whose behalf authorization to the system was granted. <ul>
                <li><strong>The patient's FHIR ID is not returned for provider-facing standalone launch
                        workflows.</strong></li>
            </ul>
        </li>
        <li>
            <code>epic.dstu2.patient</code>: For patient-facing workflows, this parameter identifies the DSTU2 FHIR ID
            for the patient on whose behalf authorization to the system was granted. <ul>
                <li><strong>The patient's FHIR ID is not returned for provider-facing standalone launch
                        workflows.</strong></li>
            </ul>
        </li>
        <li>
            <code>encounter</code>: This parameter identifies the FHIR ID for the patient’s encounter, if in context at
            time of launch. The encounter token corresponds to the FHIR Encounter resource.<ul>
                <li><strong>The encounter FHIR ID is not returned for standalone launch workflows.</strong></li>
            </ul>
        </li>
        <li>
            <code>location</code>: This parameter identifies the FHIR ID for the ecounter department, if in context at
            time of launch. The location token corresponds to the FHIR Location resource.<ul>
                <li><strong>The location FHIR ID is not returned for standalone launch workflows.</strong></li>
            </ul>
        </li>
        <li>
            <code>appointment</code>: This parameter identifies the FHIR ID for the patient’s appointment, if
            appointment context is available at time of launch. The appointment token corresponds to the FHIR
            Appointment resource.<ul>
                <li><strong>The appointment FHIR ID is not returned for standalone launch workflows.</strong></li>
            </ul>
        </li>
        <li>
            <code>loginDepartment</code>: This parameter identifies the FHIR ID of the user's login department for
            launches from Hyperspace. The loginDepartment token corresponds to the FHIR Location resource.<ul>
                <li><strong>The loginDepartment FHIR ID is not returned for standalone launch workflows.</strong></li>
            </ul>
        </li>
    </ul>

    <p>Note that you can pass additional parameters if needed based on the integration configuration. Here's an example
        of what a JSON object including an access token and refres token might look like:</p>

    <pre class="well"><code class="http">{
"access_token": "Nxfve4q3H9TKs5F5vf6kRYAZqzK7j9LHvrg1Bw7fU_07_FdV9aRzLCI1GxOn20LuO2Ahl5RkRnz-p8u1MeYWqA85T8s4Ce3LcgQqIwsTkI7wezBsMduPw_xkVtLzLU2O",
"refresh_token": "H9TKs5F5vf6kRYAZqzK7j9L_07_FdV9aRzLCI1GxOn20LuO2Ahl5R1MeYWqA85T8s4sTkI7wezBsMduPw_xkLzLU2O",
"token_type": "bearer",
"expires_in": 3240,
"scope": "Patient.read Patient.search ",
"patient": "T1wI5bk8n1YVgvWk9D05BmRV0Pi3ECImNSK8DKyKltsMB"
}</code></pre>

<p>
        At this point, authorization is complete and the web application can access the protected patient data it
        requested using FHIR APIs.
</p>


<h3 id="Standalone-Oauth2-Launch_Using-Access-Token" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Step 4: <small>Your Application Uses FHIR APIs to Access Patient Data</small></span><a
            class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Standalone-Oauth2-Launch_Using-Access-Token"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

    <p>
        With a valid access token, your application can now access protected patient data from the EHR database using
        FHIR APIs. Queries must contain an Authorization header that includes the access token presented as a Bearer
        token.
    </p>

    <p>
        Here's an example of what a valid query looks like:
    </p>

<pre class="well"><code class="http">GET https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Patient/T1wI5bk8n1YVgvWk9D05BmRV0Pi3ECImNSK8DKyKltsMB HTTP/1.1
        
Authorization: Bearer Nxfve4q3H9TKs5F5vf6kRYAZqzK7j9LHvrg1Bw7fU_07_FdV9aRzLCI1GxOn20LuO2Ahl5RkRnz-p8u1MeYWqA85T8s4Ce3LcgQqIwsTkI7wezBsMduPw_xkVtLzLU2O
</code></pre>

<h3 id="Standalone-Oauth2-Launch_Using-Refresh-Token" class="section-link"><span
            class="tutorial-section-header-text" tabindex="-1">Step 5: <small>Use a Refresh Token to Obtain a New Access
                Token</small></span><a class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Standalone-Oauth2-Launch_Using-Refresh-Token"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>

<p>If your app uses refresh tokens (i.e. it can securely store credentials), then you can use a refresh token to
        request a new access token when the current access token expires (determined by the <code>expires_in</code>
        field from the authorization response from step 3).</p>

<p>Your application trades the <code>refresh_token</code> for a JSON object containing a new access token and
        contextual information by sending an HTTP POST to the token endpoint using a <code>Content-Type</code> HTTP
        header with value of "application/x-www-form-urlencoded". For more information, see <a
            href="https://tools.ietf.org/html/rfc6749#section-4.1.3">RFC 6749 section 4.1.3</a>.</p>

<p>
        The Epic on FHIR website can generate a client secret (effectively a password) for your app when using refresh
        tokens, and store the hashed secret for you, or Epic community members can upload a client secret hash that you
        provide them when they activate your app for their system. <strong>If you provide community members a client
            secret hash to upload, you should use a unique client secret per Epic community member and per environment
            type (non-production and production) for each Epic community member.</strong>
    </p>

<p>
    The following parameters are required in the POST body:
</p>
<ul>
    <li><code>grant_type</code>: This parameter always contains the value <code>refresh_token</code>.</li>
    <li><code>refresh_token</code>: The refresh token received from a prior authorization request.</li>
</ul>
<p>
    An <code>Authorization</code> header using HTTP Basic Authentication is required, where the username is the URL
    encoded <code>client_id</code> and the password is the URL encoded <code>client_secret</code>.
</p>
<p>
        For example, using the following <code>client_id</code> and <code>client_secret</code>:
    </p>
    <div style="padding-top: 5px; padding-left: 20px; padding-bottom: 5px;">
        <p>
            <b>client_id</b>: d45049c3-3441-40ef-ab4d-b9cd86a17225
        </p>
        <p>
            <b>URL encoded client_id</b>: d45049c3-3441-40ef-ab4d-b9cd86a17225 <em style="margin-left: 20px;">Note:
                base64 encoding Epic's client IDs will have no effect</em>
        </p>
        <p>
            <b>client_secret</b>: this-is-the-secret-2/7
        </p>
        <p>
            <b>URL encoded client_secret</b>: this-is-the-secret-2%2F7
        </p>
    </div>
    <p>
        Would result in this <code>Authorization</code> header:
    </p>
    <pre
        class="well"><code class="http">Authorization: Basic base64Encode{d45049c3-3441-40ef-ab4d-b9cd86a17225:this-is-the-secret-2%2F7}</code></pre>
    <p>
        or
    </p>
    <pre
        class="well"><code class="http">Authorization: Basic ZDQ1MDQ5YzMtMzQ0MS00MGVmLWFiNGQtYjljZDg2YTE3MjI1OnRoaXMtaXMtdGhlLXNlY3JldC0yJTJGNw==
    </code>
    </pre>
<p>
    Here's an example of what a valid HTTP POST might look like:
</p>


<pre class="well"><code class="http">POST https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token HTTP/1.1
Authorization: Basic ZDQ1MDQ5YzMtMzQ0MS00MGVmLWFiNGQtYjljZDg2YTE3MjI1OnRoaXMtaXMtdGhlLXNlY3JldC0yJTJGNw==
Content-Type: application/x-www-form-urlencoded
grant_type=refresh_token&amp;refresh_token=j12xcniournlsdf234bgsd
    </code>
</pre>

<p>The authorization server responds to the HTTP POST request with a JSON object that includes the new access token.
        The response contains the following fields:
</p>
<ul>
    <li><code>access_token</code>: This parameter contains the new access token issued.</li>
    <li><code>token_type</code>: In Epic's OAuth 2.0 implementation, this parameter always includes the value
        <code>bearer</code>.</li>
    <li><code>expires_in</code>: This parameter contains the number of seconds for which the access token is valid.
    </li>
    <li><code>scope</code>: This parameter describes the access your application is authorized for.</li>
</ul>
<p>An example response to the previous request may look like the following:</p>

<pre class="well"><code class="http">
{
"access_token": "57CjhZEdiTcCh1nqIwQUw5rODOLP3bSTnMEGNYbBerSeNn8hIUm6Mlc5ruCTfQawjRAoR8sYr8S_7vNdnJfgRKfD6s8mOqPnvJ8vZOHjEvy7l3Ra9frDaEAUBbN-j86k",
"token_type": "bearer",
"expires_in": 3240,
"scope": "Patient.read Patient.search ",
"refresh_token": "b72foiua9asdhnkjanvm"
}
</code></pre>

<h1 id="BackendOAuth2Guide" class="section-link"><span class="tutorial-section-header-text" tabindex="-1">SMART
            Backend Services (Backend OAuth 2.0)</span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=BackendOAuth2Guide"><span
                class="glyphicon glyphicon-link section-link-icon">
                </span>
        </a>
</h1>
<p>
    <strong>Contents</strong>
</p>
<ul>
    <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
            onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Backend-Oauth2_Building-Backend-Apps&quot;)"
            tabindex="0">Building a Backend OAuth 2.0 App</a></li>
    <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
            onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Backend-Oauth2_Customer-Setup&quot;)"
            tabindex="0">Complete Required Epic Community Member Setup to Audit Access from Your Backend
            Application</a></li>
    <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
            onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Backend-Oauth2_Creating-Key-Pair&quot;)"
            tabindex="0">Creating a Public Private Key Pair for JWT Signature</a>
        <ul>
            <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Backend-Oauth2_Creating-Key-Pair_OpenSSL&quot;)"
                    tabindex="0">OpenSSL</a></li>
            <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Backend-Oauth2_Creating-Key-Pair_Powershell&quot;)"
                    tabindex="0">Windows PowerShell</a></li>
            <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Backend-Oauth2_Finding-Key-Fingerprint&quot;)"
                    tabindex="0">Finding the Public Key Certificate Fingerprint (Also Called Thumbprint)</a></li>
        </ul>
    </li>
    <li>
        <a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
            onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Backend-Oauth2_Getting-Access-Token&quot;)"
            tabindex="0">Using a JWT to Obtain an Access Token for a Backend Service</a>
        <ul>
            <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Backend-Oauth2_Creating-JWT&quot;)"
                    tabindex="0">Step 1: Creating the JWT</a></li>
            <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;OAuth2&quot;,&quot;Backend-Oauth2_Access-Token-Request&quot;)"
                    tabindex="0">Step 2: POSTing the JWT to Token Endpoint to Obtain Access Token</a></li>
        </ul>
    </li>
</ul>
<p></p>
<h2>Overview</h2>
<p>Backend apps (i.e. apps without direct end user or patient interaction) can also use OAuth 2.0 authentication
    through the client_credentials OAuth 2.0 grant type. Epic's OAuth 2.0 implementation for backend services
    follows the <a href="http://hl7.org/fhir/uv/bulkdata/authorization/index.html" target="_blank">SMART Backend
        Services: Authorization Guide</a>, though it currently differs from that profile in some respects.
    Application vendors pre-register a public key for a given Epic community member on the Epic on FHIR website and
    then use the corresponding private key to sign a <a href="https://tools.ietf.org/html/rfc7519"
        target="_blank">JSON Web Token (JWT)</a> which is presented to the authorization server to obtain an access
    token.</p>
<h2 id="Backend-Oauth2_Building-Backend-Apps" class="section-link"><span class="tutorial-section-header-text"
        tabindex="-1">Building a Backend OAuth 2.0 App</span><a class="copy-section-link"
        title="Copy a link to this section to your clipboard."
        href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Backend-Oauth2_Building-Backend-Apps"><span
            class="glyphicon glyphicon-link section-link-icon"></span></a></h2>
<p>
    To use the client_credentials OAuth 2.0 grant type to authorize your backend application's access to patient
    information, two pieces of information need to be shared between the authorization server and your application:
</p>
<ol>
    <li><strong>Client ID</strong>: The client ID identifies your application to authentication servers within the
        Epic community and allows you to connect to any organization.</li>
    <li><strong>Public key</strong>: The public key is used to validate your signed JSON Web Token to confirm your
        identity.
    </li>
</ol>
<p>
    You can register your application for access to both the sandbox and Epic organizations <a
        href="https://fhir.epic.com/Developer/Apps">here</a>. You'll provide information to us, including a public
    key used to verify the JWT signature, and Epic will generate a client ID for you. Your app will need to have the
    <em>Backend Systems</em> radio button selected in order to register a public key for backend OAuth 2.0.
</p>
<h3 id="Backend-Oauth2_Customer-Setup" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Complete Required Epic Community Member Setup to Audit Access from Your Backend
            Application</span><a class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Backend-Oauth2_Customer-Setup"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>
    <p>An Epic community member's Epic Client Systems Administrator (ECSA) will need to map your client ID to an Epic
        user account that will be used for auditing web service calls made by your backend application. <strong>Your app
            will not be able to obtain an access token until this build is completed.</strong> </p>
    <section class="alert alert-info">
        <p>Note: This build is not needed in the sandbox. The sandbox automatically maps your client to a default user,
            removing the need for this setup.</p>
    </section>
    <h2 id="Backend-Oauth2_Creating-Key-Pair" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Creating a Public Private Key Pair for JWT Signature</span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Backend-Oauth2_Creating-Key-Pair"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h2>
    <p>There are several tools you can use to create a key pair. As long as you can export your public key to a base64
        encoded X.509 certificate for registration on the Epic on FHIR website, the tool you use to create the key pair
        and file format used to store the keys is not important. <strong>If your app is hosted by Epic community members
            ("on prem" hosting) instead of cloud hosted you should have unique key pairs for each Epic community member
            you integrate with. In addition, you should always use unique key pairs for non-production and production
            systems.</strong></p>
    <p>Here are examples of two tools commonly used to generate key pairs:</p>
    <h3 id="Backend-Oauth2_Creating-Key-Pair_OpenSSL" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Creating a Public Private Key Pair: OpenSSL</span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Backend-Oauth2_Creating-Key-Pair_OpenSSL"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>
    <p>You can create a new private key named <code>privatekey.pem</code> using OpenSSL with the following command:</p>
    <div class="commandWrapper">
        <p><code>openssl genrsa -out /path_to_key/privatekey.pem 2048</code></p>
    </div>
    <p>Make sure the key length is at least 2048 bits.</p>
    <p>Then you can export the public key to a base64 encoded X.509 certificate named <code>publickey509.pem</code>
        using this command:</p>
    <div class="commandWrapper">
        <p><code>openssl req -new -x509 -key /path_to_key/privatekey.pem -out /path_to_key/publickey509.pem -subj '/CN=myapp'</code>
        </p>
    </div>
    <p>Where <code>'/CN=myapp'</code> is the subject name (for example the app name) the key pair is for. The subject
        name does not have a functional impact in this case but it is required for creating an X.509 certificate.
    </p>
    <h3 id="Backend-Oauth2_Creating-Key-Pair_Powershell" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Creating a Public Private Key Pair: Windows PowerShell</span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Backend-Oauth2_Creating-Key-Pair_Powershell"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>
    <p>You can create a key pair using Windows PowerShell with this command, making sure to run PowerShell as an
        administrator:
    </p>
    <div class="commandWrapper">
        <p><code>New-SelfSignedCertificate -Subject "MyApp"</code></p>
    </div>
    <p><i>Note that the Epic on FHIR website is extracting the public key from the certificate and discarding the rest
            of the certificate information, so it's not 'trusting' the self-signed certificate per se.</i></p>
    <p>The subject name does not have a functional impact in this case but it is required for creating an X.509
        certificate.</p>
    <p>You can export the public key using either PowerShell or Microsoft Management Console.</p>
    <p>If you want to export the public key using PowerShell, take note of the certificate thumbprint and storage
        location printed when you executed the <code>New-SelfSignedCertificate</code> command.</p>
    <pre class="well"><code>PS C:\dir&gt; New-SelfSignedCertificate -Subject "MyApp"

PSParentPath: Microsoft.PowerShell.Security\Certificate::LocalMachine\MY

Thumbprint                                Subject
----------                                -------
3C4A558635D67F631E5E4BFDF28AE59B2E4421BA  CN=MyApp</code></pre>
    <p>Then export the public key to a X.509 certificate using the following commands (using the thumbprint and
        certificate location from the above example):</p>
    <div class="commandWrapper">
        <p><code>PS C:\dir&gt; $cert = Get-ChildItem -Path Cert:\LocalMachine\My\3C4A558635D67F631E5E4BFDF28AE59B2E4421BA</code>
        </p>
        <p><code>PS C:\dir&gt; Export-Certificate -Cert $cert -FilePath newpubkeypowerShell.cer</code></p>
    </div>
    <p>Export the binary certificate to a base64 encoded certificate:</p>
    <div class="commandWrapper">
        <p><code>PS C:\dir&gt; certutil.exe -encode newpubkeypowerShell.cer newpubkeybase64.cer</code></p>
    </div>
    <p>If you want to export the public key using Microsoft Management Console, follow these steps:</p>
    <ol>
        <li>
            Run (Windows + R) &gt; mmc (Microsoft Management Console).
        </li>
        <li>
            Go to File &gt; Add/Remove SnapIn.
        </li>
        <li>
            Choose Certificates &gt; Computer Account &gt; Local Computer. The exact location here depends on the
            certificate store that was used to create and store the keys when you ran the
            <code>New-SelfSignedCertificate</code> PowerShell command above. The store used is displayed in the printout
            after the command completes.
        </li>
        <li>
            Then back in the main program screen, go to Personal &gt; Certificates and find the key pair you just
            created in step 1. The "Issue To" column will equal the value passed to <code>-Subject</code> during key
            creation (e.g. "MyApp" above).
        </li>
        <li>Right click on the certificate &gt; All Tasks &gt; Export.</li>
        <li>Choose option to not export the private key.</li>
        <li>Choose to export in base-64 encoded X.509 (.CER) format.</li>
        <li>Choose a file location.</li>
    </ol>
    <h3 id="Backend-Oauth2_Finding-Key-Fingerprint" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Finding the Public Key Certificate Fingerprint (Also Called Thumbprint)</span><a
            class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Backend-Oauth2_Finding-Key-Fingerprint"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>
    <p>
        The <a href="https://en.wikipedia.org/wiki/Public_key_fingerprint">public key certificate fingerprint</a> (also
        known as thumbprint in Windows software) is displayed for JWT signing public key certificates that are uploaded
        to the Epic on FHIR website. There are a few ways you can find the fingerprint for a public key certificate:
    </p>
    <ul>
        <li>
            If you created the public key certificate using Windows PowerShell using the steps above, the thumbprint was
            displayed after completing the command. You can also print public keys and their thumbprints for a given
            certificate storage location using the <code>Get-ChildItem</code> PowerShell command. <p></p>
            <p>For example, run <code>Get-ChildItem -Path Cert:\LocalMachine\My</code> to find all certificate
                thumbprints in the local machine storage.</p>
        </li>
        <li>
            You can follow the <a
                href="https://docs.microsoft.com/en-us/dotnet/framework/wcf/feature-details/how-to-retrieve-the-thumbprint-of-a-certificate">steps
                here</a> to find the thumbprint of a certificate in Microsoft Management Console.
        </li>
        <li>
            You can run this OpenSSL command to print the public key certificate fingerprint that would be displayed on
            the Epic on FHIR website, replacing openssl_publickey.cert with the name of your public key certificate: <p>
            </p>
            <p><code>$ openssl x509 -noout -fingerprint -sha1 -inform pem -in openssl_publickey.cert</code></p>
            <p>Note that the output from this command includes colons between bytes which are not shown on the Epic on
                FHIR website.</p>
        </li>
    </ul>
    <h2 id="Backend-Oauth2_Getting-Access-Token" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Using a JWT to Obtain an Access Token for a Backend Service</span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Backend-Oauth2_Getting-Access-Token"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h2>
    <p>You will generate a one-time use JSON Web Token (JWT) to authenticate your app to the authorization server and
        obtain an access token that can be used to authenticate your app's web service calls. There are several
        libraries for creating JWTs. See <a href="https://jwt.io/" target="_blank">jwt.io</a> for some examples.</p>
    <h3 id="Backend-Oauth2_Creating-JWT" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Step 1: <small>Creating the JWT</small></span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Backend-Oauth2_Creating-JWT"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>
    <p>The JWT should have these headers:</p>
    <table class="table table-hover">
        <thead>
            <tr>
                <th id="jwt-header-val">Header</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="jwt-required-val" style="white-space: nowrap;"><code>alg</code></td>
                <td>The JWT authentication algorithm. Currently only RSA signing algorithms are supported so RSA 384
                    should be used and this should be set to <code>RS384</code>.</td>
            </tr>
            <tr>
                <td class="jwt-required-val" style="white-space: nowrap;"><code>typ</code></td>
                <td>This should always be set to <code>JWT</code>.</td>
            </tr>
        </tbody>
    </table>
    <p>The JWT header should be formatted as follows:</p>
    <pre class="well"><code class="http">{
"alg": "RS384",
"typ": "JWT"
}</code></pre>

<p>The JWT should have these claims in the payload:</p>
<table class="table table-hover">
    <thead>
        <tr>
            <th id="jwt-claims-claim">Claim</th>
            <th id="jwt-claims-description">Description</th>
            <th id="jwt-claims-remarks">Remarks</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="jwt-required-val" style="white-space: nowrap;"><code>iss</code></td>
            <td>
                Issuer of the JWT. This is the app's <code>client_id</code>, as determined during registration on
                the Epic on FHIR website.
            </td>
            <td>This is the same as the value for the <code>sub</code> claim.</td>
        </tr>
        <tr>
            <td class="jwt-required-val" style="white-space: nowrap;"><code>sub</code></td>
            <td>
                The service's <code>client_id</code>, as determined during registration on the Epic on FHIR website.
            </td>
            <td>This is the same as the value for the <code>iss</code> claim.</td>
        </tr>
        <tr>
            <td class="jwt-required-val" style="white-space: nowrap;"><code>aud</code></td>
            <td>
                The FHIR authorization server's token endpoint URL. This is the same URL to which this
                authentication JWT will be posted. See below for an example POST.
            </td>
            <td>It's possible that Epic community member systems will route web service traffic through a proxy
                server, in which case the URL the JWT is posted to is not known to the authorization server, and the
                JWT will be rejected. For such cases, Epic community member administrators can add additional
                audience URLs to the allowlist, in addition to the FHIR server token URL if needed. </td>
        </tr>
        <tr>
            <td class="jwt-required-val" style="white-space: nowrap;"><code>jti</code></td>
            <td>
                A unique identifier for the JWT.
            </td>
            <td>The <code>jti</code> must be no longer than 151 characters and cannot be reused during the JWT's
                validity period, i.e. before the <code>exp</code> time is reached.</td>
        </tr>
        <tr>
            <td class="jwt-required-val" style="white-space: nowrap;"><code>exp</code></td>
            <td>
                Expiration time integer for this authentication JWT, expressed in seconds since the "Epoch"
                (1970-01-01T00:00:00Z UTC).
            </td>
            <td>The <code>exp</code> value must be in the future, and can be no more than 5 minutes in the future at
                the time the access token request is received.</td>
        </tr>
        <tr>
            <td class="jwt-optional-val" style="white-space: nowrap;"><code>nbf</code></td>
            <td>
                Time integer before which the JWT must not be accepted for processing, expressed in seconds since
                the "Epoch" (1970-01-01T00:00:00Z UTC).
            </td>
            <td>The <code>nbf</code> value cannot be in the future, cannot be more recent than the <code>exp</code>
                value, and the <code>exp</code> - <code>nbf</code> difference cannot be greater than 5 minutes.</td>
        </tr>
        <tr>
            <td class="jwt-optional-val" style="white-space: nowrap;"><code>iat</code></td>
            <td>
                Time integer for when the JWT was created, expressed in seconds since the "Epoch"
                (1970-01-01T00:00:00Z UTC).
            </td>
            <td>The <code>iat</code> value cannot be in the future, and the <code>exp</code> - <code>iat</code>
                difference cannot be greater than 5 minutes.</td>
        </tr>
    </tbody>
</table>

<p>Here's an example JWT payload:</p>
<pre class="well"><code class="http">{
"iss": "d45049c3-3441-40ef-ab4d-b9cd86a17225",
"sub": "d45049c3-3441-40ef-ab4d-b9cd86a17225",
"aud": "https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token",
"jti": "f9eaafba-2e49-11ea-8880-5ce0c5aee679",
"exp": 1583524402,
"nbf": 1583524102,
"iat": 1583524102
}
    </code>
</pre>
    <p>The header and payload are then base64 URL encoded, combined with a period separating them, and cryptographically
        signed using the private key to generate a signature. Conceptually:</p>
    <pre
        class="well"><code class="http">signature = RSA-SHA384(base64urlEncoding(header) + '.' + base64urlEncoding(payload), privatekey)</code></pre>
    <p>The full JWT is constructed by combining the header, body and signature as follows: </p>
    <pre
        class="well"><code class="http">base64urlEncoding(header) + '.' + base64urlEncoding(payload) + '.' + base64urlEncoding(signature)</code></pre>
    <p>A fully constructed JWT looks something like this:</p>
    <pre
        class="well"><code class="http">eyJhbGciOiJSUzM4NCIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJkNDUwNDljMy0zNDQxLTQwZWYtYWI0ZC1iOWNkODZhMTcyMjUiLCJzdWIiOiJkNDUwNDljMy0zNDQxLTQwZWYtYWI0ZC1iOWNkODZhMTcyMjUiLCJhdWQiOiJodHRwczovL2ZoaXIuZXBpYy5jb20vaW50ZXJjb25uZWN0LWZoaXItb2F1dGgvb2F1dGgyL3Rva2VuIiwianRpIjoiZjllYWFmYmEtMmU0OS0xMWVhLTg4ODAtNWNlMGM1YWVlNjc5IiwiZXhwIjoxNTgzNTI0NDAyLCJuYmYiOjE1ODM1MjQxMDIsImlhdCI6MTU4MzUyNDEwMn0.dztrzHo9RRwNRaB32QxYLaa9CcIMoOePRCbpqsRKgyJmBOGb9acnEZARaCzRDGQrXccAQ9-syuxz5QRMHda0S3VbqM2KX0VRh9GfqG3WJBizp11Lzvc2qiUPr9i9CqjtqiwAbkME40tioIJMC6DKvxxjuS-St5pZbSHR-kjn3ex2iwUJgPbCfv8cJmt19dHctixFR6OG-YB6lFXXpNP8XnL7g85yLOYoQcwofN0k8qK8h4uh8axTPC21fv21mCt50gx59XgKsszysZnMDt8OG_G4gjk_8JnGHwOVkJhqj5oeg_GdmBhQ4UPuxt3YvCOTW9S2vMikNUnxrhdVvn2GVg</code></pre>
    <h3 id="Backend-Oauth2_Access-Token-Request" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Step 2: <small>POSTing the JWT to Token Endpoint to Obtain Access Token</small></span><a
            class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Backend-Oauth2_Access-Token-Request"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h3>
    <p>Your application makes a HTTP POST request to the authorization server's OAuth 2.0 token endpoint to obtain
        access token. The following form-urlencoded parameters are required in the POST body:</p>
    <ul>
        <li><code>grant_type</code>: This should be set to <code>client_credentials</code>.</li>
        <li><code>client_assertion_type</code>: This should be set to
            <code>urn:ietf:params:oauth:client-assertion-type:jwt-bearer</code>.</li>
        <li><code>client_assertion</code>: This should be set to the JWT you created above.</li>
    </ul>
    <p>Here is an example request:</p>
    <pre
        class="well"><code class="http">POST https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token HTTP/1.1
Content-Type: application/x-www-form-urlencoded

grant_type=client_credentials&amp;client_assertion_type=urn%3Aietf%3Aparams%3Aoauth%3Aclient-assertion-type%3Ajwt-bearer&amp;client_assertion=eyJhbGciOiJSUzM4NCIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJkNDUwNDljMy0zNDQxLTQwZWYtYWI0ZC1iOWNkODZhMTcyMjUiLCJzdWIiOiJkNDUwNDljMy0zNDQxLTQwZWYtYWI0ZC1iOWNkODZhMTcyMjUiLCJhdWQiOiJodHRwczovL2ZoaXIuZXBpYy5jb20vaW50ZXJjb25uZWN0LWZoaXItb2F1dGgvb2F1dGgyL3Rva2VuIiwianRpIjoiZjllYWFmYmEtMmU0OS0xMWVhLTg4ODAtNWNlMGM1YWVlNjc5IiwiZXhwIjoxNTgzNTI0NDAyLCJuYmYiOjE1ODM1MjQxMDIsImlhdCI6MTU4MzUyNDEwMn0.dztrzHo9RRwNRaB32QxYLaa9CcIMoOePRCbpqsRKgyJmBOGb9acnEZARaCzRDGQrXccAQ9-syuxz5QRMHda0S3VbqM2KX0VRh9GfqG3WJBizp11Lzvc2qiUPr9i9CqjtqiwAbkME40tioIJMC6DKvxxjuS-St5pZbSHR-kjn3ex2iwUJgPbCfv8cJmt19dHctixFR6OG-YB6lFXXpNP8XnL7g85yLOYoQcwofN0k8qK8h4uh8axTPC21fv21mCt50gx59XgKsszysZnMDt8OG_G4gjk_8JnGHwOVkJhqj5oeg_GdmBhQ4UPuxt3YvCOTW9S2vMikNUnxrhdVvn2GVg</code></pre>
    <p>And here is an example response body assuming the authorization server approves the request:</p>
    <pre class="well"><code class="http">{
"access_token": "i82fGhXNxmidCt0OdjYttm2x0cOKU1ZbN6Y_-zBvt2kw3xn-MY3gY4lOXPee6iKPw3JncYBT1Y-kdPpBYl-lsmUlA4x5dUVC1qbjEi1OHfe_Oa-VRUAeabnMLjYgKI7b",
"token_type": "bearer",
"expires_in": 3600,
"scope": "Patient.read Patient.search"
}</code></pre>
    <ul>
        <li><strong>Note: </strong>Starting in the May 2020 Epic version, Epic community members can enable a feature
            that makes all OAuth 2.0 tokens and codes, including access tokens, <a href="https://jwt.io/">JSON Web
                Tokens (JWTs)</a> instead of opaque tokens. This change allows apps to inspect the tokens directly
            without needing to call the Introspection endpoint, but it also increases the length of these tokens
            significantly. This feature is enabled in the fhir.epic.com sandbox. Developers should ensure that app URL
            handling does not truncate OAuth 2.0 tokens and codes. </li>
    </ul>

<h1 id="NonOauth" class="section-link"><span class="tutorial-section-header-text" tabindex="-1">Non-OAuth 2.0
            Authentication</span><a class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=NonOauth"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h1>
    Epic supports forms of authentication in addition to OAuth 2.0. <strong>Only use these forms of authentication if
        OAuth 2.0 is not an option.</strong>

<h2 id="Basic-Auth" class="section-link"><span class="tutorial-section-header-text" tabindex="-1">HTTP Basic
            Authentication </span><a class="copy-section-link" title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=Basic-Auth"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h2>

<p>
    HTTP Basic Authentication requires that Epic community members provision a username and password that your
    application will provide to the web server to authenticate every request.
</p>
<p>
    Epic supports HTTP Basic Authentication via a system-level user record created within Epic's database. You may
    hear this referred to as an <i>EMP</i> record. You will need to work with an organization's Epic user security
    team to have a username and password provided to you.
</p>
<p>
    When calling Epic's web services with HTTP Basic Authentication the username must be provided as follows:
    <code>emp$&lt;username&gt;</code>. Replace &lt;username&gt; with the username provided by the organization
    during implementation of your application.
</p>
<p>
    Base64 encode your application's credentials and pass them in the HTTP Authorization header. For example, if
    you've been given a system-level username/password combination that is username/Pa$$w0rd1, the Authorization
    header would have a value of ZW1wJHVzZXJuYW1lOlBhJCR3MHJkMQ== (the base64 encoded version of
    emp$username:Pa$$w0rd1):
</p>
<pre class="well"><code class="http">GET http://localhost:8888/website HTTP/1.1
Host: localhost:8888
Proxy-Connection: keep-alive
<span style="color: blue; font-weight: bold;">Authorization: Basic ZW1wJHVzZXJuYW1lOlBhJCR3MHJkMQ==</span></code></pre>

<h3>Storing HTTP Basic Authentication Values</h3>

<p>
    The username and password you use to authenticate your web service requests are extremely sensitive since they
    can be used to pull PHI out of an Epic organization's system. The credentials should always be encrypted and
    should not be stored directly within your client's code. You should make sure that access to decrypt the
    credentials should be limited to only the users that need access to it. For example, if a service account
    submits the web service requests, only that service account should be able to decrypt the credentials.
</p>
<h2>Client Certificates and SAML tokens</h2>

<p>
    Epic also supports the use of Client Certificates and SAML tokens as an authentication mechanism for
    server-to-server web service requests. We do not prefer these methods because both require web server
    administrators to maintain a trusted certificate for authentication. As certificates expire or servers need to
    be moved around, this puts an additional burden on system administrators.
</p>

<h2 id="NonOauth_Additional-Required-Headers" class="section-link"><span class="tutorial-section-header-text"
            tabindex="-1">Additional Required Headers</span><a class="copy-section-link"
            title="Copy a link to this section to your clipboard."
            href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=NonOauth_Additional-Required-Headers"><span
                class="glyphicon glyphicon-link section-link-icon"></span></a></h2>

<p>
    When you use OAuth 2.0 authentication, Epic can automatically gather important information about the client
    making a web service request. When you use a non-OAuth 2.0 authentication mechanism, we require that this
    additional information be passed in an HTTP header.
</p>
<h3 id="NonOauth_Epic-Client-ID-Header" class="section-link"><span class="tutorial-section-header-text"
        tabindex="-1">Epic-Client-ID</span><a class="copy-section-link"
        title="Copy a link to this section to your clipboard."
        href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=NonOauth_Epic-Client-ID-Header"><span
            class="glyphicon glyphicon-link section-link-icon"></span></a></h3>
<p>
    This is the client ID you were given upon your app's creation on the Epic on FHIR site. <strong>This is always
        required when calling Epic's web services if your app doesn't use OAuth 2.0 authentication.</strong>
</p>
<pre class="well"><code class="http">
GET http://localhost:8888/website HTTP/1.1
Hos: localhost:8888
Proy-Connection: keep-alive
Authorization: Basic ZW1wJHVzZXJuYW1lOlBhJCR3MHJkMQ==
<span style="color: blue; font-weight: bold;">Epic-Client-ID: 0000-0000-0000-0000-0000</span></code></pre>

<h3 id="NonOauth_Epic-User-Headers" class="section-link"><span class="tutorial-section-header-text"
        tabindex="-1">Epic-User-ID and Epic-User-IDType</span><a class="copy-section-link"
        title="Copy a link to this section to your clipboard."
        href="https://fhir.epic.com/Documentation?docId=oauth2&amp;section=NonOauth_Epic-User-Headers"><span
            class="glyphicon glyphicon-link section-link-icon"></span></a></h3>
<p>
    This is required for auditing requests to FHIR resources. The Epic-User-ID corresponds to an EMP (Epic User)
    account that an organization creates for your application. This might be required depending on the web services
    you are calling. The Epic-User-IDType is almost always <em>EXTERNAL</em>.
</p>
<pre class="well"><code class="http">GET http://localhost:8888/website HTTP/1.1
Host: localhost:8888
Proxy-Connection: keep-alive
Authorization: Basic ZW1wJHVzZXJuYW1lOlBhJCR3MHJkMQ==
Epic-Client-ID: 0000-0000-0000-0000-0000					 	
<span style="color: blue; font-weight: bold;">Epic-User-ID: username</span>					 	
<span style="color: blue; font-weight: bold;">Epic-User-IDType: EXTERNAL</span></code></pre>
</div>

</div>
<div id="fhir" style="display:none">
    <h3 class="body-header body-header-without-subtext">
        FHIR Tutorial
    </h3>
    <div>
        <p class="lead">
            FHIR is all about quickly getting data about a patient from a clinical system, and FHIR is all
            interconnected — you can build on top of your queries to get a wide variety of data.
            The topics below include a walkthrough introduction to FHIR data and sample patient and clinical information
            - everything you need to turn up the heat on your first FHIR App!
        </p>
        <p>
            Let's walk through a couple of examples using jQuery — in a larger application, you might build your own
            framework, or use an existing one, to interact with FHIR, but FHIR is easy enough to use without any special
            libraries. This tutorial assumes you are passing a form of authorization covered in one of our
            authentication guides. It's important to note FHIR APIs support OAuth 2.0 and additionally HTTP Basic
            Authentication.
        </p>
        <h2>
            Interacting with the API
        </h2>
        <p>
            To get started, let's define the FHIR server base URL. This is a constant address that all FHIR API
            endpoints for the server's default FHIR version live under. In a SMART on FHIR launch, this is specified
            with the iss parameter. We'll store this variable as a separate entity, so we can reference it across our
            queries:
        </p>

<pre class="well">    <code>
var baseUrl = "<strong>https://fhir.epic.com/interconnect-fhir-oauth</strong>/api/FHIR/DSTU2"
    </code>
</pre>
<p>
        You can expect the bold part of the above URL to be different for each unique instance of Epic you integrate
        with.
    </p>
    <h3>Finding Patients via Demographic Search</h3>
    <p>
        With the base URL defined, we can start building some simple queries and investigating the API. Because
        we're interacting with patient data, there's an obvious place to get started: finding the patient whose data
        we want to show. Epic's FHIR support provides two methods for applications to find and interact with
        patients:
    </p>
    <ol>
        <li>The Epic application can provide a patient to you, determined from the context or its patient selection
            activity. This method can apply to both clinician and patient-facing workflows</li>
        <li>Our Patient API provides the means to search for patients using demographic information such as their
            first and last name. We use this method when your application is independent from Epic.</li>
    </ol>

<pre class="well">    <code>
var patientSearchString = "/Patient?given=Derrick&amp;family=Lin&amp;birthdate=1973-06-03"
    </code>
</pre>

<p>
    We can use jQuery's <code>$.getJSON()</code> function to call into the URL and retrieve our data in JSON
    format. It takes a parameter describing the URL to query, and a callback for doing something with the data
    once the API responds. In our case, let's just log the data to the console so we can inspect it before
    developing code that works with it:
</p>
<pre class="well">   
<code>
</code>
</pre>
<p>
    The complete request URL at this point will read:
</p>
<pre class="well">
<code>
"https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Patient?family=Lin&amp;given=Derrick&amp;birthdate=1973-06-03"
</code>
</pre>

<p>If you execute the above request, you'll be able to see the following response in your console:</p>
        <p>
        </p>
        <pre class="pre-scrollable">{
  "resourceType": "Bundle",
  "type": "searchset",
  "total": 1,
  "link": [
    {
      "relation": "self",
      "url": "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Patient?family=Lin&amp;given=Derrick&amp;birthdate=1973-06-03"
    }
  ],
  "entry": [
    {
      "link": [
        {
          "relation": "self",
          "url": "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Patient/TGsj9rYGI8SV-LYYkdH1JqbxYuFGfrODXSPC6iLLYkJcB"
        }
      ],
      "fullUrl": "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Patient/TGsj9rYGI8SV-LYYkdH1JqbxYuFGfrODXSPC6iLLYkJcB",
      "resource": {
        "resourceType": "Patient",
        "id": "TGsj9rYGI8SV-LYYkdH1JqbxYuFGfrODXSPC6iLLYkJcB",
        "extension": [
          {
            "url": "http://hl7.org/fhir/StructureDefinition/us-core-race",
            "valueCodeableConcept": {
              "coding": [
                {
                  "system": "urn:oid:2.16.840.1.113883.5.104",
                  "code": "2028-9",
                  "display": "Asian"
                }
              ],
              "text": "Asian"
            }
          },
          {
            "url": "http://hl7.org/fhir/StructureDefinition/us-core-ethnicity",
            "valueCodeableConcept": {
              "coding": [
                {
                  "system": "urn:oid:2.16.840.1.113883.5.50",
                  "code": "UNK",
                  "display": "Unknown"
                }
              ],
              "text": "Unknown"
            }
          },
          {
            "url": "http://hl7.org/fhir/StructureDefinition/us-core-birth-sex",
            "valueCodeableConcept": {
              "coding": [
                {
                  "system": "http://hl7.org/fhir/v3/AdministrativeGender",
                  "code": "M",
                  "display": "Male"
                }
              ],
              "text": "Male"
            }
          }
        ],
        "identifier": [
          {
            "use": "usual",
            "system": "urn:oid:1.2.840.114350.1.13.0.1.7.5.737384.0",
            "value": "E4005"
          },
          {
            "use": "usual",
            "system": "urn:oid:1.2.840.114350.1.13.0.1.7.5.737384.14",
            "value": "203711"
          },
          {
            "extension": [
              {
                "url": "http://hl7.org/fhir/StructureDefinition/rendered-value",
                "valueString": "xxx-xx-0000"
              }
            ],
            "use": "usual",
            "system": "urn:oid:2.16.840.1.113883.4.1"
          }
        ],
        "active": true,
        "name": [
          {
            "use": "usual",
            "text": "Derrick Lin",
            "family": [
              "Lin"
            ],
            "given": [
              "Derrick"
            ]
          }
        ],
        "telecom": [
          {
            "system": "phone",
            "value": "785-555-5555",
            "use": "home"
          },
          {
            "system": "phone",
            "value": "785-666-6666",
            "use": "work"
          }
        ],
        "gender": "male",
        "birthDate": "1973-06-03",
        "deceasedBoolean": false,
        "address": [
          {
            "use": "home",
            "line": [
              "7324 Roosevelt Ave"
            ],
            "city": "INDIANAPOLIS",
            "state": "IN",
            "postalCode": "46201",
            "country": "US",
            "period": {
              "start": "2019-05-24T00:00:00Z"
            }
          }
        ],
        "maritalStatus": {
          "text": "Married"
        },
        "communication": [
          {
            "language": {
              "coding": [
                {
                  "system": "urn:oid:2.16.840.1.113883.6.99",
                  "code": "en",
                  "display": "English"
                }
              ],
              "text": "English"
            },
            "preferred": true
          }
        ]
      },
      "search": {
        "mode": "match"
      }
    }
  ]
}

</pre>
        <p></p>
        <p>Let's pick out a couple of key elements to learn more about:</p>
        <ol>
            <li>
                <p>The <code>total</code> element tells us how many elements were found in the search:</p>
                <ul>
                    <li>a <code>total</code> of 0 elements tells us that no results were found in the search. This may
                        mean that you didn't provide enough demographic data to make a match, or that the API doesn't
                        know about the patient you're looking for.</li>
                    <li>a <code>total</code> of 1 element tells us that exactly one patient was found. A single patient
                        returned in a search likely means that you've found the right person, but you should always
                        check the demographic information to confirm.</li>
                    <li>a <code>total</code> of more than 1 element tells us that the demographic information wasn't
                        sufficient to match to a single patient. If this happens, you can examine the response to match
                        against known demographic data, or perform a second query against the API with additional
                        demographic information.</li>
                </ul>
                <br>
                <p>Ideally, you provide enough patient information in your initial search for the API to return a single
                    patient — it's much safer to confirm a single patient than to compare a list of possible results,
                    and it reduces the complexity on your app's users to select a single patient rather than review a
                    list.</p>
                <p>In our case, we found exactly one patient, Derrick — exactly the patient we were looking for. If we
                    were building a more complex app, we might need to handle other scenarios gracefully, such as when
                    no patients are found.</p>
                <p>The following response is an example of what to expect if a patient match is not found. Note the text
                    contained within the OperationOutcome resource:</p>
                <p>
                </p>

<pre class="pre-scrollable">{
  "resourceType": "Bundle",
  "type": "searchset",
  "total": 0,
  "link": [
    {
      "relation": "self",
      "url": "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Patient?given=Derrick"
    }
  ],
  "entry": [
    {
      "resource": {
        "resourceType": "OperationOutcome",
        "issue": [
          {
            "severity": "warning",
            "code": "informational",
            "details": {
              "coding": [
                {
                  "system": "urn:oid:1.2.840.114350.1.13.0.1.7.2.657369",
                  "code": "4101",
                  "display": "Resource request returns no results."
                }
              ],
              "text": "Resource request returns no results."
            }
          }
        ]
      },
      "search": {
        "mode": "outcome"
      }
    }
  ]
}

</pre>
 </li>
 <li>
     <p>The <code>entry[n].link[n].url</code> element points to the permanent location of a unique patient.
         If we make a request to this URL, we will <strong>always</strong> get back that patient's data.
         Storing this URL can be useful if you want to save off a list of recently used patients, for
         example. The last part of the <code>url</code> in our search example is Derrick's FHIR ID. We'll use
         this to make all of our other queries to FHIR APIs.</p>
 </li>
 <li>
     <p>The <code>entry[0].resource</code> element contains all of the actual data about Derrick Lin. Within
                    this object, you'll find information about Derrick such as his address, phone number, and birth
                    date.</p>
            </li>
        </ol>
        <p>That's a quick overview of the patient search endpoint, but if you're interested in learning more about our
            Patient search, you can read more <a href="https://open.epic.com/Clinical/FHIR?whereTo=Patient">here</a>.
            Let's continue on by saving off Derrick's FHIR ID so we can refer to it later:</p>

<pre class="well">    <code>
var patientId = data.entry[0].link[0].url.split("/").pop();
    </code>
</pre>

 <h3>Interlude: Relative Resource URLs (R4 and Later)</h3>
 <p>There are several versions of the FHIR standard. You might use DSTU2 FHIR resources (like in the example
     above), STU3 resources, or more recent versions of FHIR resources, such as R4 resources. A variety of R4
     resources are supported starting in the August 2019 version of Epic.</p>
 <p>For R4 and later versions of FHIR, Epic uses relative URLs for all referenced FHIR resources included in a
     query response. For example, if we look back at the sample response from our Patient query above, if that
     query was an R4 version Patient query, instead of returning an absolute reference URL for the careProvider:
 </p>
 <pre>"careProvider": [
  {
    "display": "Physician Family Medicine, MD",
    "reference": "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Practitioner/Thh97FZ9lU9.p-Rgpozo6vwB"
  }
]
</pre>

<p>An R4 resource would return a relative URL instead:</p>
<pre>"careProvider": [
  {
    "display": "Physician Family Medicine, MD",
    "reference": "Practitioner/Thh97FZ9lU9.p-Rgpozo6vwB"
  }
]
</pre>
<p>This distinction is important to keep in mind, particularly if you’re working with some FHIR resources that
    use the DSTU2 or STU3 standard, and other resources that use the R4 standard. Make sure to append the
    baseURL to the front of the relative URL when querying referenced FHIR resources.
</p>

<h3>Searching for Clinical Data by Patient ID</h3>
<p>So we've found our patient, and we have some demographics that we could parse and display to an end user. But
    how do we get access to clinical information about that patient? To retrieve this data, we need to use
    another endpoint within our API, along with the patient ID we saved off above.</p>
<p>Let's start with a simple query — retrieving a patient's diagnoses. The Condition endpoint exposes exactly
    that, and provides a search endpoint to find diagnoses by Patient ID. We can construct a query that provides
    Derrick's FHIR ID to grab all of the active diagnoses recorded in our system:</p>
<pre class="well">    <code>
var conditionSearchString = "/Condition?patient=" + patientId;
    </code>
</pre>

<p>We'll use the same method as we did with the Patient search to investigate the response returned from the
            Condition API, and log the response body to the console as a simple means to view the data.
    </p>

        <pre class="well">    <code>
    </code>
</pre>

<p>Executing the above request returns the following:</p>

<pre class="pre-scrollable">{
  "resourceType": "Bundle",
  "type": "searchset",
  "total": 1,
  "link": [
    {
      "relation": "self",
      "url": "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Condition?patient=TGsj9rYGI8SV-LYYkdH1JqbxYuFGfrODXSPC6iLLYkJcB&amp;category=diagnosis"
    }
  ],
  "entry": [
    {
      "link": [
        {
          "relation": "self",
          "url": "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Condition/TH12jkQYrabXcEh-2vXMxLAB"
        }
      ],
      "fullUrl": "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Condition/TH12jkQYrabXcEh-2vXMxLAB",
      "resource": {
        "resourceType": "Condition",
        "id": "TH12jkQYrabXcEh-2vXMxLAB",
        "patient": {
          "display": "Derrick Lin",
          "reference": "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Patient/TGsj9rYGI8SV-LYYkdH1JqbxYuFGfrODXSPC6iLLYkJcB"
        },
        "asserter": {
          "display": "Physician Family Medicine, MD",
          "reference": "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Practitioner/Thh97FZ9lU9.p-Rgpozo6vwB"
        },
        "dateRecorded": "2019-05-28",
        "code": {
          "coding": [
            {
              "system": "http://hl7.org/fhir/sid/icd-9-cm/diagnosis",
              "code": "V49.89",
              "display": "Risk for coronary artery disease between 10% and 20% in next 10 years"
            },
            {
              "system": "urn:oid:2.16.840.1.113883.6.90",
              "code": "Z91.89",
              "display": "Risk for coronary artery disease between 10% and 20% in next 10 years"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "315016007",
              "display": "At risk of coronary heart disease (finding)"
            }
          ],
          "text": "Risk for coronary artery disease between 10% and 20% in next 10 years"
        },
        "category": {
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "29308-4",
              "display": "Diagnosis"
            },
            {
              "system": "http://snomed.info/sct",
              "code": "439401001",
              "display": "Diagnosis"
            },
            {
              "system": "http://hl7.org/fhir/condition-category",
              "code": "diagnosis",
              "display": "Diagnosis"
            },
            {
              "system": "http://argonautwiki.hl7.org/extension-codes",
              "code": "problem",
              "display": "Problem"
            }
          ],
          "text": "Diagnosis"
        },
        "clinicalStatus": "active",
        "verificationStatus": "confirmed",
        "onsetDateTime": "2019-05-28"
      },
      "search": {
        "mode": "match"
      }
    }
  ]
}
</pre>

<p>Looking at the response, there are a couple of key elements to highlight to help us understand the condition
    information:
    
</p>

<ol>
    <li>
        <p>As with Patient, the <code>total</code> element lets you know the number of diagnoses returned for a
            query. In this case, it lets us know how many diagnoses our system has documented for Derrick Lin.
        </p>
    </li>
    <li>
        <p>The <code>entry[n].link[n].url</code> element points to the permanent location of a unique Condition
            resource. If we make a request to this URL, we will <strong>always</strong> get back data for this
            specific diagnosis for the patient. The last part of the <code>url</code> in our search example is
            the FHIR ID for this Condition. The FHIR ID for this resource can also be retrieved directly from
            <code>entry[n].resource[n].id</code>. This might be useful for future reference to check whether the
            patient has any new conditions documented.</p>
        <ul>
            <li>For an R4 query, the entry[n].link[n].url element value in this example would be shortened to
                Condition/TH12jkQYrabXcEh-2vXMxLAB. All other resources included in this response, such as the
                Patient and Practitioner resources, would also use relative URLs. Refer back to the
                <i>Interlude: Relative Resource URLs (R4 and Later)</i> topic for a summary of this difference.
            </li>
        </ul>
    </li>
    <li>
        <p>The <code>entry[n].resource[n].patient</code> element describes the patient the diagnosis relates to.
            This allows us to confirm the association between diagnosis and patient, and shows how FHIR works by
            linking these smaller resources to paint a larger picture. In this case, the element in each
            diagnosis entry points to our patient, Derrick.</p>
    </li>
    <li>
        <p> The <code>entry[n].resource[n].asserter</code> element describes the clinician who recorded the
            diagnosis in the system. A link to the provider resource is included to allow you to search
            additional information about the provider who recorded this diagnosis.
        </p>
    </li>
    <li>
        <p>The <code>onsetDateTime</code> tells you when the condition was first noticed.</p>
    </li>
    <li>The <code>recordedDate</code> element tells you when the diagnosis was actually made and recorded.</li>
</ol>
<p>That's a lot of information with a simple query, and it demonstrates how quickly we can build up and
    associate the different endpoints available on our API. </p>
<h3>Wrap Up</h3>
<p>Nice work! You made it to the end of our walkthrough, and you understand how to find patients, and start
    working with their data. There's even more to discover in our documentation, like additional data types, and
    more complex searches. </p>
<br>
<h2>Advanced Topics</h2>
<p>
    In this section we'll cover a few advanced topics that may come up in your implementation of FHIR.
</p>
<h3>Content negotiation in FHIR</h3>

<p>
    Epic’s FHIR server defaults to using XML for FHIR resources, but many apps may prefer sending and receiving
    JSON instead. FHIR allows both XML and JSON MIME-types and specifies how a client can request one or the
    other. Per the <a href="https://www.hl7.org/fhir/http.html#mime-type">FHIR spec</a> and in Epic’s FHIR
    server, a client can specify XML or JSON through either the <a
        href="https://www.hl7.org/fhir/http.html#parameters">_format</a> query parameter or by specifying the
    MIME-type in an HTTP header. A client should use the <a
        href="https://tools.ietf.org/html/rfc7231#section-5.3.2"><code>Accept</code> HTTP header</a> to specify
    the MIME-type of the content that it wishes to receive from the server and the <a
        href="https://tools.ietf.org/html/rfc7231#section-3.1.1.5"><code>Content-Type</code> HTTP header</a> to
    specify the MIME-type of the content that it is sending in the body of its request, for example, as part of
    a create or update.
</p>
<p>
    Epic supports all of the following MIME-types for FHIR interactions:
</p>
<ul>
            <li>application/fhir+json</li>
            <li>application/json+fhir</li>
            <li>application/xml+fhir</li>
            <li>application/fhir+xml</li>
        </ul>


        <h3>Using STU3 Resources in a DSTU2 Environment</h3>

        <p>
            You might come across a FHIR API that you want to use that is only available in STU3, but the base URL for
            the environment returns a DSTU2 endpoint. This means that you will have a patient's DSTU2 FHIR ID but an
            STU3 resource requires an STU3 FHIR ID. As of the November 2018 version, Patient FHIR resources are FHIR ID
            version agnostic and you can use the Patient DSTU2 ID with the Patient STU3 resource and vice versa. As of
            the May 2019 version, all FHIR resources are Patient FHIR ID version agnostic and you can use the Patient
            DSTU2 ID with all STU3 resources and vice versa. You can pass in PatientID and PatientIDType to return a
            collection of different identifiers unique to a patient using the Patient.Read FHIR (STU3) resource. This
            can be used to get different Patient FHIR version IDs or to get an Epic-specific ID from a FHIR ID or vice
            versa. To start, we need to construct a search string for this resource.</p>

        <p>
            Using the provided URL template for this web service, we can create a search string. Note, this can be a
            DSTU2 ID for the STU3 resource in November 2018+:
        </p>

        <pre class="well">    <code>
var baseUrl = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/STU3"
var identifierSearchString = "/Patient/" + patientId;
    </code>
</pre>

<p>
            We can use jQuery's $.getJSON() function to call into the URL and retrieve our data in JSON format. It takes
            a parameter describing the URL to query, and a callback for doing something with the data once the API
            responds. In our case, let's just log the data to the console so we can inspect it before developing code
            that works with it:
        </p>

        <pre class="well">    <code>
$.getJSON(baseUrl + identifierSearchString, function(data,error) { console.log(data); });
    </code>
</pre>

        <p>
            The response contains a number of identifiers for the patient, most notably Derrick's FHIR STU3 ID, in the
            identifier element:
        </p>

        <p>
        </p>
        <pre class="pre-scrollable">{
  "resourceType": "Patient",
  "id": "eq081-VQEgP8drUUqCWzHfw3",
  "extension": [
    {
      "valueCode": "M",
      "url": "http://hl7.org/fhir/us/core/StructureDefinition/us-core-birthsex"
    },
    {
      "extension": [
        {
          "valueCoding": {
            "system": "http://hl7.org/fhir/us/core/ValueSet/omb-race-category",
            "code": "2028-9",
            "display": "Asian"
          },
          "url": "ombCategory"
        },
        {
          "valueString": "Asian",
          "url": "text"
        }
      ],
      "url": "http://hl7.org/fhir/us/core/StructureDefinition/us-core-race"
    },
    {
      "extension": [
        {
          "valueCoding": {
            "system": "http://hl7.org/fhir/us/core/ValueSet/omb-ethnicity-category",
            "code": "UNK",
            "display": "Unknown"
          },
          "url": "ombCategory"
        },
        {
          "valueString": "Unknown",
          "url": "text"
        }
      ],
      "url": "http://hl7.org/fhir/us/core/StructureDefinition/us-core-ethnicity"
    }
  ],
  "identifier": [
    {
      "use": "usual",
      "type": {
        "text": "EPIC"
      },
      "system": "urn:oid:1.2.840.114350.1.13.0.1.7.5.737384.0",
      "value": "E4005"
    },
    {
      "use": "usual",
      "type": {
        "text": "EXTERNAL"
      },
      "value": "Z6127"
    },
    {
      "use": "usual",
      "type": {
        "text": "FHIR"
      },
      "value": "TGsj9rYGI8SV-LYYkdH1JqbxYuFGfrODXSPC6iLLYkJcB"
    },
    {
      "use": "usual",
      "type": {
        "text": "FHIR STU3"
      },
      "value": "eq081-VQEgP8drUUqCWzHfw3"
    },
    {
      "use": "usual",
      "type": {
        "text": "INTERNAL"
      },
      "value": "     Z6127"
    },
    {
      "use": "usual",
      "type": {
        "text": "EPI"
      },
      "system": "urn:oid:1.2.840.114350.1.13.0.1.7.5.737384.14",
      "value": "203711"
    },
    {
      "extension": [
        {
          "valueString": "xxx-xx-0000",
          "url": "http://hl7.org/fhir/StructureDefinition/rendered-value"
        }
      ],
      "use": "usual",
      "system": "urn:oid:2.16.840.1.113883.4.1"
    }
  ],
  "active": true,
  "name": [
    {
      "use": "usual",
      "text": "Derrick Lin",
      "family": "Lin",
      "given": [
        "Derrick"
      ]
    }
  ],
  "telecom": [
    {
      "system": "phone",
      "value": "785-555-5555",
      "use": "home"
    },
    {
      "system": "phone",
      "value": "785-666-6666",
      "use": "work"
    }
  ],
  "gender": "male",
  "birthDate": "1973-06-03",
  "deceasedBoolean": false,
  "address": [
    {
      "use": "home",
      "line": [
        "7324 Roosevelt Ave"
      ],
      "city": "INDIANAPOLIS",
      "district": "MARION",
      "state": "IN",
      "postalCode": "46201",
      "country": "US",
      "period": {
        "start": "2019-05-24"
      }
    }
  ],
  "maritalStatus": {
    "text": "Married"
  },
  "communication": [
    {
      "language": {
        "coding": [
          {
            "system": "http://hl7.org/fhir/ValueSet/languages",
            "code": "en",
            "display": "English"
          }
        ],
        "text": "English"
      },
      "preferred": true
    }
  ],
  "managingOrganization": {
    "reference": "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/STU3/Organization/enRyWnSP963FYDpoks4NHOA3",
    "display": "Epic Hospital System"
  }
}
</pre>
        <p></p>

        <p>
            You can now save off Derrick's STU3 FHIR ID taking advantage of Jquery's $.grep() function:
        </p>

        <pre class="well">    <code>
var result = $.grep(data, function(x) { return x.type[n].text == "FHIR STU3"; });
var patientIdSTU3 = result[0].value  //We’ll only ever return one FHIR STU3 ID
    </code>
</pre>

        <p>
            Now that you have the STU3 ID, you're free to call into STU3 resources. Remember to change your base URL to
            reference STU3. Here's an example of the same Condition search we did earlier but now using Derrick's STU3
            FHIR ID.
        </p>

        <pre class="pre-scrollable">var completeSearchString = "https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/STU3/Condition?patient=" + patientIdSTU3 + "&amp;clinical-status=active"
</pre>

        <p>
            Of note, one difference you might notice between DSTU2 and STU3 resources is that paging is only supported
            in STU3 resources. DSTU2 responses come back in one request.
        </p>
    </div>


</div>
<div id="searchparameters" style="display:none">
    <h3 class="body-header body-header-without-subtext">
        Search Parameters
    </h3>



<div>
        <h1>FHIR Search Parameters</h1>
        <p>This document describes the FHIR Search result parameters currently available, what they can do, and how to
            use each of these parameters with Epic FHIR APIs.</p>
        <p>Search result parameters modify how results are returned in a Search API’s response. See the table below for
            the search result parameters that are supported by Epic and the versions in which they are supported.</p>
        <br>
        <table class="table table-hover">
            <tbody>
                <tr>
                    <td><strong>Parameter</strong>
                    </td>
                    <td>
                        <strong>Supported in DSTU2?</strong>
                    </td>
                    <td>
                        <strong>Supported in STU3?</strong>
                    </td>
                    <td>
                        <strong>Supported in R4?</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        _id
                    </td>
                    <td>
                        Yes
                    </td>
                    <td>
                        Yes
                    </td>
                    <td>
                        Yes
                    </td>
                </tr>
                <tr>
                    <td>
                        _count
                    </td>
                    <td>
                        Yes, for the Observation resource only
                    </td>
                    <td>
                        Yes
                    </td>
                    <td>
                        Yes
                    </td>
                </tr>
                <tr>
                    <td>
                        _include
                    </td>
                    <td>
                        No
                    </td>
                    <td>
                        Yes
                    </td>
                    <td>
                        Yes
                    </td>
                </tr>
                <tr>
                    <td>
                        _revInclude
                    </td>
                    <td>
                        No
                    </td>
                    <td>
                        No
                    </td>
                    <td>
                        Yes
                    </td>
                </tr>
            </tbody>
        </table>
        <p>&nbsp;&nbsp;</p>
        <h2>_id Parameter</h2>
        <p>The _id parameter is used to indicate a FHIR ID, allowing read-type functionality in a search FHIR API.</p>
        <p>So why would you want to use _id in a search API instead of a read API? Well, what if you wanted to perform a
            read with a FHIR search parameter? The _id parameter allows you to do just that, by using FHIR search
            parameters while still reading data about a single resource. </p>
        <h3>Examples:</h3>
        <pre class="pre-scrollable">GET https://hostname/instance/api/FHIR/STU3/Patient?_id=eYg3-1aJmCMq-umIIq2Njxw3&amp;_include=Patient:generalPractitioner:Practitioner
</pre>
        <h2>_count Parameter</h2>
        <p>The _count parameter is used to limit the number of results returned by a search request. Once the number of
            results specified has been reached, the URL to the next page of results will be generated and included in
            the response. This URL contains a session ID which returns the next group of _count results. This is useful
            for resources that could return a large number of results (such as Observation), as using _count can
            minimize the demand on the server. &nbsp;_count can be set to any number from 1 to 999.</p>
        <section class="alert alert-warning">
            <p><strong>Important note on resources that return many results:</strong></p>
            <p>If your search returns more than 1000 results, the search will automatically be broken into pages with
                1000 results each, regardless of whether or not you use the _count parameter. This means that your app
                needs to support this functionality to ensure that you receive all applicable data associated with the
                API request.</p>
            <p>The API response when using the _count parameter behaves differently for STU3 and later requests, versus
                DSTU2 requests, as described below.</p>
        </section>
        <h3>Examples:</h3>
        <p>For STU3 and above, look for the <code>link.relation</code> parameter. If <code>link.relation</code> is
            <code>"next"</code>, then use the request in <code>link.url</code> to obtain the next set of resources.
            Continue until <code>link.relation</code> is <code>"self"</code>.</p>
        <h4>Sample Request</h4>
        <pre class="pre-scrollable">GET https://apporchard.epic.com/interconnect-aocurprd-username/api/FHIR/STU3/Observation?patient=e63wRTbPfr1p8UW81d8Seiw3&amp;category=vital-signs&amp;_count=10
</pre>
        <h4>Sample Response</h4>
        <pre class="pre-scrollable">{ 
&nbsp;&nbsp;&nbsp;"resourceType": "Bundle",
&nbsp;&nbsp;&nbsp; "type": "searchset",
&nbsp;&nbsp;&nbsp; "total": 10,
&nbsp;&nbsp;&nbsp; "link": [
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "relation": "next",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "url": "https://apporchard.epic.com/interconnect-aocurprd-oauth/api/FHIR/STU3/Observation?patient=e63wRTbPfr1p8UW81d8Seiw3&amp;category=vital-signs&amp;_count=10&amp;<strong>sessionID=16-BAF392808B2B11EA92E00050568B7BE6</strong>"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }
...<i>{response continues}</i>
</pre>
        <h2>_include Parameter</h2>
        <p>The _include parameter is used to return resources related to the initial search result. Any other resource
            included as a reference can be returned in the response bundle. This reduces the number of requests needed
            to see information. For example, when searching for a patient's encounters, you can include the encounter
            providers as well.</p>
        <p>In the February 2021 version of Epic and prior versions, included references are returned immediately
            following the associated search result. Starting in the May 2021 version of Epic for STU3 and R4 FHIR
            resources, included resource references from all search results are at the end of the response bundle rather
            than after each individual search result, and each unique referenced resource instance is included only once
            in the bundle. Consumers of FHIR search results should be able to accept both bundle formats until a future
            version when we expect all integrations to eventually transition to the latter format.</p>
        <p>Epic supports the _include parameter primarily for FHIR search interactions. In addition, it is supported for
            particular operation types, like Appointment.$find.</p>
        <p>For this parameter to work, the Search response must include a reference to another FHIR resource.</p>
        <p>General format: _include = [resource you’re searching]:[name of reference element you want included]:[type of
            resource if element can refer to multiple (optional)]</p>
        <ul>
            <li>[resource you’re searching] – this is the initial API you are calling
                <ul>
                    <li>Ex: When calling Observation.Search, you are searching the Observation resource.</li>
                </ul>
            </li>
            <li>[name of reference element you want included] – one of the response parameters for that API that has a
                type of “Reference”</li>
            <li>[type of resource if element can refer to multiple] – when the second parameter could refer to multiple
                resources, the third parameter specifies which resource to include</li>
        </ul>
        <h3>Examples:</h3>
        <ul>
            <li>Immunization.Search
                <ul>
                    <li>Parameter in response: Practitioner.actor (Reference)</li>
                    <li>From this, the second parameter would be “actor”</li>
                    <li><code>_include=Immunization:actor:Practitioner</code></li>
                    <li>Note: there is only one actor for Immunizations so the third parameter is optional
                        (<code>_include=Immunization:actor</code> would also work here)</li>
                </ul>
            </li>
        </ul>
        <ul>
            <li>Appointment $find (STU3)
                <ul>
                    <li>Parameters in response: Participant.actor (Reference(Patient|Practitioner|Location))</li>
                    <li>From this, the second parameter would be "actor" just like in the first example</li>
                    <li>Unlike the first example, you need to specify a third parameter as there are three potential
                        actors referenced. Any of the following examples are valid here:
                        <ul>
                            <li><code>_include=Appointment:actor:Patient</code></li>
                            <li><code>_include=Appointment:actor:Practitioner</code></li>
                            <li><code>_include=Appointment:actor:Location</code></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
        <h2>_revInclude Parameter</h2>
        <p>The _revInclude parameter (reverse include) is used to return a particular resource, along with other
            resources that refer to that resource. For this parameter to return results, the FHIR resource specified in
            the search response must be referenced by the resource specified in the _revInclude parameter.</p>
        <h3>Provenance</h3>
        <p></p>Epic currently supports using this parameter to fetch provenance data for search interactions with any
        FHIR resources that are part of the U.S. Core Data for Interoperability data classes and elements. This
        parameter is supported in Epic version February 2020 or later (special updates are required to use this feature
        on the February 2020, May 2020, or August 2020 versions).<p></p>
        <p>Parameter format: <code>_revInclude=Provenance:target</code></p>
        <p>The response will have the following format:</p>
        <table class="table table-hover">
            <tbody>
                <tr>
                    <td><strong>Element</strong>
                    </td>
                    <td>
                        <strong>Type</strong>
                    </td>
                    <td>
                        <strong>Cardinality</strong>
                    </td>
                    <td>
                        <strong>Description</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        target
                    </td>
                    <td>
                        Reference (resource)
                    </td>
                    <td>
                        1..*
                    </td>
                    <td>
                        The resource this provenance supports
                    </td>
                </tr>
                <tr>
                    <td>
                        recorded
                    </td>
                    <td>
                        Instant
                    </td>
                    <td>
                        1..1
                    </td>
                    <td>
                        Timestamp that the activity was recorded/updated
                    </td>
                </tr>
                <tr>
                    <td>
                        agent
                    </td>
                    <td>
                        Backbone Element
                    </td>
                    <td>
                        1..*
                    </td>
                    <td>
                        Actor involved
                    </td>
                </tr>
                <tr>
                    <td>
                        agent.type
                    </td>
                    <td>
                        CodeableConcept
                    </td>
                    <td>
                        0..1
                    </td>
                    <td>
                        How the agent participated
                    </td>
                </tr>
                <tr>
                    <td>
                        agent.who
                    </td>
                    <td>
                        Reference (Practitioner | Patient | Organization)
                    </td>
                    <td>
                        1..1
                    </td>
                    <td>
                        Who participated
                    </td>
                </tr>
                <tr>
                    <td>
                        agent.onBehalfOf
                    </td>
                    <td>
                        Reference (Organization)
                    </td>
                    <td>
                        0..1
                    </td>
                    <td>
                        Who the agent is representing.
                    </td>
                </tr>
            </tbody>
        </table>
        <h4>Examples:</h4>
        <h5>Sample Request</h5>
        <pre class="pre-scrollable">https://www.example.org/api/FHIR/R4/DocumentReference?_id=eiuHBlNpiGxJwXRUWVqHjN9QaKO0ODrkAJrvUByRLuMc3&amp;_revInclude=Provenance:target
</pre>

<h5>Sample Response</h5>
<pre class="pre-scrollable">{
&nbsp;&nbsp;&nbsp;&nbsp;"link": [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"relation": "self",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"url": "https://www.example.org/api/FHIR/R4/Provenance/eC7YEH-XJufbPDHTDFWShEk2b9Ki98xTRyLbhFXwQ6sfcjhyfmeem2nFqdxZYHcS0FhROyzJUToN.o1UE0DQeXQ3"
&nbsp;&nbsp;&nbsp;&nbsp;}],
&nbsp;&nbsp;&nbsp;&nbsp;"fullUrl": "https://www.example.org/api/FHIR/R4/Provenance/eC7YEH-XJufbPDHTDFWShEk2b9Ki98xTRyLbhFXwQ6sfcjhyfmeem2nFqdxZYHcS0FhROyzJUToN.o1UE0DQeXQ3",
&nbsp;&nbsp;&nbsp;&nbsp;"resource": {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"resourceType": "Provenance",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"id": "eC7YEH-XJufbPDHTDFWShEk2b9Ki98xTRyLbhFXwQ6sfcjhyfmeem2nFqdxZYHcS0FhROyzJUToN.o1UE0DQeXQ3",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"target": [{"reference": "DocumentReference/eiuHBlNpiGxJwXRUWVqHjN9QaKO0ODrkAJrvUByRLuMc3"}],
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"recorded": "2020-06-23T07:33:12Z",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"agent": [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"type": {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"coding": [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"system": "http://terminology.hl7.org/CodeSystem/provenance-participant-type",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"code": "author",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"display": "Author"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}],
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"text": "Author"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"who": {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"reference": "Practitioner/eIOnxJsdFcfgJ4iy716I3MA3",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"display": "Starter Provider, MD"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"onBehalfOf": {"display": "Example Organization"}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"type": {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"coding": [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"system": "http://terminology.hl7.org/CodeSystem/provenance-participant-type",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"code": "transmitter",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"display": "Transmitter"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}],
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"text": "Transmitter"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"who": {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"display": "Example Organization"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}]
&nbsp;&nbsp;&nbsp;&nbsp;},
&nbsp;&nbsp;&nbsp;&nbsp;"search": {"mode": "include"}
}
</pre>
        <h3>AuditEvent</h3>
        <p></p>Epic currently supports using this parameter to fetch audit event data for search interactions with
        select FHIR resources starting in Epic version May 2021. AuditEvent returns audit event information for a
        specific resource instance. Examples of audited events include edits to an order or the signing of a note.
        AuditEvent is supported only for a subset of FHIR version R4 resources.<p></p>
        <p>Parameter format: <code>_revInclude=AuditEvent:entity</code></p>
        <p>The response will have the following format:</p>
        <table class="table table-hover">
            <tbody>
                <tr>
                    <td><strong>Element</strong>
                    </td>
                    <td>
                        <strong>Type</strong>
                    </td>
                    <td>
                        <strong>Cardinality</strong>
                    </td>
                    <td>
                        <strong>Description</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>type</p>
                    </td>
                    <td>
                        <p>Coding</p>
                    </td>
                    <td>
                        <p>1..1</p>
                    </td>
                    <td>
                        <p>Type or identifier of the event</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>recorded</p>
                    </td>
                    <td>
                        <p>Instant</p>
                    </td>
                    <td>
                        <p>1..1</p>
                    </td>
                    <td>
                        <p>Time when the event was recorded</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>agent</p>
                    </td>
                    <td>
                        <p>BackboneElement</p>
                    </td>
                    <td>
                        <p>1..*</p>
                    </td>
                    <td>
                        <p>Actors involved in the event</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>agent.role</p>
                    </td>
                    <td>
                        <p>CodeableConcept</p>
                    </td>
                    <td>
                        <p>0..*</p>
                    </td>
                    <td>
                        <p>Agent role in the event</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>agent.who</p>
                    </td>
                    <td>
                        <p>Reference (PractitionerRole | Practitioner | Organization | Device | Patient | RelatedPerson)
                        </p>
                        <p>&nbsp;</p>
                    </td>
                    <td>
                        <p>0..1</p>
                    </td>
                    <td>
                        <p>Identifier of who participated</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>agent.requestor</p>
                    </td>
                    <td>
                        <p>Boolean</p>
                    </td>
                    <td>
                        <p>1..1</p>
                    </td>
                    <td>
                        <p>Whether the user is the initiator</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>source</p>
                    </td>
                    <td>
                        <p>BackboneElement</p>
                    </td>
                    <td>
                        <p>1..1</p>
                    </td>
                    <td>
                        <p>The audit event reporter</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>source.observer</p>
                    </td>
                    <td>
                        <p>Reference (PractitionerRole | Practitioner | Organization | Device | Patient | RelatedPerson)
                        </p>
                    </td>
                    <td>
                        <p>1..1</p>
                    </td>
                    <td>
                        <p>The identity of the source detecting the event</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>entity</p>
                    </td>
                    <td>
                        <p>BackboneElement</p>
                    </td>
                    <td>
                        <p>0..*</p>
                    </td>
                    <td>
                        <p>Data or objects used</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>entity.what</p>
                    </td>
                    <td>
                        <p>Reference (Any)</p>
                    </td>
                    <td>
                        <p>0..1</p>
                    </td>
                    <td>
                        <p>Specific instance of the resource</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>entity.detail</p>
                    </td>
                    <td>
                        <p>BackboneElement</p>
                    </td>
                    <td>
                        <p>0..*</p>
                    </td>
                    <td>
                        <p>Additional information about the entity</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>entity.detail.type</p>
                    </td>
                    <td>
                        <p>String</p>
                    </td>
                    <td>
                        <p>1..1</p>
                    </td>
                    <td>
                        <p>Name of the property that changed</p>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <p>String</p>
                    </td>
                    <td>
                        <p>1..1</p>
                    </td>
                    <td>
                        <p>Property value</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>entity.detail extension (audit-event-detail-state)</p>
                    </td>
                    <td>
                        <p>Extension</p>
                    </td>
                    <td>
                        <p>0..1</p>
                    </td>
                    <td>
                        <p>Indicates whether the value in entity.detail.valueString is the value before or after the
                            audited event</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <h4>Examples:</h4>
        <h5>Sample Request</h5>
        <pre class="pre-scrollable">https://www.example.org/api/FHIR/R4/MedicationRequest?_id=eiuHBlNpiGxJwXRUWVqHjN9QaKO0ODrkAJrvUByRLuMc3&amp;_revInclude=AuditEvent:entity
</pre>

<h5>Sample Response</h5>
<pre class="pre-scrollable">{
"link": [{
"relation": "self",
"url": https://www.example.org/api/FHIR/R4/AuditEvent/erLtgh1AreI5Y0dE6YVZxVzXV3FaxnhsMWrsnBmt-JUcxF7D14JKfYlolngfGblRgIoJNa8UzOyGJ38eJBF3aKn94wMPR9m6M.LWkVmDWuRxpwYKzNEloKILbZHuXeidD3
}],
"fullUrl": https://www.example.org/api/FHIR/R4/AuditEvent/erLtgh1AreI5Y0dE6YVZxVzXV3FaxnhsMWrsnBmt-JUcxF7D14JKfYlolngfGblRgIoJNa8UzOyGJ38eJBF3aKn94wMPR9m6M.LWkVmDWuRxpwYKzNEloKILbZHuXeidD3,
"resource": {
"resourceType": "AuditEvent",
"id": "erLtgh1AreI5Y0dE6YVZxVzXV3FaxnhsMWrsnBmt-JUcxF7D14JKfYlolngfGblRgIoJNa8UzOyGJ38eJBF3aKn94wMPR9m6M.LWkVmDWuRxpwYKzNEloKILbZHuXeidD3",
"type": {
"system": "http://open.epic.com/FHIR/StructureDefinition/audit-event-type",
"code": "Modify Order",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "display": "Modify Order"
},
"recorded": "2020-12-09T21:00:03Z",
"agent": [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "role": [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "coding": [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "system": "http://open.epic.com/FHIR/StructureDefinition/audit-event-agent-role",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "code": "Ordering Provider",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "display": "Ordering Provider"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }]
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }],
"who": {
"reference": "Practitioner/eEISPNvH1RKgu1t69j6Bx2g3",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "display": "Starter Provider, MD"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; },
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "requestor": true
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }],
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "source": {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "observer": {"display": "Example Organization"}
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; },
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "entity": [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "what": {"reference": "MedicationRequest/eiuHBlNpiGxJwXRUWVqHjN9QaKO0ODrkAJrvUByRLuMc3"},
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "detail": [{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "type": "Comment",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "valueString": "Order Modified"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; },
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "type": "Administration instructions",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "valueString": "Administration Instructions edited"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }]
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "search": {"mode": "include"}
}
</pre>
    </div>


</div>
<div id="testpatients" style="display:none">
    <h3 class="body-header body-header-without-subtext">
        Sandbox Test Data
    </h3>
    <div>
        <p>
            These test users can be used to authenticate when testing Provider-Facing Standalone OAuth 2.0 workflows
            with the FHIR sandbox.
        </p>
        <p>
            <i>Sandbox URI - https://fhir.epic.com/interconnect-fhir-oauth/</i>
            <br>
            <i>FHIR Endpoints - <a href="https://open.epic.com/MyApps/Endpoints">See 'FHIR API Endpoints' on
                    open.epic</a></i>
        </p>
        <table class="table table-hover">
            <tbody>
                <tr>
                    <th width="25%" class="td-break-word">
                        <p>User</p>
                    </th>
                    <th width="25%" class="td-break-word">
                        <p>User Login</p>
                    </th>
                    <th width="25%" class="td-break-word">
                        <p>User Password</p>
                    </th>
                </tr>
                <tr>
                    <td width="25%" class="td-break-word">
                        <p>FHIR, USER</p>
                    </td>
                    <td width="25%" class="td-break-word">
                        <p>FHIR</p>
                    </td>
                    <td width="25%">
                        <p>EpicFhir11!</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p>
            These are some of the test patients that exist in the Epic on FHIR sandbox.
        </p>
        <p>
            <i>Sandbox URI - https://fhir.epic.com/interconnect-fhir-oauth/</i>
            <br>
            <i>FHIR Endpoints - <a href="https://open.epic.com/MyApps/Endpoints">See 'FHIR API Endpoints' on
                    open.epic</a></i>
        </p>
        <table class="table table-hover">
            <tbody>
                <tr>
                    <th width="25%" class="td-break-word">
                        <p>Patient</p>
                    </th>
                    <th width="50%" class="td-break-word">
                        <p>FHIR Identifier</p>
                    </th>
                    <th width="25%" class="td-break-word">
                        <p>Applicable Resources</p>
                    </th>
                </tr>
                <tr>
                    <td width="25%" class="td-break-word">
                        <p>Camila Lopez</p>
                    </td>
                    <td width="50%" class="td-break-word">
                        <p>FHIR: erXuFYUfucBZaryVksYEcMg3</p>
                        <p>External: Z6129</p>
                        <p>MRN: 203713</p>
                        <p>MyChart Login Username: fhircamila</p>
                        <p>MyChart Login Password: epicepic1</p>
                    </td>
                    <td width="25%">
                        <ul style="list-style: none;padding-left: 0;">
                            <li>DiagnosticReport</li>
                            <li>Medication</li>
                            <li>MedicationOrder</li>
                            <li>MedicationRequest</li>
                            <li>MedicationStatement</li>
                            <li>Observation (Labs)</li>
                            <li>Patient</li>
                            <li>Procedure</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td width="25%" class="td-break-word">
                        <p>Derrick Lin</p>
                    </td>
                    <td width="50%" class="td-break-word">
                        <p>FHIR: eq081-VQEgP8drUUqCWzHfw3</p>
                        <p>External: Z6127</p>
                        <p>MRN: 203711</p>
                        <p>MyChart Login Username: fhirderrick</p>
                        <p>MyChart Login Password: epicepic1</p>
                    </td>
                    <td width="25%">
                        <ul style="list-style: none;padding-left: 0;">
                            <li>CarePlan</li>
                            <li>Condition</li>
                            <li>Goal</li>
                            <li>Medication</li>
                            <li>MedicationOrder</li>
                            <li>MedicationRequest</li>
                            <li>MedicationStatement</li>
                            <li>Observation (Smoking History)</li>
                            <li>Patient</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td width="25%" class="td-break-word">
                        <p>Desiree Powell</p>
                    </td>
                    <td width="50%" class="td-break-word">
                        <p>FHIR: eAB3mDIBBcyUKviyzrxsnAw3</p>
                        <p>External: Z6130</p>
                        <p>MRN: 203714</p>
                        <p>MyChart Login Username: fhirdesiree</p>
                        <p>MyChart Login Password: epicepic1</p>
                    </td>
                    <td width="25%">
                        <ul style="list-style: none;padding-left: 0;">
                            <li>Immunization</li>
                            <li>Observation (Vitals)</li>
                            <li>Patient</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td width="25%" class="td-break-word">
                        <p>Elijah Davis</p>
                    </td>
                    <td width="50%" class="td-break-word">
                        <p>FHIR: egqBHVfQlt4Bw3XGXoxVxHg3</p>
                        <p>External: Z6125</p>
                        <p>MRN: 203709</p>
                    </td>
                    <td width="25%">
                        <ul style="list-style: none;padding-left: 0;">
                            <li>AllergyIntollerance</li>
                            <li>Binary</li>
                            <li>Condition</li>
                            <li>DocumentReference</li>
                            <li>Medication</li>
                            <li>MedicationOrder</li>
                            <li>MedicationRequest</li>
                            <li>MedicationStatement</li>
                            <li>Observation (Smoking History)</li>
                            <li>Patient</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td width="25%" class="td-break-word">
                        <p>Linda Ross</p>
                    </td>
                    <td width="50%" class="td-break-word">
                        <p>FHIR: eh2xYHuzl9nkSFVvV3osUHg3</p>
                        <p>External: Z6128</p>
                        <p>MRN: 203712</p>
                    </td>
                    <td width="25%">
                        <ul style="list-style: none;padding-left: 0;">
                            <li>Condition</li>
                            <li>Medication</li>
                            <li>MedicationOrder</li>
                            <li>MedicationRequest</li>
                            <li>MedicationStatement</li>
                            <li>Observation (Vitals)</li>
                            <li>Patient</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td width="25%" class="td-break-word">
                        <p>Olivia Roberts</p>
                    </td>
                    <td width="50%" class="td-break-word">
                        <p>FHIR: eh2xYHuzl9nkSFVvV3osUHg3</p>
                        <p>External: Z6131</p>
                        <p>MRN: 203715</p>
                    </td>
                    <td width="25%">
                        <ul style="list-style: none;padding-left: 0;">
                            <li>Binary</li>
                            <li>Condition</li>
                            <li>Device</li>
                            <li>DocumentReference</li>
                            <li>Patient</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td width="25%" class="td-break-word">
                        <p>Warren McGinnis</p>
                    </td>
                    <td width="50%" class="td-break-word">
                        <p>FHIR: e0w0LEDCYtfckT6N.CkJKCw3</p>
                        <p>External: Z6126</p>
                        <p>MRN: 203710</p>
                    </td>
                    <td width="25%">
                        <ul style="list-style: none;padding-left: 0;">
                            <li>AllergyIntollerance</li>
                            <li>Binary</li>
                            <li>Condition</li>
                            <li>DiagnosticReport</li>
                            <li>DocumentReference</li>
                            <li>Observation (Labs)</li>
                            <li>Observation (Vitals)</li>
                            <li>Patient</li>
                            <li>Procedure</li>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>
        <p>
            If you're using the legacy DSTU2 sandbox for testing, here are a few patients you can check out.
        </p>
        <p>
            <i>Sandbox URI - https://open-ic.epic.com/argonaut/</i>
            <br>
            <i>FHIR Endpoints - <a href="https://open.epic.com/MyApps/Endpoints">See 'FHIR API Endpoints' on
                    open.epic</a></i>
        </p>
        <table class="table table-hover">
            <tbody>
                <tr>
                    <th width="25%" class="td-break-word">
                        <p>Patient</p>
                    </th>
                    <th width="50%" class="td-break-word">
                        <p>Account Details</p>
                    </th>
                    <th width="25%" class="td-break-word">
                        <p>Applicable Resources</p>
                    </th>
                </tr>
                <tr>
                    <td width="25%" class="td-break-word">
                        <p>Jessica Argonaut</p>
                    </td>
                    <td width="50%" class="td-break-word">
                        <p>FHIR: TUKRxL29bxE9lyAcdTIyrWC6Ln5gZ-z7CLr2r-2SY964B</p>
                        <p>MyChart Login Username: fhirjessica</p>
                        <p>MyChart Login Password: n/a</p>
                        <p>Note: This account is only enabled as a proxy account</p>
                    </td>
                    <td width="25%">
                        <ul style="list-style: none;padding-left: 0;">
                            <li>Patient</li>
                            <li>AllergyIntollerance</li>
                            <li>MedicationOrder</li>
                            <li>MedicationStatement</li>
                            <li>Condition</li>
                            <li>DiagnosticReport</li>
                            <li>DocumentReference</li>
                            <li>Observation</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td width="25%" class="td-break-word">
                        <p>Jason Argonaut</p>
                    </td>
                    <td width="50%" class="td-break-word">
                        <p>FHIR: Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB</p>
                        <p>MyChart Login Username: fhirjason</p>
                        <p>MyChart Login Password: epicepic1</p>
                        <p>Note: This account has proxy access to fhirjessica</p>
                    </td>
                    <td width="25%">
                        <ul style="list-style: none;padding-left: 0;">
                            <li>Patient</li>
                            <li>AllergyIntollerance</li>
                            <li>MedicationOrder</li>
                            <li>MedicationStatement</li>
                            <li>Condition</li>
                            <li>DiagnosticReport</li>
                            <li>Immunization</li>
                            <li>CarePlan</li>
                            <li>Goal</li>
                            <li>Procedure</li>
                            <li>DocumentReference</li>
                            <li>Observation</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td width="25%" class="td-break-word">
                        <p>Daisy Tinsley</p>
                    </td>
                    <td width="50%" class="td-break-word">
                        <p>FHIR: TbsWqA46u3BtlZT2KSkx5cZOT1o0mGbI3VPAGXv6XcdEB</p>
                        <p>MyChart Login Username: fhirdaisy</p>
                        <p>MyChart Login Password: epicepic1</p>
                    </td>
                    <td width="25%">
                        <ul style="list-style: none;padding-left: 0;">
                            <li>Patient</li>
                            <li>AllergyIntollerance</li>
                            <li>MedicationOrder</li>
                            <li>MedicationStatement</li>
                            <li>Condition</li>
                            <li>DiagnosticReport</li>
                            <li>Immunization</li>
                            <li>CarePlan</li>
                            <li>Procedure</li>
                            <li>DocumentReference</li>
                            <li>Observation</li>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>


</div>
<div id="patientfacingfhirapps" style="display:none">
    <h3 class="body-header body-header-without-subtext">
        Patient-Facing Apps Using FHIR
    </h3>
    <div>
        <h2>FHIR API Response Variations in Patient-Facing Applications</h2>
        <p>Some FHIR APIs may return different data when the application is used in a patient-facing context. This
            typically occurs because data that should not be visible to a patient is filtered out of FHIR API responses
            in patient-facing applications. Some filtering criteria is configurable by the community member such that
            two community members could filter the same data in different ways. Below are some examples of possible
            differences in the results returned by a FHIR API in a patient context:</p>
        <ul>
            <li>FHIR APIs will only return results relevant to the authenticated patient. If one patient, Susan, is
                logged in to the application, the Observation (Labs) API won’t return lab results for a patient other
                than Susan.</li>
            <li>FHIR APIs may not return patient-entered data that has not yet been reviewed and reconciled by a
                clinician.</li>
            <li>FHIR APIs may return patient friendly names for resources, such as medications. For example, a
                patient-facing app may return the medication name <code>pseudoephedrine</code> instead of
                <code>PSEUDOPHEDRINE HCL 30 MG PO TABS</code>.</li>
            <li>FHIR APIs may be configured to exclude specific lab results to comply with state and local regulations
            </li>
        </ul>
        <p>Community members may choose to disable this filtering functionality for their Epic instance, resulting in
            near identical behavior between patient-facing FHIR API responses and the response of apps with other
            audiences. </p>
        <p>Epic recommends that you thoroughly test each API at each Community Member’s site prior to a go-live in
            production to ensure that you understand the behavior of each API for that community member’s Epic
            deployment.
</p>
<h2>Automatic Client ID Distribution</h2>
<h3>Automatic Client ID Distribution: USCDI Apps</h3>
<p>Client IDs for USCDI apps will automatically download to a community member’s Epic instances when all of the
    following conditions are met:</p>
<ul>
            <li>The application:</li>
            <ul>
                <li>Is created through Epic on FHIR</li>
                <li>Uses only FHIR APIs (of any supported version) listed within the <a
                        href="https://www.healthit.gov/isa/united-states-core-data-interoperability-uscdi">USCDI v1</a>
                    core data set</li>
                <li>Only reads data from Epic</li>
                <li>Is patient-facing</li>
                <li>Does not use refresh tokens <b>OR</b> uses refresh tokens and <a
                        href="https://fhir.epic.com/Documentation?docId=epiconfhirrequestprocess&amp;section=devsecret">has
                        a client secret uploaded by the vendor for that community member</a></li>
                <li>Is marked "Ready for Production" and was marked ready after Sept. 3rd, 2020</li>
                <ul>
                    <li>Apps can be marked "Ready for Sandbox Use" to test with our Epic on FHIR environment prior to
                        marking the app "Ready for Production"</li>
                    <li>We recommend that any apps marked "Ready for Production" will be tested with a customer's
                        non-production environment before going live</li>
                </ul>
            </ul>
            <li>The Community Member:</li>
            <ul>
                <li>Has signed the open.epic API Subscription Agreement</li>
                <li>Has not disabled auto-download functionality</li>
            </ul>
        </ul>

<h3>Automatic Client ID Distribution: CCDS (MU3) Apps</h3>
        <p>Client IDs for CCDS apps will automatically download to a community member’s Epic instances when all of the
            following conditions are met:</p>
        <ul>
            <li>The application:</li>
            <ul>
                <li>Is created through Epic on FHIR</li>
                <li>Uses only <b>DSTU2</b> FHIR APIs †</li>
                <li>Only reads data from Epic</li>
                <li>Is patient-facing</li>
                <li>Does not use refresh tokens</li>
                <li>Is marked "Ready for Production" and was marked ready after Dec. 17th, 2020</li>
                <ul>
                    <li>Apps can be marked "Ready for Sandbox Use" to test with our Epic on FHIR environment prior to
                        marking the app "Ready for Production"</li>
                    <li>We recommend that any apps marked "Ready for Production" will be tested with a customer's
                        non-production environment before going live</li>
                </ul>
            </ul>
            <li>The Community Member:</li>
            <ul>
                <li><b>Need not</b> sign the open.epic API Subscription Agreement</li>
            </ul>
        </ul>

<h3>Automatic Client ID Distribution: CMS Payor Apps</h3>
        <p>Client IDs for apps meant to meet CMS payor requirements will automatically download to a community member's
            Epic instances when all of the following conditions are met:</p>
        <ul>
            <li>The application:
                <ul>
                    <li>Is created through Epic on FHIR</li>
                    <li>Uses only ExplanationOfBenefit (of any supported version) and/or any FHIR APIs (of any supported
                        version) listed within the <a
                            href="https://www.healthit.gov/isa/united-states-core-data-interoperability-uscdi">USCDI
                            v1</a> core data set </li>
                    <li>Only reads data from Epic</li>
                    <li>Is patient-facing</li>
                    <li>Uses OAuth 2.0</li>
                    <li>Is marked "Ready for Production"
                        <ul>
                            <li>Apps can be marked "Ready for Sandbox Use" to test with our Epic on FHIR environment
                                prior to marking the app "Ready for Production"</li>
                            <li>We recommend that any apps marked "Ready for Production" will be tested with a
                                customer's non-production environment before going live</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>The Community Member:
                <ul>
                    <li>Has signed the open.epic API Subscription Agreement</li>
                    <li>Has enabled this auto-download functionality</li>
                </ul>
            </li>
        </ul>

<h2>Manual Client ID Distribution</h2>
        <p>Any applications that do not meet all of the above criteria will need to be manually downloaded to the
            community member's Epic instances. Steps to do so can be found in the <a style="cursor: pointer"
                onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                onclick="Epic$USCDI$Dev$ShowDocument(&quot;epiconfhirrequestprocess&quot;)" tabindex="0">App Creation
                and Request Process</a> guide.</p>
        <br><br><br>
        <p>† Only the following DSTU2 FHIR APIs qualify for MU3 auto-download.</p>
        <ul>
            <li>AllergyIntolerance.Read</li>
            <li>AllergyIntolerance.Search</li>
            <li>Binary.Read</li>
            <li>CarePlan.Read</li>
            <li>CarePlan.Search</li>
            <li>Condition.Read</li>
            <li>Condition.Search</li>
            <li>Device.Read</li>
            <li>Device.Search</li>
            <li>DiagnosticReport.Read</li>
            <li>DiagnosticReport.Search</li>
            <li>DocumentReference.Read</li>
            <li>DocumentReference.Search</li>
            <li>Goal.Read</li>
            <li>Goal.Search</li>
            <li>Immunization.Read</li>
            <li>Immunization.Search</li>
            <li>Medication.Read</li>
            <li>Medication.Search</li>
            <li>MedicationOrder.Read</li>
            <li>MedicationOrder.Search</li>
            <li>MedicationStatement.Read</li>
            <li>MedicationStatement.Search</li>
            <li>Observation.Read</li>
            <li>Observation.Search</li>
            <li>Patient.Read</li>
            <li>Patient.Search</li>
            <li>Practitioner.Read</li>
            <li>Practitioner.Search</li>
            <li>Procedure.Read</li>
            <li>Procedure.Search</li>
        </ul>
    </div>


</div>
<div id="vaccinecredential" style="display:none">
    <h3 class="body-header body-header-without-subtext">
        Vaccine Credentials
    </h3>
    <div>
        <section class="alert alert-info">
            <p>Epic's support for vaccine credentials using the SMART Health Cards specification is evolving. The
                content on this page might change without notice.</p>
        </section>
        <ul>
            <li><a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;,&quot;vc-whatisavaccinecredential&quot;)">What's
                    a "Vaccine Credential?</a>
                <ul>
                    <li><a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-relatedterms&quot;)">Related
                            Terms &amp; Definitions</a></li>
                    <li><a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-audience&quot;)">Audience
                            and Intent</a></li>
                </ul>
            </li>
            <li><a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-whatsasmarthealthcard&quot;)">What's
                    a SMART Health Card?</a>
                <ul>
                    <li><a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-accessing&quot;)">Accessing
                            Epic-Generated SMART Health Cards</a></li>
                    <li><a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-reading&quot;)">Reading
                            SMART Health Cards</a>
                        <ul>
                            <li><a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                                    tabindex="0"
                                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-qrcodes&quot;)">QR
                                    codes</a>
                                <ul>
                                    <li><a style="cursor: pointer"
                                            onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-numericaldecoding&quot;)">Numerical
                                            Decoding</a></li>
                                    <li><a style="cursor: pointer"
                                            onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-chunkedqrcodes&quot;)">Chunked
                                            QR Codes</a></li>
                                </ul>
                            </li>
                            <li><a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                                    tabindex="0"
                                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-smarthealthcardfiles&quot;)">.smart-health-card
                                    Files</a></li>
                        </ul>
                    </li>
                    <li><a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-consumingsmarthealthcards&quot;)">Consuming
                            SMART Health Cards</a>
                        <ul>
                            <li><a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                                    tabindex="0"
                                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-joseheader&quot;)">JOSE
                                    Header</a></li>
                            <li><a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                                    tabindex="0"
                                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-payload&quot;)">Payload</a>
                                <ul>
                                    <li><a style="cursor: pointer"
                                            onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-inflation&quot;)">Inflation</a>
                                    </li>
                                    <li><a style="cursor: pointer"
                                            onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-smarthealthcardheader&quot;)">SMART
                                            Health Card Header</a></li>
                                    <li><a style="cursor: pointer"
                                            onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-fhirbundle&quot;)">FHIR
                                            Bundle</a></li>
                                    <li><a style="cursor: pointer"
                                            onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-fhirpatient&quot;)">FHIR
                                            Patient</a>
                                        <ul>
                                            <li><a style="cursor: pointer"
                                                    onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                                                    tabindex="0"
                                                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-considerationsforpatientidentity&quot;)">Considerations
                                                    For Patient Identity</a></li>
                                        </ul>
                                    </li>
                                    <li><a style="cursor: pointer"
                                            onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-fhirimmunization&quot;)">FHIR
                                            Immunization</a>
                                        <ul>
                                            <li><a style="cursor: pointer"
                                                    onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                                                    tabindex="0"
                                                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-fhirimmunizationexample&quot;)">Example
                                                    Immunization</a></li>
                                        </ul>
                                    </li>
                                    <li><a style="cursor: pointer"
                                            onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-fhirobservation&quot;)">FHIR
                                            Observation</a>
                                        <ul>
                                            <li><a style="cursor: pointer"
                                                    onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                                                    tabindex="0"
                                                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-fhirobservationexample&quot;)">Example
                                                    Observation</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                                    tabindex="0"
                                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-trust&quot;)">SMART
                                    Health Cards &amp; Trust</a>
                                <ul>
                                    <li><a style="cursor: pointer"
                                            onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                                            onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-verifythesignedwebtoken&quot;)">Verify
                                            the Signed Web Token</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a style="cursor: pointer" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" tabindex="0"
                    onclick="Epic$USCDI$Dev$ShowDocument(&quot;vaccinecredential&quot;, &quot;vc-librariesandreferences&quot;)">Open
                    Source Libraries &amp; References</a></li>
        </ul>
        <h1 id="vc-whatisavaccinecredential" class="section-link">What's a "Vaccine Credential"?</h1>
        <p>
            A vaccine credential binds an individual’s demographic information with information about their lab tests or
            vaccine history into a package that is machine-readable and digitally signed by the issuer. Unlike a paper
            vaccine card, a vaccine credential is verifiable and therefore much more difficult to tamper with or
            counterfeit.
        </p>
        <img src="./Documentation - Epic on FHIR_files/CDC-COVID-vaccine-card.jpg"
            alt="A CDC vaccine card, widely issued in the United States to patients when they receive a COVID-19 vaccination.">
        <p class="image-label">A CDC vaccine card, widely issued in the United States to patients when they receive a
            COVID-19 vaccination.</p>
        <h2 id="vc-relatedterms" class="section-link">Related Terms &amp; Definitions</h2>

        <ul>
            <li>Vaccine or immunity passport. Passports are issued by governments. An immunity passport asserts
                immunity. In contrast, a vaccine credential is just an alternative way of representing information
                documented about a patient--specifically, their vaccination or lab history.</li>
            <li>Health Wallet. An app available for install and use on a consumer's device that aggregates and stores
                vaccine credentials from different sources. A health wallet is a convenient way to store multiple
                credentials, but is not strictly required.</li>
            <li>Pass App. An app available for install and use on a consumer's device that reads one or more vaccine
                credentials to determine if the user meets entry criteria for a specific venue or building, for example.
                might also perform some form of identity verification and might also commonly have health wallet-type
                functionality. A pass app might be useful in creating "fast lanes" in which a consumer's identity would
                not need to be re-verified, but a verifier should not require the patient to have a pass app.</li>
            <li>Verifier. A consumer of a SMART Health Card.</li>
        </ul>

        <h2 id="vc-audience" class="section-link">Audience and Intent</h2>

        <p>This document was designed to support health wallets, pass apps, and verifiers, in designing apps and
            technologies that can consume a patient-provided SMART Health Card. We note particularly that any developer
            creating a pass app should also consider how verifiers using that pass app will interact with patients who
            do not have access to technology and present only with their ID and a printed SMART Health Card. Any
            verifier that <em>requires</em> patients to download a specific type of pass app (and thus, cannot read and
            interact with printed SMART Health Cards) is disadvantaging individuals without technology. SMART Health
            Cards were specifically designed to provide patient access regardless of technological ability or
            sophistication.</p>
        <p>In other words, if you require an individual to download and present a certain type of app to enter your
            venue, you have not implemented the specification correctly.</p>

        <h1 id="vc-whatsasmarthealthcard" class="section-link">What's a SMART Health Card?</h1>

        <p>A SMART Health Card is a standard for representing a vaccine credential. The SMART Health Cards standard is
            open, interoperable, and built on top of HL7 FHIR and compatible with W3C's Verifiable Credentials. It's
            designed to be:</p>
        <ul>
            <li>Open – The standard is open source.</li>
            <li>Interoperable – It's built on top of FHIR.</li>
            <li>Privacy-preserving – Clinical content is embedded within SMART Health Cards, ensuring that a patient
                can't be tracked based upon where they use their vaccine credential.</li>
            <li>Equitable – It can be represented entirely on paper.</li>
        </ul>
        <p>A SMART Health Card contains very limited patient demographics, and either information about the patient's
            COVID-19 immunization or COVID-19 lab result.</p>
        <p>Technically, a SMART Health Card is a JSON object containing a JWS of compressed JSON, consisting of a FHIR
            Patient and either one or two FHIR Immunizations; or a FHIR Observation resource. The card can be
            represented as a file ending in the <code>.smart-health-card</code> extension, or as a QR code printed on
            paper or displayed digitally.</p>
        <p>You can learn more about SMART Health Cards by visiting the following websites:</p>
        <ul>

            <li><a href="https://vci.org/">https://vci.org/</a></li>
            <li><a href="https://smarthealth.cards/">https://smarthealth.cards/</a></li>
            <li><a
                    href="http://build.fhir.org/ig/dvci/vaccine-credential-ig/branches/main/">http://build.fhir.org/ig/dvci/vaccine-credential-ig/branches/main/</a>
            </li>
        </ul>

        <h2 id="vc-accessing" class="section-link">Accessing Epic-Generated SMART Health Cards</h2>

        <p>Epic generates SMART Health Cards in two different ways and via a few different workflows:</p>
        <ol>
            <li>Patients can access SMART Health Cards as either a QR code displayed in or a file downloaded from the
                MyChart website or the MyChart mobile apps.</li>
            <li>Health systems using Epic can generate and print a QR code containing a patient's SMART Health Card in
                the clinician-facing interface, Hyperspace. Typically, this workflow would be used to provide SMART
                Health Cards to patients without access to MyChart.</li>
        </ol>
        <p>Patients can share QR codes either directly from a mobile device or from a piece of paper. You should test
            and validate that your app can consume the QR code from both a mobile device and from a piece of paper.</p>
        <p>A patient can download their SMART Health Card from MyChart onto their device. If you're developing an app
            that can run on a patient's device, alongside MyChart, you should associate your app with the
            <code>.smart-health-card</code> filename extension.</p>

        <h2 id="vc-reading" class="section-link">Reading SMART Health Cards</h2>
        <h3 id="vc-qrcodes" class="section-link">QR codes</h3>

        <p>Every SMART Health Card can be represented as a QR code. Typical QR scanner apps expect the QR code to
            contain a URL that can be navigated to in a browser, but a SMART Health Card's QR code does not contain an
            http:// URL. Rather, the content begins with an shc:/ prefix, followed by a series of numbers and optionally
            forward slashes (/).</p>
        <p>For example, a SMART Health Card QR code could contain this example content after it is scanned:</p>
        <p><tt></tt></p>
        <pre><tt>shc:/567629595326546034602925407728043360287028647167452228092864456652064567322455425662713655612967382737094440447142272560543945364126252454415527384355065405373633654575390303603122290952432060346029243740446057360106413733531270742335036958065345203372766522453157257438663242742309774531624034227003622673375938313137693259503100426236772203355605036403580035610300766508583058374270060532535258081003347207250832110567524430725820265707614406667441643124353569754355376170447732541065607642436956282976307276247374426657326827736143256004577460541121230968766729300406344232537345706367327527360526620626030777047434662721590753712525295755717157712404305365553050007570070626252762326973342068033331323970433259453532396071684011083877646011585542067525276750403569746574042500432127583065602700296834605068374126617637383032031153273532295552400564655669374044767208283743690777676126004361576025612868262976107611767042442744504570773124097729726134647000600635240740664475562634400569324229237641072230287177047474320721676355073560542058377743647371335212506320077300352940590840740926502974284434626571632106437507760408126227344173035234272224650859284424550330072738216709675476296859290842564163266200100030407726503740243343666063287100680677296841286766396657316871405227413263621127776161647443056825046903656367560042552754107309332612290435433150635250555600416734505731035241562857000866406707250066585029295368623020723771290300716970507307715829230409632369722208220022421235364507226273407623637673266125102552327255034322275824010800676966507066627420335210701037520576422230612371756123652765287762007444002131666470397727224500564164325437330542095250237703346428534576030771757133406430361224385420</tt></pre>
        <p></p>
        <p>The numbers following the shc:/ prefix are the content of the SMART Health Card and must be numerically
            decoded.</p>


        <h4 id="vc-numericaldecoding" class="section-link">Numerical Decoding</h4>

        <p>SMART Health Cards' QR codes represent characters as their ASCII code. The SMART Health Cards specification
            defines a simple algorithm to represent each character from the JWS as a two-digit number. Following the
            prefix, each pair of digits is converted into a single character by adding 45 to determine the encoded
            letter that the integer represents.</p>
        <p>For example, the first pair of digits in the numerically encoded SMART Health Card example above is 56. By
            adding our offset constant 45 to 56, the value translates to ASCII's lower-case e (Chr(101) = "e"). The
            constant 45 was chosen because 45 is the ASCII value for "-", which is the lowest character that can exist
            in our Base64-URL encoded content.</p>
        <p>Decoding the string proceeds as follows:</p>
        <p>56 + 45 = e</p>
        <p>76 + 45 = y</p>
        <p>29 + 45 = J</p>
        <p><em>Etc.</em></p>

        <p>When parsing is complete, the above shc:/ string resolves to the following JWS:</p>
        <p><tt></tt></p>
        <pre><tt>eyJhbGciOiJFUzI1NiIsImtpZCI6ImZoa3ZpMEdWektQdjJpSHR6YUYtWHFicTZQVGFEcVdHSXd3c2RQNnZxT00iLCJ6aXAiOiJERUYifQ.3VRNb9swDP0rg3bZANuynCZLfFwSoMWwD6zZLkUOCs0kGvRhSLLRrMh_L-WkQzC0Pe20m0g-Pj0-yn5gKgRWs32Mbag570Ou4F5M82paYKugAGf4jY3owVmLEPPrxXdRjsYzMc7niyWXreIJyKuyEvwWofMqHvjXFi1fwic8BD6qypJK13OWMbvZslpMxHQ2Gk3G04z1wOoHBh4btFFJfdttftE1KbndK_-xs43GFHkMrvOAq0NLMTsXMhZPMTitqU85Szmi8gdW3xFHp_UPrwnw1F-XBHgKniH-JqOi_qRVGjyRSKM08bHPMJdaU2mnerRUYyu5IRXr4zpjG-XjfiFjIqGJy7y8ysWYHY_ZszLE6zJujOms-i3PE4UoYxeGOU2rMWJDyV4CKItz1wwM4Bpld4PicAgRzXmvtNa9_lA4v-PJUh5Uw6G_JwIYOkntlB3Xx4y159kHOVv0aOHCEn5hIYEd0K4HSBp6pcyJqhJ5WeVlGk-7-KUzG_RUENXoilIt-q3zJqVIpoTofLqtUaHVMlk8HzjjmwX2qF1r0nlpe-WdHc7v6NG9J1PXL_la_de-VpO_fL0aVeIf-5oUp4F-og_JJbqkKAuRtJ0-trs_v4tgJD16lDruC5C-CW9PQZ4CkvUyDlyvGjF7FaMud0XCHgE.5-pro_sokwANa7s7Ra2yWCKjDtxjDnHnIzk-wY-BLomsTzHCZ-eVmMcRN2W6a_Dz0OmIbZy04txtNUmKQ9EScA</tt></pre>
        <p></p>
        <p>See the SMART Health Cards specification for <a
                href="https://smarthealth.cards/#every-health-card-can-be-embedded-in-a-qr-code">an alternate
                explanation</a> of numerical encoding.</p>

        <h4 id="vc-chunkedqrcodes" class="section-link">Chunked QR Codes</h4>
        <p>In some cases, the underlying demographic and clinical data stored in a SMART Health Card QR code might
            simply be too large for a single QR code. If so, the SMART Health Card is "chunked" -- split across multiple
            QR codes that must be scanned and reassembled before any data can be accessed. In this case, the shc:/
            prefix is appended with two additional values, delimited by forward slashes ("/"). The first value defines
            the order of the current QR code for reassembly and the second is the total number of QR codes required to
            reassemble the data.</p>

        <p>For example, a SMART Health Card of 3500 characters is split across three distinct QR codes. The prefix of
            the first QR code will be: shc:/1/3/, the second: shc:/2/3/ and the third: shc:/3/3/. The contents of all
            three QR codes (following the final forward slash) must be concatenated in order before inflation.</p>

        <p>Requiring multiple QR codes to receive a single SMART Health Card is undesirable and likely confusing to
            consumers. Epic will rarely place so much information in a single SMART Health Card such that chunking the
            data across multiple QR codes is necessary. However, <em>apps consuming these QR codes must be prepared to
                scan and assemble multiple QR codes</em>.</p>

        <h3 id="vc-smarthealthcardfiles" class="section-link">.smart-health-card Files</h3>

        <p>In addition to presenting a QR code to a scanner, a consumer can also download a file to share a SMART Health
            Card. In this case, there's no QR code, no numeric encoding, and no chunking. The SMART Health Card is
            represented as a JWS wrapped in a simple JSON array. The filename extension of this file will always be
            ".smart-health-card".</p>
        <p>Here's an example of a <code>.smart-health-card</code> file containing a single SMART Health Card:</p>
        <p>
            <tt>
            </tt>
        </p>
        <pre><tt>
{
  "verifiableCredential": [
"eyJhbGciOiJFUzI1NiIsImtpZCI6ImZoa3ZpMEdWektQdjJpSHR6YUYtWHFicTZQVGFEcVdHSXd3c2RQNnZxT00iLCJ6aXAiOiJERUYifQ.3VRNb9swDP0rg3bZANuynCZLfFwSoMWwD6zZLkUOCs0kGvRhSLLRrMh_L-WkQzC0Pe20m0g-Pj0-yn5gKgRWs32Mbag570Ou4F5M82paYKugAGf4jY3owVmLEPPrxXdRjsYzMc7niyWXreIJyKuyEvwWofMqHvjXFi1fwic8BD6qypJK13OWMbvZslpMxHQ2Gk3G04z1wOoHBh4btFFJfdttftE1KbndK_-xs43GFHkMrvOAq0NLMTsXMhZPMTitqU85Szmi8gdW3xFHp_UPrwnw1F-XBHgKniH-JqOi_qRVGjyRSKM08bHPMJdaU2mnerRUYyu5IRXr4zpjG-XjfiFjIqGJy7y8ysWYHY_ZszLE6zJujOms-i3PE4UoYxeGOU2rMWJDyV4CKItz1wwM4Bpld4PicAgRzXmvtNa9_lA4v-PJUh5Uw6G_JwIYOkntlB3Xx4y159kHOVv0aOHCEn5hIYEd0K4HSBp6pcyJqhJ5WeVlGk-7-KUzG_RUENXoilIt-q3zJqVIpoTofLqtUaHVMlk8HzjjmwX2qF1r0nlpe-WdHc7v6NG9J1PXL_la_de-VpO_fL0aVeIf-5oUp4F-og_JJbqkKAuRtJ0-trs_v4tgJD16lDruC5C-CW9PQZ4CkvUyDlyvGjF7FaMud0XCHgE.5-pro_sokwANa7s7Ra2yWCKjDtxjDnHnIzk-wY-BLomsTzHCZ-eVmMcRN2W6a_Dz0OmIbZy04txtNUmKQ9EScA"
  ]
}
</tt></pre><tt>
        </tt>

        <p></p>
        <p>The <code>.smart-health-card</code> files downloaded from MyChart web or mobile will contain one or two
            entries in the verifiableCredential JSON array. Each array entry is a self-contained SMART Health Card.</p>

        <h2 id="vc-consumingsmarthealthcards" class="section-link">Consuming SMART Health Cards</h2>

        <p>Regardless of how a SMART Health Card is received -- a QR code scan or a file download, the same steps are
            used to verify and access the information contained within it.</p>
        <p>A JWS, including our SMART Health Card JWS, is made up of three, Base64-encoded parts:&nbsp; JOSE header, a
            payload, and a signature. Per RFC 7515, these three parts are concatenated together with periods.</p>

        <h3 id="vc-joseheader" class="section-link">JOSE Header</h3>

        <p>The JOSE (Javascript Object Signing and Encryption) Header identifies the algorithms used to sign and
            compress the payload and a unique identifier of the public key to be used to verify the signature.</p>
        <p>For example, the JOSE header from the above JWS is:</p>
        <p><tt>eyJhbGciOiJFUzI1NiIsImtpZCI6ImZoa3ZpMEdWektQdjJpSHR6YUYtWHFicTZQVGFEcVdHSXd3c2RQNnZxT00iLCJ6aXAiOiJERUYifQ</tt>
        </p>
        <p>Upon Base64-decoding it (and formatting for readability):</p>
        <p><tt></tt></p>
        <pre><tt>{
"alg": "ES256",
"kid": "fhkvi0GVzKPv2iHtzaF-Xqbq6PTaDqWGIwwsdP6vqOM",
"zip": "DEF"
}
</tt></pre>
        <p></p>
        <ul>
            <li><code>alg</code> – the algorithm used to sign the JWS.</li>
            <li><code>kid</code> – a "key identifier" which is used to find the public key for validating the signature.
            </li>
            <li><code>zip</code> – the compression algorithm used to deflate the payload. Note that the JWS standard
                does not define the zip header; the SMART Health Card uses borrows it from the closely related JWT
                standard.</li>
        </ul>
        <p>In the SMART Health Card standard, the alg header will always be "ES256" and the zip header will always be
            "DEF". In Epic's implementation of the standard, only the kid header will change, as it uniquely identifies
            the public key used by the healthcare delivery organization that issued the SMART Health Card. The public
            key may be rotated periodically.</p>

        <h3 id="vc-payload" class="section-link">Payload</h3>

        <p>The payload of a SMART Health Card JWS (that is, the second of the three period-delimited pieces in the JWS)
            is base64-encoded, compressed JSON. The JSON is compressed with the DEFLATE algorithm.</p>

        <h4 id="vc-inflation" class="section-link">Inflation</h4>

        <p>The DEFLATE algorithm is open source and has significant library support. DEFLATE is the same compression
            algorithm used by gzip but doesn't make use of the gzip header and footer. See <a
                href="https://www.ietf.org/rfc/rfc1951.txt">RFC 1951</a> for more information about this compression
            algorithm.</p>
        <p>As an example, starting with this JWS payload from our example above, &nbsp;</p>
        <p><tt></tt></p>
        <pre><tt>3VRNb9swDP0rg3bZANuynCZLfFwSoMWwD6zZLkUOCs0kGvRhSLLRrMh_L-WkQzC0Pe20m0g-Pj0-yn5gKgRWs32Mbag570Ou4F5M82paYKugAGf4jY3owVmLEPPrxXdRjsYzMc7niyWXreIJyKuyEvwWofMqHvjXFi1fwic8BD6qypJK13OWMbvZslpMxHQ2Gk3G04z1wOoHBh4btFFJfdttftE1KbndK_-xs43GFHkMrvOAq0NLMTsXMhZPMTitqU85Szmi8gdW3xFHp_UPrwnw1F-XBHgKniH-JqOi_qRVGjyRSKM08bHPMJdaU2mnerRUYyu5IRXr4zpjG-XjfiFjIqGJy7y8ysWYHY_ZszLE6zJujOms-i3PE4UoYxeGOU2rMWJDyV4CKItz1wwM4Bpld4PicAgRzXmvtNa9_lA4v-PJUh5Uw6G_JwIYOkntlB3Xx4y159kHOVv0aOHCEn5hIYEd0K4HSBp6pcyJqhJ5WeVlGk-7-KUzG_RUENXoilIt-q3zJqVIpoTofLqtUaHVMlk8HzjjmwX2qF1r0nlpe-WdHc7v6NG9J1PXL_la_de-VpO_fL0aVeIf-5oUp4F-og_JJbqkKAuRtJ0-trs_v4tgJD16lDruC5C-CW9PQZ4CkvUyDlyvGjF7FaMud0XCHgE</tt></pre>
        <p></p>
        <p>we Base64 URL-decode and inflate to arrive at the actual JSON content. The JSON of a SMART Health Card is
            made up of a header and a few FHIR resources.</p>

        <h4 id="vc-smarthealthcardheader" class="section-link">SMART Health Card Header</h4>

        <p>A SMART Health Card's header elements identify if the card contains immunization or lab result information, a
            unique url for the organization that issued the card and the time at which the card was issued.</p>
        <ul>
            <li><code>iss</code> – Append /.well-known/jwks.json to this url to access the JSON Wek Key Set containing a
                public key for verification of the JWS signature. See <a
                    href="https://fhir.epic.com/Documentation?docId=oauth2#trust">SMART Health Cards &amp; Trust</a> for
                additional information.</li>
            <li><code>nbf</code> – The time at which the SMART Health Card was issued (per RFC 7519). Epic generates
                SMART Health Cards on-demand, so this timestamp will correspond to the time at which the card was
                accessed by the patient.</li>
            <li><code>type</code> – Identifies the SMART Health Card as containing either COVID-19 vaccination or lab
                result. Epic-generated SMART Health Cards will always be one of the following:
                <ul>
                    <li><tt>["https://smarthealth.cards#health-card", "https://smarthealth.cards#covid19",
                            "https://smarthealth.cards#immunization"]</tt></li>
                    <li><tt>["https://smarthealth.cards#health-card", "https://smarthealth.cards#covid19",
                            "https://smarthealth.cards#laboratory"]</tt></li>
                </ul>
            </li>
            <li><code>credentialSubject</code> – Contains a FHIR Bundle of a Patient resource, either one or two FHIR
                Immunization resources, or an Observation resource representing a lab result.</li>
        </ul>


        <p>This is an example of the SMART Health Card header describing an immunization card, issued on April 20, 2021.
        </p>
        <p><tt>
            </tt></p>
        <pre><tt>{
	"iss": "https://vs-icx18-28.epic.com/Interconnect-HDR1035915-CDE/api/epic/2021/Security/Open/EcKeys/32001/SHC",
	"nbf": 1618933658,
	"type": ["https://smarthealth.cards#health-card", "https://smarthealth.cards#covid19", "https://smarthealth.cards#immunization"],
	"vc": {
  "credentialSubject": {
      //payload removed for readability
		}
	}
}
</tt></pre><tt>
        </tt>
        <p></p>

        <h4 id="vc-fhirbundle" class="section-link">FHIR Bundle</h4>

        <p>The credentialSubject element from the SMART Health Card header defines the version of FHIR and contains a
            Bundle of FHIR resources.</p>
        <ul>
            <li><code>fhirVersion</code> – Version of FHIR resources contained in this SMART Health Card. Epic generated
                SMART Cards will always use FHIR R4 resources, represented in this field as “4.0.1”.</li>
            <li><code>fhirBundle</code> – A Bundle of type collection containing two to three FHIR resources. Each entry
                in the Bundle is a distinct FHIR resource.</li>
        </ul>
        <p>Here's an example of a credentialSubject containing a fhirBundle:</p>
        <p><tt></tt></p>
        <pre><tt>{
	"credentialSubject": {
		"fhirVersion": "4.0.1",
		"fhirBundle": {
			"resourceType": "Bundle",
			"type": "collection",
			"entry": [
        { //FHIR resource },
				{ //FHIR resource #2 },
				{ //another FHIR resource! },
			}
		}
	}
</tt></pre>
        <p></p>
        <h4 id="vc-fhirpatient" class="section-link">FHIR Patient</h4>
        <p>SMART Health Cards always contain a FHIR Patient resource. Unlike a typical Patient resource, the only
            information in the resource is the patient's given and family name, and date of birth.</p>
        <p>When consuming a SMART Health Card, the demographics in this Patient resource are verified against the user's
            identity.</p>
        <p><tt></tt></p>
        <pre><tt>{
	"resource": {
		"resourceType": "Patient",
		"name": [{
			"family": "McCall",
			"given": ["Table"]
		}],
		"birthDate": "2000-04-15"
	}
}
</tt></pre>
        <p></p>
        <h5 id="vc-considerationsforpatientidentity" class="section-link">Considerations For Patient Identity</h5>
        <p>Importantly, <strong>possession of a SMART Health Card does not prove a patient's identity</strong>. In fact,
            patient caregivers, for example: parents, may access and download the SMART Health Cards of their wards.
            Pass apps and verifiers must verify identity.</p>
        <p>When a patient shares a SMART Health Card with a verifier, the verifier is expected to validate the patient's
            identity independently. For example, if a patient presents their driver's license along with their SMART
            Health Card, the verifier should confirm that the demographics within the SMART Health Card (specifically,
            the name and date of birth), match those on the driver's license.</p>
        <h4 id="vc-fhirimmunization" class="section-link">FHIR Immunization</h4>
        <p>A SMART Health Card documenting a vaccination contains one or two FHIR Immunization resources. The below
            example describes a &nbsp;Pfizer immunizations completed on February 1.</p>
        <ul>
            <li><code>status</code> – contains the value “completed”</li>
            <li><code>vaccineCode</code> – contains a CVX code identifying the given vaccine.</li>
            <li><code>occurrenceDateTime</code> – the date on which the vaccine was administered</li>
            <li><code>lotNumber</code> – the product lot number, further identifying the vaccine</li>
            <li><code>performer</code> – contains the name of the organization at which the vaccine was administered.
                Will not be present if administering organization is unknown.</li>
        </ul>

        <h5 id="vc-fhirimmunizationexample" class="section-link">Example Immunization</h5>

        <tt>
            <pre>{
	"resource": {
		"resourceType": "Immunization",
		"status": "completed",
		"vaccineCode": {
			"coding": [{
				"system": "http://hl7.org/fhir/sid/cvx",
				"code": "208"
			}]
		},
		"patient": {
			"reference": "Patient/resource:0"
		},
		"occurrenceDateTime": "2021-02-01",
		"lotNumber": "1234",
		"performer": [{
			"actor": {
				"display": "Current Development Environment (CDE)"
			}
		}]
	}
} 
</pre>
        </tt>
        <h4 id="vc-fhirobservation" class="section-link">FHIR Observation</h4>
        <p>A SMART Health Card documenting a lab result contains one FHIR Observation resource. The below example
            describes a negative lab test resulted on November 23, 202.</p>
        <ul>
            <li><code>status</code> – contains the value "final"</li>
            <li><code>code</code> – contains a LOINC code identifying the lab test, if known</li>
            <li><code>effectiveDateTime</code> – the time at which the lab test was resulted</li>
            <li><code>performer</code> – contains the name of the organization at which the test was performed.</li>
            <li><code>value</code> – the result of a COVID-19 lab test may be represented as a SNOMED code, a quantity,
                or a string.</li>
        </ul>

        <h5 id="vc-fhirobservationexample" class="section-link">Example Observation</h5>

        <tt>
            <pre>{
 	"resource": {
 		"resourceType": "Observation",
 		"status": "final",
 		"code": {
 			"coding": [{
 				"system": "http://loinc.org",
 				"code": "882-1"
 			}]
 		},
 		"subject": {
 			"reference": "Patient/resource:0"
 		},
 		"effectiveDateTime": "2020-11-23T06:00:00Z",
 		"performer": [{
 			"display": "Health system"
 		}],
 		"valueString": "Negative"
 	}
 }
</pre>
        </tt>

        <h3 id="vc-trust" class="section-link">SMART Health Cards &amp; Trust</h3>

        <p>A SMART Health Card is verifiable because its signed payload can be verified against a trusted public key.
            Each issuer publishes a JSON Web Key Set (JWKS) containing at least one public key. Typically, each
            healthcare organization that uses Epic acts as a distinct issuer.</p>
        <p>A verifier must determine which issuers to trust. Issuers participating in the Vaccine Credential Initiative
            have shared their publicly accessible JSON Web Key Set URLs in this directory:</p>
        <ul>
            <li><a
                    href="https://raw.githubusercontent.com/the-commons-project/vci-directory/main/vci-issuers.json">https://raw.githubusercontent.com/the-commons-project/vci-directory/main/vci-issuers.json</a>
            </li>
        </ul>
        <p>Verifiers should consider trusting and caching all of the public keys provided in the key sets from this
            directory.</p>

        <h4 id="vc-verifythesignedwebtoken" class="section-link">Verify the Signed Web Token</h4>

        <p>An issuer's key set is discoverable during verification by appending "/.well-known/jwks.json" to a SMART
            Health Card's iss value. For example, iss value in the above SMART Health Cards is an inaccessiable, url
            internal to Epic:
            <tt>https://vs-icx18-28.epic.com/Interconnect-HDR1035915-CDE/api/epic/2021/Security/Open/EcKeys/32001/SHC</tt>
        </p>

        <p><tt></tt></p>
        <pre><tt>HTTP Get https://vs-icx18-28.epic.com/Interconnect-HDR1035915-CDE/api/epic/2021/Security/Open/EcKeys/32001/SHC/.well-known/jwks.json

{
  "keys": [{
    "x": "gfoeoAb2l-hqEHXyZIBqa1OObZ7Tunx5hlYRXtnPJwg",
    "y": "HcBgiLJ53BFqOp5lxiMUz5Jsik9DtM286pyWv2hVKNA",
    "kid": "fhkvi0GVzKPv2iHtzaF-Xqbq6PTaDqWGIwwsdP6vqOM",
    "use": "sig",
    "kty": "EC",
    "alg": "ES256",
    "crv": "P-256"
  }]
}
</tt></pre>
        <p></p>

        <p>The key identifier from the JWKS's <code>kid</code> element corresponds to <code>kid</code> element in the
            header of the SMART Health Card's JWS. Epic issued SMART Health Cards are signed using ECDSA P-256 SHA-256.
        </p>
        <p>Verifiers should use standard security libraries to verify SMART Health Card signatures. Refer to <a
                href="https://tools.ietf.org/html/rfc7515#appendix-A.3">Appendix A.3 of RFC 7515</a> to understand how
            Epic’s signature is calculated and formatted.</p>


        <h1 id="vc-librariesandreferences" class="section-link">Open Source Libraries &amp; References</h1>
        <p>While it is not an exhaustive list, nor maintained or affiliated with Epic, the following resources might be
            helpful:</p>
        <ul>
            <li>Reference implementation: <a href="https://c19.cards/venue">https://c19.cards/venue</a> and open source
                code: <a
                    href="https://github.com/smart-on-fhir/health-cards-tests">https://github.com/smart-on-fhir/health-cards-tests</a>
            </li>
            <li>Microsoft-developed SMART Health Card validation SDK: <a
                    href="https://github.com/microsoft/health-cards-validation-SDK">https://github.com/microsoft/health-cards-validation-SDK</a>
            </li>
            <li>MITRE-developed SMART Health Cards tutorial Jupyter Notebook: <a
                    href="https://github.com/dvci/health-cards-walkthrough/blob/main/SMART%20Health%20Cards.ipynb">https://github.com/dvci/health-cards-walkthrough/blob/main/SMART%20Health%20Cards.ipynb</a>
            </li>
        </ul>

    </div>


</div>
<div id="fhir_bulk_data" style="display:none">
    <h3 class="body-header body-header-without-subtext">
        FHIR Bulk Data Access Tutorial
    </h3>



    <div>
        <h1 id="FHIR_Bulk_Data_Top" class="section-link">FHIR Bulk Data Access Tutorial</h1>


        <p>
            <strong>Contents</strong>
        </p>
        <ul>
            <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                    onclick="Epic$USCDI$Dev$ShowDocument(" fhir_bulk_data","background")"="" tabindex="0">Background</a>
            </li>
            <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                    onclick="Epic$USCDI$Dev$ShowDocument("
                    fhir_bulk_data","important_considerations_for_using_bulk_data")"="" tabindex="0">Important
                    Considerations for Using Bulk Data</a></li>
            <ul>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(" fhir_bulk_data","technical_constraints")"=""
                        tabindex="0">Technical Constraints</a></li>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(" fhir_bulk_data","when_to_use_bulk_data")"=""
                        tabindex="0">When to Use Bulk Data</a></li>
                <ul>
                    <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                            onclick="Epic$USCDI$Dev$ShowDocument(" fhir_bulk_data","good_use_cases_for_bulk_data")"=""
                            tabindex="0">Good Use Cases for Bulk Data</a></li>
                    <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                            onclick="Epic$USCDI$Dev$ShowDocument(" fhir_bulk_data","poor_use_cases_for_bulk_data")"=""
                            tabindex="0">Poor Use Cases for Bulk Data</a></li>
                </ul>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(" fhir_bulk_data","best_practices")"="" tabindex="0">Best
                        Practices</a></li>
            </ul>
            <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                    onclick="Epic$USCDI$Dev$ShowDocument("
                    fhir_bulk_data","epics_implementation_of_the_bulk_data_standard")"="" tabindex="0">Epic's
                    Implementation of the Bulk Data Standard</a></li>
            <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                    onclick="Epic$USCDI$Dev$ShowDocument(" fhir_bulk_data","using_the_bulk_data_apis")"=""
                    tabindex="0">Using the Bulk Data APIs</a></li>
            <ul>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(" fhir_bulk_data","client_configuration")"=""
                        tabindex="0">Client Configuration</a></li>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(" fhir_bulk_data","kicking_off_the_bulk_data_request")"=""
                        tabindex="0">Kicking Off the Bulk Data Request</a></li>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(" fhir_bulk_data","checking_the_status_of_the_request")"=""
                        tabindex="0">Checking the Status of the Request</a></li>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(" fhir_bulk_data","viewing_resource_files")"=""
                        tabindex="0">Viewing Resource Files</a></li>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(" fhir_bulk_data","viewing_request_errors")"=""
                        tabindex="0">Viewing Request Errors</a></li>
                <li><a style="cursor: pointer;" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="Epic$USCDI$Dev$ShowDocument(" fhir_bulk_data","deleting_requests")"=""
                        tabindex="0">Deleting Requests</a></li>
            </ul>

        </ul>


        <h2 id="Background" class="section-link">Background</h2>
        <p>The FHIR Bulk Data Access specification, also known as Flat FHIR, gives healthcare organizations the ability
            to provide external clients with large amounts of data for a population of patients formatted as FHIR
            resources. This functionality is available starting in the August 2021 version of Epic.</p>


        <h2 id="Important_Considerations_for_Using_Bulk_Data" class="section-link">Important Considerations for Using
            Bulk Data</h2>
        <p>The bulk data API is powerful, and because it uses the FHIR standard, you might find it easier to export and
            process data from bulk data if you are already familiar with that standard. However, there are important
            things to take into consideration before deciding to use bulk data. As always, carefully consider your use
            case to determine whether bulk data is the best solution.</p>


        <h3 id="Technical_Constraints" class="section-link">Technical Constraints</h3>
        <p>Bulk data runs on an organization's operational database, so it is important to consider performance. Bulk
            data exports large amounts of data for large groups of patients, which takes more time to complete the
            larger the data set or patient population. Responses are not instantaneous, so use cases of bulk data should
            not rely on immediate responses. Later in this tutorial, we will cover how to set up requests to help
            minimize this wait time.</p>
        <p>Also, bulk data requests are not incremental. The API collects all data for the requested patients and
            resources before it starts returning any data. You cannot retrieve any results until all results are ready.
        </p>
        <h3 id="When_to_Use_Bulk_Data" class="section-link">When to Use Bulk Data</h3>
        <p>As with any data exchange, form should follow function. While having all of this data formatted as FHIR
            resources might be exciting or sound easier to work with, the data won't be useful if bulk data's technical
            capabilities don't align with your use case. Consider your exchange paradigm and workflow needs first, then
            see if bulk data meets those requirements. If it does - great! If it doesn't, explore other options to
            interoperate with Epic <a href="https://open.epic.com/Home/Interoperate">on open.epic</a>. Below are
            examples of use cases that fit bulk data and some that don't.</p>


        <h4 id="Good_Use_Cases_for_Bulk_Data" class="section-link">Good Use Cases for Bulk Data</h4>
        <ul>
            <li>A one-time load of data in preparation for continuous data exchange using other methods</li>
            <li>Monthly loads of a targeted set of data (for example, <code>_type=Patient,Condition</code>)</li>
            <li>Weekly export of a dynamic group of patients (for example, all patients discharged in the last week with
                a certain diagnosis)</li>
            <li>Weekly loads of small patient populations (less than one hundred), such as for registry submissions</li>
        </ul>


        <h4 id="Poor_Use_Cases_for_Bulk_Data" class="section-link">Poor Use Cases for Bulk Data</h4>
        <ul>
            <li>Data synchronization with data warehouses or other databases</li>
            <li>Periodic loads of large amounts of clinical data</li>
            <li>Incremental data loads</li>
            <li>Data exports for groups of over one thousand patients</li>
        </ul>


        <h3 id="Best_Practices" class="section-link">Best Practices</h3>
        <p>Epic has several recommendations that can help you have the best experience with bulk data APIs.</p>
        <ul>
            <li>Limit group sizes to around a thousand patients or fewer.</li>
            <li>Use the <code>_type</code> parameter whenever possible to improve response time and minimize storage
                requirements.</li>
            <li>For groups of under one hundred patients, check the request status every ten minutes, or use exponential
                backoff. For groups of over one hundred patients, check the status every thirty minutes, or use
                exponential backoff. More information on exponential backoff can be found in the <a
                    href="http://hl7.org/fhir/uv/bulkdata/export/index.html#bulk-data-status-request">bulk data export
                    specification</a>.</li>
            <li>After retrieving all of the resource file content, use the FHIR Bulk Data Delete Request API to allow
                the server to clean up the data from your request.</li>
        </ul>


        <h2 id="Epics_Implementation_of_the_Bulk_Data_Standard" class="section-link">Epic's Implementation of the Bulk
            Data Standard</h2>
        <p>The implementation guide for Bulk Data Access is on <a href="http://hl7.org/fhir/uv/bulkdata/">HL7's
                website</a>. Starting in the August 2021 version, Epic supports the 1.0.1 version of the specification
            for R4 FHIR resources. We have also incorporated some features from the 1.1.0 version to provide additional
            functionality. Epic supports only the Group Export operation. We do not support _since or other bulk data
            operations at this time.</p>


        <h2 id="Using_the_Bulk_Data_APIs" class="section-link">Using the Bulk Data APIs</h2>
        <p>Now that you have determined that bulk data is an appropriate solution for your use case, let’s go through
            how to use the APIs. This diagram shows the typical flow of the bulk data APIs.</p>
        <img src="./Documentation - Epic on FHIR_files/BulkDataDiagram.png">


        <h3 id="Client_Configuration" class="section-link">Client Configuration</h3>
        <p>External clients must be authorized to use the bulk data APIs: <a
                href="https://fhir.epic.com/Specifications?api=10169">Bulk Data Kick-off</a>, <a
                href="https://fhir.epic.com/Specifications?api=10170">Bulk Data Status Request</a>, <a
                href="https://fhir.epic.com/Specifications?api=10171">Bulk Data File Request</a>, and <a
                href="https://fhir.epic.com/Specifications?api=10172">Bulk Data Delete Request</a>. Additionally, they
            must be authorized for the R4 search API for each resource they want to request, for example,
            AllergyIntolerance.Search (R4). The healthcare organization you're integrating with also needs to authorize
            your client to access the specific groups of patients. Work with that organization to enable a group that is
            appropriate for your use case.</p>


        <h3 id="Kicking_Off_the_Bulk_Data_Request" class="section-link">Kicking Off the Bulk Data Request</h3>
        <p>To begin, you need the base URL of the organization you want to integrate with, as previously described in
            the FHIR Tutorial. Let's say this is our base URL:</p>
        <pre class="pre-scrollable">https://apporchard.epic.com/interconnect-aocurprd-oauth/api/FHIR/R4
</pre>
        <p>Bulk data uses the Group resource for the export operation. Contact the organization you are integrating with
            to discuss what group of patients to use for your integration and to get the FHIR ID for that group.</p>
        <p>Use the Group FHIR ID to call the group export operation:</p>
        <pre class="pre-scrollable">https://apporchard.epic.com/interconnect-aocurprd-oauth/api/FHIR/R4/Group/eIscQb2HmqkT.aPxBKDR1mIj3721CpVk1suC7rlu3yX83/$export
</pre>
        <p>The request above returns results for a default set of R4 resources in the ndjson format. The default set
            includes the resources in the U.S. Core Data for Interoperability (USCDI) data classes, resources from the
            patient compartment of the bulk data access specification, and additional supporting resources outside of
            the patient compartment. Starting in the November 2021 version of Epic (and in the August 2021 version with
            a special update), Provenance is also in the default set of resources.</p>
        <p>Epic also supports two parameters for the group export operation: <code>_type</code> and
            <code>includeAssociatedData</code>.</p>
        <p>The <code>_type</code> parameter accepts a comma-delimited list of FHIR resource types. When used, bulk data
            returns only the resource types specified in the parameter. Epic recommends using this parameter whenever
            possible because limiting the scope of the request to only the resources you need decreases both response
            times and the amount of data stored.</p>
        <p>The <code>_type</code> parameter is also the only way to retrieve Binary resources. Binary files can become
            very large in this workflow, so Binary resources are not returned by default. Certain very large Binary
            files cannot be retrieved by bulk data operations at all. If a Binary file is not returned, you can still
            get the ID from the resource that it's associated with, for example, from the
            DocumentReference.content.attachment.url element, and perform a separate Binary.Read request for the
            content.</p>
        <p>For resources where Epic doesn't support a search by Patient ID (for example, Medication), a resource that
            has search by Patient ID and references the resource of interest (for example, MedicationRequest) should be
            included. For example, you could use the <code>_type</code> parameter to limit the request to include only
            Patient, MedicationRequest, and Medication resources as follows:</p>
        <pre class="pre-scrollable">https://apporchard.epic.com/interconnect-aocurprd-oauth/api/FHIR/R4/Group/eIscQb2HmqkT.aPxBKDR1mIj3721CpVk1suC7rlu3yX83/$export?_type=patient,medicationrequest,medication
</pre>
        <p>The <code>includeAssociatedData</code> parameter can be set to "LatestProvenanceResources" to include the
            Provenance resource associated with each resource instance included in the bulk data files.</p>
        <pre class="pre-scrollable">https://apporchard.epic.com/interconnect-aocurprd-oauth/api/FHIR/R4/Group/eIscQb2HmqkT.aPxBKDR1mIj3721CpVk1suC7rlu3yX83/$export?includeAssociatedData= LatestProvenanceResources
</pre>
        <p>When you have compiled all components of the request URL, you can submit the GET request to kick-off the bulk
            data workflow. Note that by default, a client can request a specific group of patients only once in a
            twenty-four-hour period. If you need to request bulk data more frequently, work with the Epic organization
            you're integrating with to configure an appropriate request window.</p>
        <p>Your request must include the following headers:</p>
        <ul>
            <li>Accept: application/fhir+json</li>
            <li>Prefer: respond-async</li>
        </ul>


        <h3 id="Checking_the_Status_of_the_Request" class="section-link">Checking the Status of the Request</h3>
        <p>After kicking off the bulk data request, use the status API to track the progress of the request. In the
            response headers of the kick-off request, the value of the “Content-Location” header is the status URL for
            this bulk data request. Each bulk data request has a unique identifier used to request the status of a
            specific bulk data request.</p>
        <pre class="pre-scrollable">https://apporchard.epic.com/interconnect-aocurprd-oauth/api/FHIR/BulkRequest/ B0F84FB8D37411EB92726C04221B350C
</pre>
        <p>If the bulk data request has not finished processing, the response body is empty. An approximate measure of
            progress is available in the "X-Progress" response header. The value of this header is "Searched X of Y
            patients" where "Y" is the number of patients in the group and "X" is the number of patients processed so
            far. This is only an approximate measure of progress because there is additional processing required after
            all patients have been searched, so it's possible for the header to be set to "Searched 100 of 100 patients"
            with no response body returned.</p>
        <p>Epic recommends pinging for the request status every ten minutes for groups with a hundred or fewer patients,
            every thirty minutes for groups over a hundred, or using exponential backoff as described in the <a
                href="http://hl7.org/fhir/uv/bulkdata/export/index.html#bulk-data-status-request">bulk data export
                specification</a>.</p>
        <p>After the request is completed, the status API returns the URLs for the resource files. For more information
            on the structure of the response, reference the API details.</p>
        <pre class="pre-scrollable">{
&nbsp;&nbsp;&nbsp; "transactionTime": "2021-06-23T16:39:52Z",
&nbsp;&nbsp;&nbsp; "request": "https://apporchard.epic.com/interconnect-aocurprd-oauth/api/FHIR/R4/Group/eIscQb2HmqkT.aPxBKDR1mIj3721CpVk1suC7rlu3yX83/$export?_type=patient,encounter,condition",
&nbsp;&nbsp;&nbsp; "requiresAccessToken": "true",
&nbsp;&nbsp;&nbsp; "output": [
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "type": "Patient",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "url": "https://apporchard.epic.com/interconnect-aocurprd-oauth/api/FHIR/BulkRequest/9ED3042CD44111EB84F2D2068206269D/e19upATM-PTKGZuHsy04IUQ3"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; },
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "type": "Condition",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "url": "https://apporchard.epic.com/interconnect-aocurprd-oauth/api/FHIR/BulkRequest/9ED3042CD44111EB84F2D2068206269D/e8D8N1JwB3qCiHJkmeV.98w3"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }
&nbsp;&nbsp;&nbsp; ],
&nbsp;&nbsp;&nbsp; "error": [
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "type": "OperationOutcome",
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "url": "https://apporchard.epic.com/interconnect-aocurprd-oauth//api/FHIR/BulkRequest/9ED3042CD44111EB84F2D2068206269D/eKbGgVw9pUn6xIiB8kZ756w3"
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }
&nbsp;&nbsp;&nbsp; ]
}
</pre>


        <h3 id="Viewing_Resource_Files" class="section-link">Viewing Resource Files</h3>
        <p>Bulk data generates a separate file for each resource type. The "output" elements in the status API response
            lists each resource type and the corresponding file request URL. If a single resource has a very large
            number of results, the data is split into multiple files, all linked in the response. There is a maximum of
            three thousand resource instances per file. To view a file, you'll use the file request API. The file URLs
            for this API are found in the file manifest from the status API.</p>
        <pre class="pre-scrollable">https://apporchard.epic.com/interconnect-aocurprd-oauth/api/FHIR/BulkRequest/9ED3042CD44111EB84F2D2068206269D/e19upATM-PTKGZuHsy04IUQ3
</pre>
        <p>The format of the bulk data files is ndjson. The ndjson format is similar to JSON, but is newline-sensitive.
            For more information, refer to the <a href="http://ndjson.org/">ndjson specification</a>. Resource instances
            are included in the bulk data file through a search by Patient ID or through a reference in a previously
            gathered resource instance. The files do not differentiate by patient, so results for all patients are
            included in each resource file.</p>


        <h3 id="Viewing_Request_Errors" class="section-link">Viewing Request Errors</h3>
        <p>In addition to the resource files, if there are any request-level errors, the "error" element in the status
            API response includes the URL for the OperationOutcome file. This file includes only request-level errors.
            Bulk data does not return resource-level errors.</p>
        <table class="table table-hover">
            <tbody>
                <tr>
                    <td>
                        <p style="white-space:nowrap;font-weight:bold;">Error Code</p>
                    </td>
                    <td>
                        <p style="white-space:nowrap;font-weight:bold;">Error Text</p>
                    </td>
                    <td>
                        <p style="white-space:nowrap;font-weight:bold;">Notes</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>59130</p>
                    </td>
                    <td>
                        <p>"Some information was not or may not have been returned due to business rules, consent or
                            privacy rules, or access permission constraints."</p>
                    </td>
                    <td>
                        <p>This error is logged when a resource is requested that the client is not authorized for.
                            OperationOutcome.issue.diagnostics lists the resource.</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>59100</p>
                    </td>
                    <td>
                        <p>"Content invalid against the specification or a profile."</p>
                    </td>
                    <td>
                        <p>This error is logged when a parameter is included in the request that Epic doesn't support in
                            bulk data. OperationOutcome.issue.diagnostics lists the parameter.</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>59136</p>
                    </td>
                    <td>
                        <p>"The resource or profile is not supported."</p>
                    </td>
                    <td>
                        <p>This error is logged when a resource is requested that Epic doesn't support in bulk data.
                            OperationOutcome.issue.diagnostics lists the resource.</p>
                    </td>
                </tr>
            </tbody>
        </table>


        <h3 id="Deleting_Requests" class="section-link">Deleting Requests</h3>
        <p>If you no longer want to run a request after starting it, or no longer need the data stored, the bulk data
            delete API is available. This API uses the same URL as the status API, but uses the DELETE HTTP method
            rather than GET.</p>
        <pre class="pre-scrollable">DELETE https://apporchard.epic.com/interconnect-aocurprd-oauth/api/FHIR/BulkRequest/ B0F84FB8D37411EB92726C04221B350C
</pre>
        <p>If you delete a request before it is completed (the status API hasn’t returned any content), the request does
            not finish processing and is marked as deleted in the receiving system. The deleted request does not count
            towards the one request per group per client per day limit.</p>
        <p>If you delete a request after it is completed (the status API returns file URLs), the request remains marked
            as completed in the system, but the data generated by the request is deleted. Deleting the generated data
            after you've retrieved it can help prevent excessive storage requirements for the server. In this case, the
            request does count towards the client's request limit. Epic recommends running the delete API after you
            finish retrieving the data files.</p>
    </div>


</div>
</div>
</div>
</div>


<script>
    Epic.USCDI.PageLoad.AddToPageLoad(function () {
        var docId = "oauth2";
        var section = null
        Epic$USCDI$Dev$ShowDocument(docId, section, true);
    });

</script>
</div>
</div>
</div>
</div>
<div id="USCDIPrivacyPolicyWrapper">
    <div id="USCDIPrivacyPolicyCookie" class="hidden">
        <div id="USCDIPrivacyPolicyCookieBackground">
            <p>
                We use cookies from Google Analytics to improve our website.
                By accepting, you will receive these cookies from Epic on FHIR.
                Decline if you wish to use Epic on FHIR without these cookies.
                <a href="https://open.epic.com/Home/PrivacyPolicy" target="_blank">Read our privacy policy here</a>.

            </p>

            <div id="USCDIPrivacyPolicyCookieActions">
                <input id="USCDICookiesAccept" type="button" value="Accept" class="btn btn-default">
                <input id="USCDICookiesDecline" type="button" value="Decline" class="btn btn-default">
            </div>
        </div>
    </div>
</div>


<div class="footer-background" style="position:relative;">
    <div class="row footer-row">
        <div class="footer-logo" style="display:inline-block;">
            <a href="http://www.epic.com/" title="Epic homepage">
                <img src="./Documentation - Epic on FHIR_files/EpicLogo.svg">
            </a>
        </div>
        <div class="footer-links">
            <div class="footer-link"><a href="https://fhir.epic.com/FAQ" tabindex="0"
                    onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" id="FAQLink">Frequently Asked Questions
                    (FAQs)</a></div>
            <div class="footer-link"><a href="https://open.epic.com/Home/Contact" id="ContactLink">Contact</a></div>

            <div class="footer-link">
                <a href="https://fhir.epic.com/Resources/Terms"
                    onclick="ga(&#39;send&#39;, &#39;pageview&#39;, &#39;/Resources/Terms&#39;);" title="Terms"
                    tabindex="0" onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);" aria-label="Terms of Use"
                    target="_blank">Terms of Use</a>
            </div>
            <div class="footer-link"><a href="https://open.epic.com/Home/PrivacyPolicy" target="_blank">Privacy
                    Policy</a></div>
        </div>
        <div class="footer-copyright" style="display:inline-block;">
            <div style="font-size:x-small;">1979 Milky Way, Verona, WI 53593 </div>
            <div style="font-size:x-small;">
                © 1979-2022 Epic Systems Corporation. All rights reserved.
                Protected by U.S. patents. For details visit <a href="http://www.epic.com/patents"
                    target="_blank">www.epic.com/patents</a>.
                <br>
                HL7®, FHIR® and the FLAME mark are the registered trademarks of HL7 and are used with the permission of
                HL7. The use of these trademarks does not constitute a product endorsement by HL7.
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_GenericAlert" tabindex="-1" role="dialog" aria-hidden="true"
    aria-labelledby="GenericAlertTitle">
    <div class="modal-dialog" style="top: 30%">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <h4 class="modal-title" id="GenericAlertTitle"></h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer" style="text-align:left;">
                    <button type="button" tabindex="0" class="btn btn-primary"
                        onkeyup="Epic.USCDI.Util.RaiseClickEventOnEnter(event);"
                        onclick="$(&#39;#modal_GenericAlert&#39;).modal(&#39;hide&#39;);">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" style="position: absolute; margin-top: 25%;" id="LogoutWarningDialogue" tabindex="-4"
    role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <p>Due to inactivity you will be logged out in <span id="LogoutWarningDialogueTimer">60</span> seconds.
                </p>
            </div>
        </div>
    </div>
</div>

<div id="divTooltip" class="helptextDiv" style="display: none;"></div>
<script src="./Documentation - Epic on FHIR_files/jquery"></script>

<script src="./Documentation - Epic on FHIR_files/epicCore"></script>



<script type="text/javascript">
    function closeCookiePopup() {
        $('#').css("opacity", "0");
        setTimeout(function () {
            $('#USCDIPrivacyPolicyCookie').addClass('hidden');
        }, 500)
    }
    $(function () {
        if (!Epic.USCDI.Util.CookieExists(Epic.USCDI.Util.GACookieName)) {
            $('#USCDIPrivacyPolicyCookie').removeClass('hidden');
        }

        $('#USCDICookiesAccept').click(function () {
            closeCookiePopup();

            var date = (new Date("2023-01-05T19:19:28.4089866Z")).toUTCString();
            Epic.USCDI.Util.CreateCookie(Epic.USCDI.Util.GACookieName, "accept", date);
        });

        $('#USCDICookiesDecline').click(function () {
            closeCookiePopup();

            var date = (new Date("2023-01-05T19:19:28.4089866Z")).toUTCString();
            Epic.USCDI.Util.CreateCookie(Epic.USCDI.Util.GACookieName, "decline", date);
        });

        if (!Epic.USCDI.Util.CookieExists('EoFTimezoneOffset')) {
            var date = (new Date("2022-01-05T20:19:28.4089866Z")).toUTCString();
            Epic.USCDI.Util.CreateCookie('EoFTimezoneOffset', new Date().getTimezoneOffset(), date);
        }
    });

    function scrollAnimate(handler) {
        var frame;
        return function () {
            if (frame) {
                cancelAnimationFrame(frame);
            }

            frame = requestAnimationFrame(function () {
                handler();
            });

        }
    };

    function scrollCheck() {
        var logo = $(".header-logo-link");
        var topVal = logo.css("top");
        var atTop = $(window).scrollTop() < 5;
        if (atTop && logo && topVal != "-10px") {
            logo.css({ top: "-10px" });
        }
        else if (!atTop && logo && topVal != "-30px") {
            logo.css({ top: "-30px" });
        }
    }

    var supportsPassive = false;
    try {
        var opts = Object.defineProperty({}, 'passive', {
            get: function () {
                supportsPassive = true;
            }
        });
        window.addEventListener("testPassive", null, opts);
        window.removeEventListener("testPassive", null, opts);
    } catch (e) { }

    document.addEventListener("scroll", scrollAnimate(scrollCheck)
        , supportsPassive ? { passive: true } : undefined);
</script>

<script type="text/javascript">
    Epic.USCDI.PageLoad.AddToPageLoad(
        function () {
            $("#UrlRedirect").val(encodeURIComponent(window.location.pathname + window.location.search + window.location.hash));
        });
</script>
<div id="SOJ" style="display:none">
    <div>
        <div>
            <div><span>Saving</span><img src="./Documentation - Epic on FHIR_files/loadingellipse.gif"></div>
        </div>
    </div>
</div>
<div id="LoadingGIF" style="display:none">
    <div>
        <div>
            <div><span>Loading</span><img src="./Documentation - Epic on FHIR_files/loadingellipse.gif"></div>
        </div>
    </div>
</div>
<script type="text/javascript" src="./Documentation - Epic on FHIR_files/tooltip.js"></script>
<div id="divTooltip" class="helptextDiv" style="display: none;"></div>
<div style="display:none;" id="IsAdmin">False</div>

</body>

</html>