import pytest
from httpx import AsyncClient
from starlette.status import (
    HTTP_404_NOT_FOUND, 
    HTTP_422_UNPROCESSABLE_ENTITY, 
    HTTP_200_OK)


# @pytest.mark.asyncio
# class TestTest:

#     async def test_test_route(self, test_client: httpx.AsyncClient):
#         resp = await test_client.get("/test_route")
#         assert resp.status_code == status.HTTP_200_OK
#         assert resp.json() == 'Hello World!'


@pytest.mark.asyncio
class TestGoogleOCR:

    async def test_tesseract_jpg_upload_image(
        self, 
        page_1_jpg, 
        test_client: AsyncClient):
        
        payload = {'file': open(page_1_jpg, 'rb')}

        resp = await test_client.post("/api/v1/ocr/google/tesseract/image", 
                                        files=payload)
        assert resp.status_code == HTTP_200_OK

    async def test_google_pdf_upload(self, 
                                    page_1_pdf, 
                                    conf, 
                                    test_client: AsyncClient):
        
        payload = {'file': open(page_1_pdf, 'rb')}

        r = await test_client.post(
            "/api/v1/ocr/google/tesseract/pdf", 
            files=payload)
        
        assert r.status_code == HTTP_200_OK


@pytest.mark.asyncio
class TestAWSTextract:

    async def test_textract_jpg_upload(
        self, 
        page_1_jpg, 
        conf, 
        test_client: AsyncClient):
        
        payload = {'file': open(page_1_jpg, 'rb')}

        r = await test_client.post(
            "/api/v1/ocr/aws/textract/image", 
            files=payload)
        
        assert r.status_code == HTTP_200_OK
        
        assert 'Content-Disposition' in r.headers
        assert r.headers['Content-Disposition'] == conf.CSV_RESULT_FILENAME


    async def test_multi_file_base(
        self, 
        page_1_jpg, 
        page_1_pdf, 
        test_client: AsyncClient):

        f1 = {
            "file_list": ("file_1", open(page_1_jpg, 'rb'), "multipart/form-data"), 
            "file_list": ("file_2", open(page_1_pdf, 'rb'), "multipart/form-data")
            }

        resp = await test_client.post(
            "/api/v1/ocr/google/tesseract/image/multiple", 
            files=f1)

        assert resp.status_code == HTTP_200_OK
