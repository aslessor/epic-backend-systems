from fastapi.testclient import TestClient
# from fastapi_users.password import get_password_hash
from fastapi.encoders import jsonable_encoder
from fastapi import status
from backend.main import app
from backend.config.settings import get_settings
import pytest
from uuid import UUID
import asyncio
import httpx
from asgi_lifespan import LifespanManager
# from backend.db.schemas.schema import (model)

settings = get_settings()

# def get_settings_override():
    # return config.DevSettings(DATABASE_URL='mongomock://localhost')


@pytest.fixture
def page_1_jpg():
    return settings.PAGE_1

@pytest.fixture
def page_1_pdf():
    return settings.PAGE_1_PDF


@pytest.fixture
def conf():
    return settings


@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()

@pytest.fixture
async def test_client():
    # app.dependency_overrides[get_database] = get_test_database
    async with LifespanManager(app):
        async with httpx.AsyncClient(app=app, base_url="http://app.io") as test_client:
            yield test_client

# @pytest.fixture
# def good_ocr_result() -> OCRResults:
#     test_result = TestResult(
#         test_name='Cholesterol',
#         test_result=2.3
#     )

#     return OCRResults(
#         id=UUID("bc8c9ad9-e056-42-823e-d1ac38241707"),
#         age=21,
#         gender=Gender.female,
#         results=[test_result]
#         )

# @pytest.fixture
# async def logged_in_client(user, user_password):
#     async with LifespanManager(app):
#         async with httpx.AsyncClient(app=app, base_url="http://app.io") as test_client:
#             resp = await test_client.post("/auth/cookie/login", 
#                                           data={
#                                               'username': user.email, 
#                                               'password': user_password
#                                               })
#             assert resp.status_code == 200
#             assert 'Set-Cookie' in resp.headers
#             yield test_client