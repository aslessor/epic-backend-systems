import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'


axios.defaults.baseURL = process.env.VUE_APP_URL;

const axiosHeaders = axios.create({
  baseURL: process.env.VUE_APP_URL,
  headers: { 'Authorization': `Bearer ${window.localStorage.getItem('accessToken')}` }
})

// axiosHeaders.interceptors.response.use(function (response) {
//   return response
// }, function (error) {
//   console.log('error: ', error)
//   if (error.response.status == 401) {
//       store.dispatch('auth/refreshAuth')
//   }
//   return Promise.reject(error)
// })

export default axiosHeaders


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
