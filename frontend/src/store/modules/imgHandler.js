import axios from "axios";
import fleek from '@fleekhq/fleek-storage-js';   
// import axiosHeaders from '../../main'

const state = {
    img: null,
    imgList: [],
    imgPreviewList: [],
  };
  // get data from the state
  // are computed properties and syncronous
  const getters = {
    getImgStr: state => state.img,
    getImgList: state => state.imgList,
    getImgPreviewList: state => state.imgPreviewList,
  };
  //mutations update the state
  //convention to use uppercase letters for mutations
  const mutations = {
    CLEAR_IMAGE_STATE(state) {
      state.img = null;
      state.imgList = [];
      state.imgPreviewList = [];
    },
    DELETE_IMAGE_FROM_STATE(state, data) {
      state.imgList.splice(data, 1)
      state.imgPreviewList.splice(data, 1)
    },

    MULTI_FILE_TO_ARR(state, data) {
      // console.log('MULTI_FILE_TO_ARR: ', data)
      state.imgList.push(data)
    },

    MULTI_PREVIEW_TO_ARR(state, data) {
      // console.log('MULTI_PREVIEW_TO_ARR: ', data)
      state.imgPreviewList.push(data)
    },



  };
  const actions = {

    async multiFileAction( { state, commit }, event ) {
      Array.from(event).map(image => {

        commit('MULTI_FILE_TO_ARR', image);
        let url = URL.createObjectURL(image);
        commit('MULTI_PREVIEW_TO_ARR', url);
      });
  },


    async singleFileAction( { commit }, event ) {
        commit('uploadImg', event)
    },

    async fleekUploadMulti( { state, dispatch }, event) {
      console.log('fleekUpload: ', event)
      console.log('multiState: ', state.imgList)

      let urlArr = []
      for (let i = 0; i < state.imgList.length; i++) {
          console.log(i, state.imgList[i]);

          const input = {
            apiKey: '',
            apiSecret: '',
            key: `productImg/${ state.imgList[i].name }`,
            data: state.imgList[i] 
          };
          const res = await fleek.upload(input);

          urlArr.push( res.publicUrl );
      }

      console.log('urlArr: ', urlArr)
      const postArr = await axios.post(`/ecom/update_db/${event.product_id}`, { images: urlArr } );
      console.log('post urlArr: ', postArr)

    },
  };

  export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions
  };