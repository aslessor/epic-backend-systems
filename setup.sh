#!/bin/bash


yarn add -D @vicons/ionicons5

yarn add -D @varlet/ui


# virtualenvwrapper

# mkvirtualenv -p /usr/local/bin/python3
# mkvirtualenv -p /usr/lib/python3.9

#   make new env:
#   - mkvirtualenv <env name>
#   list envs:
#   - lsvirtualenv
#   show details
#    - showvirtualenv
#   delete virtualenv:
#   - rmvirtualenv

# install Wand==0.6.7 Debian like Ubuntu
# sudo apt-get install libmagickwand-dev

# https://github.com/emcconville/wand/tree/1622a4747b1261a4ef0c11a861e934aba15096b6

# if you need SVG, WMF, OpenEXR, DjVu, and Graphviz support 
# sudo apt-get install libmagickcore5-extra


# Deploy Script python 3.9 / 3.8
# pip install fastapi==0.70.0 \
#             aiofiles==0.8.0 \
#             gunicorn[gthread]==20.1.0 \
#             uvicorn[standard]==0.15.0 \
#             orjson==3.6.4 \
#             python-multipart==0.0.5 \
#             pandas==1.3.4 \
#             pytesseract==0.3.8 \
#             boto3==1.20.18 \
#             opencv-python==4.5.4.60 \
#             Wand==0.6.7 \
#             Pillow==8.4.0 \
#             borb==2.0.15 \
#             httpx==0.21.1 \


# ASYNC TEST PKGS
# pip install httpx==0.21.1 \
#             asgi-lifespan \
#             pytest-asyncio \
#             pytest-cov




# BASH CMDS
## NODE SERIAL PORT COMMANDS
#list serial ports:
#dmesg | grep tty
