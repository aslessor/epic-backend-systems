
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from passlib.context import CryptContext

import base64

from jose import jwk, jws, jwt
from jose.utils import base64url_encode, base64url_decode

from datetime import datetime, timedelta, timezone

# from jwt.utils import get_int_from_datetime
# from jwt import (
#     JWT,
#     jwk_from_dict,
#     jwk_from_pem,
# )

from backend.settings import get_settings
from backend.utils import BackendAuth

settings = get_settings()
'''
vendors pre-register a public key for a given Epic community member on the Epic on FHIR website 
then use the corresponding private key to sign a JSON Web Token (JWT) 
which is presented to the authorization server to obtain an access token.
'''
'''
openssl x509 -pubkey -noout < cert.pem
'''

SECRET = '8340DAF9BD97CE72CDD4B96C3FF74DD48F5FB741'
ALGO = "RS384"
ACCESS_TOKEN_EXPIRE_MINUTES = 30


# this uploaded to epic
public_key = 'MIIDFzCCAf+gAwIBAgIUP+ZEEx7XPA+g0HwChajXdJ6CxQ4wDQYJKoZIhvcNAQELBQAwGzEZMBcGA1UEAwwQZmFzdGFwaV9maGlyX2FwaTAeFw0yMTEyMjExNzEyMDJaFw0yMjAxMjAxNzEyMDJaMBsxGTAXBgNVBAMMEGZhc3RhcGlfZmhpcl9hcGkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCtM9uSk+ktQxKAqclYbLTMOheIX68BxP0Yew0fCcKFSJqKKV+ozrjPjFWfv63Y9SHnTxuoVq0yVeTD7JAd1pu7p3Hz7QWzSj5R3fopGnn2v+FS6EWWGMC042Omt9FbruRb4fhdAeLxYZb1CHeDxvR1Dz47KKKjq6IQQRZyluJwAEtdfh4N/JGMCvCv1w2Cht9cooY9ZjK4aKDwighvMgVnInRdhy3+J1Gl/mrGlVU28ar8ymLPLC/ahlYSNHfXR/27hJ3ajBGJOTcfAmnd2csPA8ZnBJMQnjm1PsJDnjsdF4byWNIWe6dAveoTSLcyR+/Bjb1x8Sb1hISUmoKIrGetAgMBAAGjUzBRMB0GA1UdDgQWBBRCLRh85M5Ae5zzG7yUgj95ohOb9zAfBgNVHSMEGDAWgBRCLRh85M5Ae5zzG7yUgj95ohOb9zAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQBoWy3fTMWqOFd7pe/dFoANC0zoL4O83rUpWwwbYXbALTzvohJ4WOWUanOzgPkeEzilP8z9WzlZcCsLGTlJVlISaeRiMhS8bbpvIC2qO0M/UZP07cgGRqDQ25w12C1HzJaE7cAqHDYj4NcvP9Yr13PN/HuGAKInXyNdLZGoeZZgI8xXt5d5UppApgdxIAKpT6F1VmCh8LMy29UGEGUNOo3OaUwz06UmhUobpXGGgv0E0SdW8+x0X9AgUpnSdAz5L4hmRjKrQlepJTqwKArbh099GxS4oulqP3dn2nkw4zYz6Kg8rZi1vTgMrOOb6JPY9SCaJ4T+sWidESxylDVdlICK'
# use this to create jwt
private_key = "MIIEogIBAAKCAQEArTPbkpPpLUMSgKnJWGy0zDoXiF+vAcT9GHsNHwnChUiaiilfqM64z4xVn7+t2PUh508bqFatMlXkw+yQHdabu6dx8+0Fs0o+Ud36KRp59r/hUuhFlhjAtONjprfRW67kW+H4XQHi8WGW9Qh3g8b0dQ8+Oyiio6uiEEEWcpbicABLXX4eDfyRjArwr9cNgobfXKKGPWYyuGig8IoIbzIFZyJ0XYct/idRpf5qxpVVNvGq/Mpizywv2oZWEjR310f9u4Sd2owRiTk3HwJp3dnLDwPGZwSTEJ45tT7CQ547HReG8ljSFnunQL3qE0i3MkfvwY29cfEm9YSElJqCiKxnrQIDAQABAoIBAHJg75yxnufsBjj/Zt7FEvRXXJkLjxfdDcLv5+9aU6IPGzcoMWrZ8UPmidjwDQ9cemI1ZK9bZg76mBMmktmRktBuuJr8KKqI2fTYYU5plKd1eYh1qRaBDCmojHNZqqEgMt/Wr1xEValP6A+y4rIcXe07/r20jQsG2gRf50IPEEQjZ1jH56JgMcQouXjwKztgzzc/8WKYeyRl8MN3YKSj70UEXPs7Y2xBo4fANuRDKX4M2q9XtNI6VxwDH8CvGPAzi185M9zzScVlwnQb4zi2MD2hcr7wKHveJienRh28ujf989F65GV68X+WJ55uRjS8O8Mu7M73VfW3PdWFxyyaFwECgYEA2g/F7IroU8DQ8LAMncExaIXmTwIqaLmZe4uva954jgVZLSXMPJ70rcy0DuaqnLg9M7qU7qGly1cKYGmXuzdGsUAI2nJQCVOElqMZm6+YlJNh1TIRmgLv+PPJallen0w92CbbcA0K7B3dpSOwgwKkNEjvdYECLhZ80OrC1EKu5+0CgYEAy1YcjvsdigIIdpBJhAdqco/Ucjm52ng6Zdq6un7klof+fZLzbB/SrjQu/GJeTakw7V7P3GD3vGd1ATxTQvYXOQFZzd06gfNUOSvn0MwrAHqkBCUMxCMAOvXGZMM4tLtfHe/ttivSp9+eDej87bCUl7S5OvVHvZn83LEpauLgBsECgYAgxvwuORcA99uwQhYzZEXD+1SKDb5ILZXHf1Cn+zHNzzn/Uk4n0pDeywVJtBxzIDJ64xHd6MheD0BNC7I1/vIOVJzAESy8qiQk7HE9+KALvTeYb0U8DDL8DdD5vmM9uJ0XL4gOiLdalIarevjeSHhcL5Zm6DRmrxSl5pPQihF2ZQKBgBNJL5xAJSUZ3EOim9Zx/Z6wFa5f20jlbmSKwns93eCJNZIjhgMTYaA0QuCdU+R5z7Iz8/MrH1Q3W9mqJxuKy9kUT7g+63W35p0llO8w7kM1Xqkl+nf/+d2dwht8iLxbqUHlp95HecuCYjNfjd3dibp54be92/gXyuwDwbHpouYBAoGAH8ed3Cniw7JdbBeY/BIwemsQe5F7+BB7VTT8f7iEquuQKdc1qfm3o2xuAin0LCDMyCq+9zZkHnkrjbz3LRWoyRxYt3q+2cO12QBQ/Fg25Hnm2WIAcmiEHvJ/28nV8pLX5IF9uHNmbrlNHc5KeXLGgRR+uhiK77XXMqyYmX4yfSY="

# The header and payload are then base64 URL encoded, combined with a period separating them, and cryptographically signed using the private key to generate a signature. Conceptually:


header = {
    "alg": "RS384",
    "typ": "JWT"
}
payload = {
    "iss": settings.NON_PROD_CLIENT_ID,
    "sub": settings.NON_PROD_CLIENT_ID,
    "aud": "https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token",
    "jti": "f9eaafba-2e49-11ea-8880-5ce0c5aee679"
}
# post created jwt to epic to get access_token

def get_token():
    # POST https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token HTTP/1.1
    # Content-Type: application/x-www-form-urlencoded
    pass


def create_access_token(
    payload: dict, 
    headers: dict, 
    key: str, 
    algo: str, 
    mins: timedelta = 3):
    # now = dt.datetime.now()
    # current_time = int(datetime.timestamp(now))
    # expired = int(datetime.timestamp(now + dt.timedelta(minutes = exptime)))
    expire = datetime.utcnow() + timedelta(minutes=mins)
    to_encode = payload.copy()
    to_encode.update({"exp": expire})
    tok = jwt.encode(to_encode, key, headers=headers, algorithm=algo)
    # tok = tok.decode("utf-8")
    return tok


def make_signature(header: dict, payload: dict, private_key: str):
    # signature = RSA-SHA384(base64urlEncoding(header) + '.' + base64urlEncoding(payload), privatekey)
    # encode_header = base64url_encode(header)
    # encode_payload = base64url_encode(payload)
    signed = jws.sign(
        payload, 
        private_key, 
        headers=header, 
        algorithm='RS384'
    )
    return signed


# def main():
#     instance = JWT()
#     message = {
#         # Client ID for non-production
#         'iss': 'ece9d0d8-ce38-4430-8b69-c00c0d065f3f',
#         'sub': 'ece9d0d8-ce38-4430-8b69-c00c0d065f3f',
#         'aud': 'https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token',
#         'jti': 'f9eaafba-2e49-11ea-8830-5ce0c5aee612',
#         'iat': get_int_from_datetime(datetime.now(timezone.utc)),
#         'exp': get_int_from_datetime(datetime.now(timezone.utc) + timedelta(minutes=1)),
#     }
    
#     with open('/home/hauwei/Dropbox/APPS/medic/fastapi-fhir-api/privatekey.pem', 'rb') as fh:
#         signing_key = jwk_from_pem(fh.read())

#     compact_jws = instance.encode(message, signing_key, alg='RS384')
#     print(compact_jws)

#     headers = CaseInsensitiveDict()
#     headers['Content-Type'] = 'application/x-www-form-urlencoded'

#     data = {
#       'grant_type': 'client_credentials',
#       'client_assertion_type': 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
#       'client_assertion': compact_jws
#     }
    
#     x = requests.post('https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token', 
#                     headers=headers, 
#                     data=data)
#     # print(x.text)
#     return x


'''
GET https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/DSTU2/Patient/T1wI5bk8n1YVgvWk9D05BmRV0Pi3ECImNSK8DKyKltsMB HTTP/1.1
Authorization: Bearer Nxfve4q3H9TKs5F5vf6kRYAZqzK7j9LHvrg1Bw7fU_07_FdV9aRzLCI1GxOn20LuO2Ahl5RkRnz-p8u1MeYWqA85T8s4Ce3LcgQqIwsTkI7wezBsMduPw_xkVtLzLU2O
'''
from backend.schemas.fhir.r4_schema import *
import httpx
from pprint import pprint

def patient_list():
    r = httpx.get('https://hapi-fhir.marc-hi.ca:8443/fhir/Patient')
    # r = r.json()['entry'][0]['resource']
    r = r.json()['entry']
    # pprint(r)
    # print(len(r))
    return [SinglePatient(**i) for i in r]

    # return List[Patient(**r)]
    # result = [Patient(**i.json()) async for i in r]

def practitioner_list():
    r = httpx.get('https://hapi-fhir.marc-hi.ca:8443/fhir/Practitioner')
    r = r.json()['entry']
    return [SinglePractioner(**i) for i in r]



if __name__ == "__main__":
    # a = patient_list()
    a = practitioner_list()

    print(a)

    # b64 = b'emp$username:Pa$$w0rd1'
    # out = 'Basic ZW1wJHVzZXJuYW1lOlBhJCR3MHJkMQ=='
    # cid = 'ece9d0d8-ce38-4430-8b69-c00c0d065f3f'
    # u = b'emp$FHIR:EpicFhir11!'
    # creds = "ZW1wJEZISVI6RXBpY0ZoaXIxMSE="

    # t = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJ1cm46b2lkOmZoaXIiLCJjbGllbnRfaWQiOiJlY2U5ZDBkOC1jZTM4LTQ0MzAtOGI2OS1jMDBjMGQwNjVmM2YiLCJlcGljLmVjaSI6InVybjplcGljOk9wZW4uRXBpYy1jdXJyZW50IiwiZXBpYy5tZXRhZGF0YSI6IlItVEI1dDEyQ2dmWno0eEZsa3c2Z2Vkb0lJSEJWdUFNWWxranVkOV94cnZZd05reUMzMkpkaGd5QmxmV0RYTVVWUTVKMFRMU28wNlQzUllmVUlJcGplQmFFT2J3cmhuZGxXSkFOeVB6NThYdkhscWp6M2JUQ3gzOGo1cGdlcUhNIiwiZXBpYy50b2tlbnR5cGUiOiJhY2Nlc3MiLCJleHAiOjE2NDAyMTA2MjQsImlhdCI6MTY0MDIwNzAyNCwiaXNzIjoidXJuOm9pZDpmaGlyIiwianRpIjoiYjQwNjhlYmYtMWY2OS00YzYwLTkyZTItNDBmY2IxYzAzMzllIiwibmJmIjoxNjQwMjA3MDI0LCJzdWIiOiJleGZvNkU0RVhqV3NuaEExT0dWRWxndzMifQ.CAEwNbatNINwHlvLZq6ls-rCSJg61-nBOXi8tk9DO9GH_LFVETQL5iAPXpp8hfyaYB4bgFOncPW3M-L82wAT5jsSv6iS6AM1U9euE30braaxWP5SWJzmxY8k0CN0SaeY22O5Jqqn3YUOaTg-NN_XEIJptAoab5Y9kGmajRFn2vK_5xuvIORE9qveg2ZEcc1wm3UhAQ4j5gkxaEOzkb7BIj22SDst9xOucubUNPKdH7bAGBbdm-bd34uQ4Ab9Y_chgUBuwnz_EkBwornDXgii9c9-5FJ1E_2Rb_rOAq_rFGE5V_lWEUVoLH1Yyzj6LszNcqPfFrTMhREZLC1YuFnHHw'
    # print(jwt.decode())


# eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJ1cm46b2lkOmZoaXIiLCJjbGllbnRfaWQiOiJlY2U5ZDBkOC1jZTM4LTQ0MzAtOGI2OS1jMDBjMGQwNjVmM2YiLCJlcGljLmVjaSI6InVybjplcGljOk9wZW4uRXBpYy1jdXJyZW50IiwiZXBpYy5tZXRhZGF0YSI6IjRfdi1fRkRoZ0w0cGgxbVl4S2JPcEdVYk8wcFFULTZNZ3dJYTduX2R1eGVGRUxJdWhSSXh5a1B6NFRyeWRSUlFVeUZjbnF1SGZzSUlIY0VKUkZHOTNyZzl1TjFoelBsd0ZiR0V2Q2NkWjJ6YlU1a0twWTZsV1ZrcXFHWnVJSnlhIiwiZXBpYy50b2tlbnR5cGUiOiJhY2Nlc3MiLCJleHAiOjE2NDAyMTY1OTUsImlhdCI6MTY0MDIxMjk5NSwiaXNzIjoidXJuOm9pZDpmaGlyIiwianRpIjoiY2UxM2RkMzgtMGY0Mi00ZTZiLWExYTMtODNiMGJmOGRjNDA5IiwibmJmIjoxNjQwMjEyOTk1LCJzdWIiOiJleGZvNkU0RVhqV3NuaEExT0dWRWxndzMifQ.dfXnKg7p0zA_Lin87GzDmxeZL83xc01p-qfb6G1QYQ43vE9DLFwtGxnGwalmT-_dDEUTpMYw3D33fmphbsq8YkPaZlKsApSIGRLqMoZ3qQhUv8cqB1UGzj7cWdQawi6jzJiZrATiDn2uFoYaRVUzfCBAnHumdiTag_Sp7CZAvGgh-5ytM4xC0HV3m9GVsjWN94OWTMLuJ8OOSgLoBB84JrzDd7Jx2AtrBezifKsMe9OICV2xp2DGgEGQV5i81Zhidyu-ymX0UGnZV_s3i88D3jNqx4zxgzrZ8Q-DPbJkNFBhg0UbZFH7q_K1CrxGaNzMokdSwlFBrxPgNJcBssGMdg


    # auth = BackendAuth()
    # # auth.read_pem()
    # auth.create_jwt()
    # auth.get_token()
    
    # token = auth.get_access_token()
    # print(token)

    # headers = CaseInsensitiveDict()
    # headers["Accept"] = "application/json"
    # headers["Authorization"] = f"Bearer {token}"
    # r = requests.get(
    #     'https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/Patient/erXuFYUfucBZaryVksYEcMg3',
    #     headers=headers
    # )
    # print(r.json())

