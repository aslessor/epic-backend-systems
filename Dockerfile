FROM python:3.9-slim

ENV APP_HOME /app

WORKDIR $APP_HOME

RUN apt-get update \
    && apt-get install -y gcc g++ openssl libxml2-dev libxslt-dev musl-dev libxslt1-dev libffi-dev zlib1g-dev libssl-dev \
    && apt-get install -y build-essential python3-dev python3-pip python3-setuptools python3-wheel \
    && apt-get install -y tesseract-ocr libmagickwand-dev python3-cffi python3-opencv ffmpeg libsm6 libxext6 libcairo2 libgl1-mesa-glx

COPY . .

# upgrade pip to ensure no errors
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install -r requirements.txt --no-cache-dir
RUN pip install -vvv uvloop

# delete bytecode
# https://stackoverflow.com/questions/16869024/what-is-pycache#16869074
RUN find /usr/local/lib/python3.9 -name '*.c' -delete \
    && find /usr/local/lib/python3.9 -name '*.pxd' -delete \
    && find /usr/local/lib/python3.9 -name '*.pyd' -delete \
    && find /usr/local/lib/python3.9 -name '__pycache__' | xargs rm -r

# ENV PORT=8080
EXPOSE 8080

# run with gunicorn
CMD gunicorn -c gunicorn_config.py backend.main:app