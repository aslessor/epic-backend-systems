// import Vue from 'vue';
// import VueRouter from 'vue-router';
// import { routes } from './routes';
import routes from '../routes';

import { createRouter, createWebHashHistory } from 'vue-router';


const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

// router.beforeEach((to, from, next) => {
//   if (to.matched.some(record => record.meta.protectedRoute)) {
//       const loggedIn = !!localStorage.getItem("username");
//       console.log('loggedin: ', loggedIn);
//       if (!loggedIn) {
//         next({ name: 'Login' } );
//       } else {
//         next();
//       }
//   } else {
//     next();
//   }
// });

export default router;
// export default { router: router, routes: routes };