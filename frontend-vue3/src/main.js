import { createApp } from 'vue'
import App from './App.vue'
import axios from 'axios'
import store from './store'
import router from './router'
// import { Button } from '@varlet/ui'
// import { AppBar, Snackbar } from '@varlet/ui'
import Varlet from '@varlet/ui'
import "./styles.css"

import '@varlet/ui/es/button/style/index.js'

import { create, 
    NButton,
    NInput,
    NForm,
    NFormItem,
    NRow,
    NCol,
    NSpace,
    NCard,
    NGrid,
    NGridItem,
    NMenu,
  } from 'naive-ui'


// https://www.naiveui.com/en-US/os-theme/docs/import-on-demand
const naive = create({
    components: [
    NButton, 
    NInput,
    NForm,
    NFormItem,
    NRow,
    NCol,
    NSpace,
    NCard,
    NGrid,
    NGridItem,
    NMenu,
]
})


axios.defaults.baseURL = process.env.VUE_APP_URL;

const axiosHeaders = axios.create({
  headers: { 'Authorization': `Bearer ${window.localStorage.getItem('accessToken')}` }
});

axiosHeaders.interceptors.response.use(function (resp) {
  return resp
}, function (err) {
  console.log('error: ', err)
  if (err.response.status == 401) {
      console.log('Response Error: ', err);
  }
  return Promise.reject(err)
})
export default axiosHeaders


const app = createApp(App)
// https://v3.vuejs.org/api/application-config.html#globalproperties
// app.config.globalProperties.$navRoutes = routes;
app.use(router)
app.use(store)
app.use(naive)
// app.use(AppBar)
// app.use(Snackbar)
app.use(Varlet)

app.mount('#app')
