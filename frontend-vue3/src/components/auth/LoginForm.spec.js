
// const LoginForm = require('./LoginForm.vue');
// const vtl = require('@testing-library/vue');
// const { render, screen } = vtl
import LoginForm from './LoginForm.vue'
import { render, screen } from '@testing-library/vue'
import '@testing-library/jest-dom';


describe('Test Suite - LoginForm Component', () => {

    describe('Layout', () => {

        it('Has login form header', () => {
            render(LoginForm);
            const header = screen.queryByRole('heading', { name: 'Login Form'} );
            // expect(header).not.toBeNull();
            expect(header).toBeInTheDocument();
        });

        it('has email placeholder', () => {
            render(LoginForm);
            const input = screen.queryByPlaceholderText('E-Mail');
            expect(input).toBeInTheDocument();
        });

        it('query input tag in container', () => {
            const { container } = render(LoginForm);
            const input = container.querySelector('input');
            expect(input).toBeInTheDocument();
        });

        it('has 2 input tags in container', () => {
            const { container } = render(LoginForm);
            const inputCount = container.querySelectorAll('input').length;
            expect(inputCount).toBe(2);
        });

    })


})


