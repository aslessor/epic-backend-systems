import axios from "axios";
import { LOGIN_JWT } from '../action-types'
import axiosHeaders from '../../main'
import { SET_STATUS, GET_USERNAME } from '../mutation-types'
import * as qs from 'qs'
// import FHIR from "fhirclient"

  const state = () => ({
    patientArr: [],
    username: window.localStorage.getItem("username") || "",
    accessToken: window.localStorage.getItem("accessToken") || null,
  })

  const getters = {
    isAuthenticated: state => !!state.username,
    [GET_USERNAME]: state => state.username,
    getIsActive: state => JSON.parse(state.isActive),
    getUserType: state => JSON.parse(state.isAdmin),
    getIsFirstLogin: state => JSON.parse(state.isFirstLogin),
    getEpicToken: state => state.accessToken,
    getPatientArr: state => state.patientArr, 
  };

  const mutations = {
    
    [SET_STATUS](state, status) { 
      state.status = status;
    },

    AUTH_CLEAR(state) {
      state.accessToken = "";
      state.refreshToken = "";
      state.username = "";
      state.status = "";
      state.isAdmin = null;
      state.isFirstLogin = null;
      state.isActive = null;
    },

    REMOVE_STORAGE() { 
      window.localStorage.removeItem("username");
      window.localStorage.removeItem("accessToken");
    },

    AUTH_SUCCESS(state, resp) {
      state.status = "success";
      
      state.username = resp.data.email;
      window.localStorage.setItem("username", resp.data.email);

      state.accessToken = resp.data.access_token;
      window.localStorage.setItem("accessToken", resp.data.access_token);
    },

    SET_JWT(state, resp) {
      console.log('SET_JWT', state, resp);
      state.accessToken = resp.data.access_token;
      window.localStorage.setItem("accessToken", resp.data.access_token);
    },
    SET_PATIENT(state, resp) {
      state.patientArr = [resp];
      // state.patientArr.push(resp);
    },
    CLEAR_PATIENT(state) {
      state.patientArr = [];
    },
    EMPTY(state) {
      console.log('', state);
    },
};

const actions = {

    async loginEpic({ commit }) {  
      console.log('login action');
      await axios
        .post(`/token`)
        .then(resp => {
          console.log('TOKEN: ', resp);
          commit('SET_JWT', resp);
        });
    },

    async getPatient({ commit }) {  
      await axiosHeaders
        .get(`/read/patient`)
        .then(resp => {
          console.log('GET PATIENT: --', resp);
          // commit('CLEAR_PATIENT')
          commit('SET_PATIENT', resp.data);
        });
    },


    [LOGIN_JWT]: ({ commit }, creds) => {
      console.log('creds: ', creds);
      return new Promise((resolve, reject) => {
        // commit(AUTH_REQUEST);
        const formData = new FormData();
        formData.set('username', creds.username);
        formData.set('password', creds.password);  

        const url = "/api/auth/login";
        axios
        .post(url, formData, {headers: { 'Content-Type': 'multipart/form-data' }})
          .then(resp => {
            console.log('login: ', resp);
            commit('auth/SET_JWT');
            // localStorage.setItem("username", resp.data.username);


            axios
            .get('/api/auth/users/me', { headers: {'Authorization': `Bearer ${resp.data.access_token}` } })
            .then(resp => {
              console.log('users_me: ', resp);
              // dispatch("snackBar", { color: 'green', btnColor: 'white', statusText: `Welcome ${resp.data.email}`, data: {info: ''}});
              commit('AUTH_SUCCESS', resp);
              // router.push("/dashboard");
            }).catch(err => {console.log('auth users/me: ', err); })


            resolve(resp);
          
          }).catch(err => {
            console.log('err ', err);
            // commit(AUTH_ERROR, err);
            // commit("snackbar/setErr", err, { root: true });
            // localStorage.removeItem("username");
            reject(err);
          });
      });
    },

    async loginOauth({ commit }) {
      commit('EMPTY');
      const baseUrl = 'https://fhir.epic.com/interconnect-fhir-oauth/oauth2/authorize?response_type=code&client_id=e31e0f8f-cb86-4291-8954-504e5dab3b48&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2F%23%2Fsuccess&state=abc123&scope=fhirUser&aud=https://fhir.epic.com/interconnect-fhir-oauth/api/fhir/r4';
      // const rootUrl = 'http://localhost:8080/#/launch?iss=https://fhir.epic.com/interconnect-fhir-oauth/api/fhir/r4&aud=';￼
      window.location = baseUrl;

      // this is result:
      // use qs to parse from location on success callback url
      // http://localhost:8080/?code=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJ1cm46b2lkOmZoaXIiLCJjbGllbnRfaWQiOiJlMzFlMGY4Zi1jYjg2LTQyOTEtODk1NC01MDRlNWRhYjNiNDgiLCJlcGljLmVjaSI6InVybjplcGljOk9wZW4uRXBpYy1jdXJyZW50IiwiZXBpYy5tZXRhZGF0YSI6IktGOWNhVUlEV3FIOWVhRS1YbENxSkJlTlJBZEVtRi1neFFxeUJiVDRHX0IyUE0ybUNPWm1nMVRMeVUzVGpwSlhlamhxRGJtd21ROHRzak5LLVgzSFY3M1FpWVdoQkRMT0wxaW5VMzdqaW9QYXRMQWRCMi1hVmZtZlgxTWYweXUyIiwiZXBpYy50b2tlbnR5cGUiOiJjb2RlIiwiZXhwIjoxNjQxNDM3NTk3LCJpYXQiOjE2NDE0MzcyOTcsImlzcyI6InVybjpvaWQ6ZmhpciIsImp0aSI6ImE1MjI2OGI4LTFmNDgtNDFlZS1iYWEwLTlmYjVhMTcwMTc1YyIsIm5iZiI6MTY0MTQzNzI5Nywic3ViIjoiZWI0R2lhN0Z5aWp0UG1Ya3J0alJwUHczIn0.C9gV0aQ8iuHEwJQCdlksISY5UeSGvrgGh8LI9QxooCkUyce4Z9xjWLK2c_hUqmXGyo-a3AP_a-KjLEwJPRPu9a-lCh2KFxaCj0zgybnrQvnLMzWDSntHf-1_CiUspv9iNwCdbttl9bCOhGWkDWvGd8UTX89SruzaImNDKSaib1zL0hrMnDo5MVgI-ODAZLxwSqX8zqBVGbsymSmhZDZGPbX7YUJHKVaa_3IMRtfcXwaf_xKcMQLerkyvbtpPB3IvJII5tFTXnEXNbBI6XouuL4pdH7dQpLYXA-1jEH7sWES745H_Rgw8Rx12oNhWSc6zJ3wJ55-98a2Fk7kEWWD8Nw&state=abc123#/success
      // window.location = `${BASE_URL}?${qs.stringify(query)}`
    },


    parseLocation({ commit }, str) {
      commit('EMPTY');
      // http://localhost:8080/?code=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJ1cm46b2lkOmZoaXIiLCJjbGllbnRfaWQiOiJlMzFlMGY4Zi1jYjg2LTQyOTEtODk1NC01MDRlNWRhYjNiNDgiLCJlcGljLmVjaSI6InVybjplcGljOk9wZW4uRXBpYy1jdXJyZW50IiwiZXBpYy5tZXRhZGF0YSI6Ii1Mdm4tWVd0c1ZTX0VyOUVjU2ZhdlgtcDJYWTdpMW96eEZ3TlJIYVVzWF92Tkd0LXlvLU55X0ZGZXc1RThiLWVIREx5eXc4ZVpUZUdlTzdsbHdMbTN5WkY0SVRmckhFQll5T1ZTZzJ1VThtNnNyVm1rOC1QYWNkVHkyNzU2RWtYIiwiZXBpYy50b2tlbnR5cGUiOiJjb2RlIiwiZXhwIjoxNjQxNDQwNTI0LCJpYXQiOjE2NDE0NDAyMjQsImlzcyI6InVybjpvaWQ6ZmhpciIsImp0aSI6IjIyZjMyYTQ0LTlmZTYtNDBhMS1hMGRjLTAxYmIzMzYxMGM2MCIsIm5iZiI6MTY0MTQ0MDIyNCwic3ViIjoiZWI0R2lhN0Z5aWp0UG1Ya3J0alJwUHczIn0.I6LeILO5KKEcMqBXXBw3XA4U2A0u928DT3pQpcNcjXlFnq9B21xGzpwR8-BVUY9SrrklrkZkTth3VypIdH--hRd2aeE5KEus3wA81cjJw1W78NL9JHlHVf6w2hsluAIqMn3vo0EbpivtWNjGaqV4rE_WFdd9kfQvkIVals5onsao8gO4Xo7wZd1s7io_WunCAHlsWgkaX0pQLs4ee1s6c8BRf88gdShifiDkfO068Iu_LVylOcSkLp9WbRabE3V656nSpyQWnNt5bUBSxiMXM_YEwzptySlBwdSaKwy9IFEWv13RfLClSdl3wnetNpytaPOyzF3mSCRwXqsJB0e2Ew&state=abc123#/success
      // console.log('finalize login hash: --', str);
      // const q = qs.parse(hash.replace('?', ''));
      let parsedURL = qs.parse(str);
      return parsedURL;
      // return parsedURL.toString();
      // const query = new URLSearchParams();
      // query.append("code", q.code);
      // query.append("state", q.state);
      // query.append('realmId', q.realmId)
      // const url = "/oauth/callback?" + query.toString();
      // axios.get(`/get_auth_token/${q.code}/${q.realmId}/${q.state}`)
      // .then(resp => {
      //   commit('SET_OAUTH_TOKENS', resp)
      // });
    },

    // https://fhir.epic.com/Documentation?docId=oauth2&section=Standalone-Oauth2-Launch_Access-Token-Request_No-Refresh-Tokens
    loginOauthFinal({ commit }, str) {
      console.log('parsed: ', str);
      // const obj = dispatch('parseLocation', str);
      // const obj = qs.parse(str);
      const obj = qs.parse(str.replace('?', ''));

      console.log('obj: ', obj);
      // console.log('client id', process.env.VUE_CLIENT_ID);

      const q = {
        "grant_type": 'authorization_code',
        "code": obj.code,
        "redirect_uri": "http%3A%2F%2Flocalhost%3A8080%2F%23%2Fsuccess",
        "client_id": "e31e0f8f-cb86-4291-8954-504e5dab3b48",
        "state": obj.state
      }

      // fhircamila

      const BASE_URL = `https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token`

      const url = BASE_URL;
      // const url = `${BASE_URL}?${qs.stringify(q)}`
      console.log('url post: ', url)
      return new Promise((resolve, reject) => {
        axios
        .post(url, 
          {q}, 
          { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } } )
          .then(resp => {
            console.log('login: ', resp);

            commit('SET_JWT', resp);
            resolve(resp);
          })
          .catch(err => {
            console.log('err: ', err)
            reject(err)
          })
      })
    },

      async register({ commit }, creds) {
        await axios
        .post('/api/auth/register', 
        { username: creds.username, 
          email: creds.email, 
          password: creds.password }
          
          ).then(resp => {
          console.log('register: ', resp)
          commit('auth/SET_JWT');
          // if ( resp.status == 201 ) {
          //     dispatch('snackBar', 
          //     { color: 'green', btnColor: 'white', statusText: `Welcome ${credentials.username}!!`, 
          //     data: {info: ''}});
          // } else if(resp.status == 400 ) {
          //   dispatch('snackBar', 
          //     { color: 'red', btnColor: 'white', statusText: `Signup Error, please try again`, 
          //     data: {info: ''}});
          // }
        })
        .catch(error => {
          console.log('register error response: ', error.response)
        })
      },





    // async forgotPassword({ }, credentials) {
    //   const url = '/auth/forgot-password'
    //   await axiosHeaders.post(url, { email: credentials.email })
    //   .then(resp => {
    //     console.log(resp)

    //   })
    // },

    // finalizeForgotPassword({ }, hash) {
    //   console.log('finalize login hash: --', hash)
    //   const q = qs.parse(hash.replace('?', ''));
    //   return q.toString()
    //   // console.log('finalize login q: --', q);
    //   // const query = new URLSearchParams();

    //   // query.append("code", q.code);
    //   // query.append("state", q.state);
    //   // query.append('realmId', q.realmId)
    //   // const url = "/oauth/callback?" + query.toString();

    //   // axios
    //   // .get(`/get_auth_token/${q.code}/${q.realmId}/${q.state}`)
    //   // .then(resp => {
    //   //   console.log('tokens: --', resp)
    //   //   commit('SET_OAUTH_TOKENS', resp)
    //   // });
    // },



}

  export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions
  };
  