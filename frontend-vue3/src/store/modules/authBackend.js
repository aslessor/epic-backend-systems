import axios from "axios";
import { LOGIN_JWT } from '../action-types'
import axiosHeaders from '../../main'
import { SET_STATUS } from '../mutation-types'
// import * as qs from 'qs'
// import FHIR from "fhirclient"

  const state = () => ({
    patientArr: [],
    username: window.localStorage.getItem("username") || "",
    accessToken: window.localStorage.getItem("accessToken") || null,
  })

  const getters = {
    // isAuthenticated: state => !!state.username,
    // [GET_USERNAME]: state => state.username,
    // getIsActive: state => JSON.parse(state.isActive),
    // getUserType: state => JSON.parse(state.isAdmin),
    // getIsFirstLogin: state => JSON.parse(state.isFirstLogin),
    getEpicToken: state => state.accessToken,
    getPatientArr: state => state.patientArr, 
  };

  const mutations = {
    
    [SET_STATUS](state, status) { 
      state.status = status;
    },

    AUTH_CLEAR(state) {
      state.accessToken = "";
      state.refreshToken = "";
      // state.username = "";
      state.status = "";
      // state.isAdmin = null;
      // state.isFirstLogin = null;
      // state.isActive = null;
    },

    REMOVE_STORAGE() { 
      window.localStorage.removeItem("username");
      window.localStorage.removeItem("accessToken");
    },

    AUTH_SUCCESS(state, resp) {
      state.status = "success";
      
      state.username = resp.data.email;
      window.localStorage.setItem("username", resp.data.email);

      state.accessToken = resp.data.access_token;
      window.localStorage.setItem("accessToken", resp.data.access_token);
    },

    SET_JWT(state, resp) {
      console.log('SET_JWT', state, resp);
      state.accessToken = resp.data.access_token;
      window.localStorage.setItem("accessToken", resp.data.access_token);
    },
    SET_PATIENT(state, resp) {
      state.patientArr = [resp];
    },
    CLEAR_PATIENT(state) {
      state.patientArr = [];
    },
};

const actions = {

    async loginEpic({ commit }) {  
      console.log('login action');
      await axios
        .post(`/token`)
        .then(resp => {
          console.log('TOKEN: ', resp);
          commit('SET_JWT', resp);
        });
    },

    async getPatient({ commit }) {  
      await axiosHeaders
        .get(`/read/patient`)
        .then(resp => {
          console.log('GET PATIENT: --', resp);
          // commit('CLEAR_PATIENT')
          commit('SET_PATIENT', resp.data);
        });
    },


    [LOGIN_JWT]: ({ commit }, creds) => {
      console.log('creds: ', creds);
      return new Promise((resolve, reject) => {
        // commit(AUTH_REQUEST);
        const formData = new FormData();
        formData.set('username', creds.username);
        formData.set('password', creds.password);  

        const url = "/api/auth/login";
        axios
        .post(url, formData, {headers: { 'Content-Type': 'multipart/form-data' }})
          .then(resp => {
            console.log('login: ', resp);
            commit('auth/SET_JWT');
            // localStorage.setItem("username", resp.data.username);


            axios
            .get('/api/auth/users/me', { headers: {'Authorization': `Bearer ${resp.data.access_token}` } })
            .then(resp => {
              console.log('users_me: ', resp);
              // dispatch("snackBar", { color: 'green', btnColor: 'white', statusText: `Welcome ${resp.data.email}`, data: {info: ''}});
              commit('AUTH_SUCCESS', resp);
              // router.push("/dashboard");
            }).catch(err => {console.log('auth users/me: ', err); })


            resolve(resp);
          
          }).catch(err => {
            console.log('err ', err);
            // commit(AUTH_ERROR, err);
            // commit("snackbar/setErr", err, { root: true });
            // localStorage.removeItem("username");
            reject(err);
          });
      });
    },





}

  export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions
  };
  