import axios from "axios";
// import { LOGIN_JWT } from '../action-types'
// import axiosHeaders from '../../main'
import { SET_STATUS, GET_USERNAME } from '../mutation-types'
import * as qs from 'qs'
// import FHIR from "fhirclient"

  const state = () => ({
    patient: [],
    username: window.localStorage.getItem("username") || "",
    accessToken: window.localStorage.getItem("accessToken") || null,
  })

  const getters = {
    isAuthenticated: state => !!state.username,
    [GET_USERNAME]: state => state.username,
    getIsActive: state => JSON.parse(state.isActive),
    getUserType: state => JSON.parse(state.isAdmin),
    getIsFirstLogin: state => JSON.parse(state.isFirstLogin),
    getEpicToken: state => state.accessToken,
    getPatientArr: state => state.patient, 
  };

  const mutations = {
    
    [SET_STATUS](state, status) { 
      state.status = status;
    },

    AUTH_CLEAR(state) {
      state.accessToken = "";
      state.refreshToken = "";
      state.username = "";
      state.status = "";
      state.isAdmin = null;
      state.isFirstLogin = null;
      state.isActive = null;
    },

    REMOVE_STORAGE() { 
      window.localStorage.removeItem("username");
      window.localStorage.removeItem("accessToken");
    },

    AUTH_SUCCESS(state, resp) {
      state.status = "success";
      
      state.username = resp.data.email;
      window.localStorage.setItem("username", resp.data.email);

      state.accessToken = resp.data.access_token;
      window.localStorage.setItem("accessToken", resp.data.access_token);
    },

    SET_JWT(state, resp) {
      console.log('SET_JWT', resp);
      state.accessToken = resp.data.access_token;
      window.localStorage.setItem("accessToken", resp.data.access_token);
      state.patient = [resp.data.patient];
      console.log('SET_JWT after: ', resp);
    },

    SET_PATIENT(state, resp) {
      console.log('SET_PATIENT', resp);
      state.patient = [resp.data];
      console.log('SET_PATIENT after: ', resp);
    },

    EMPTY(state) {
      console.log('', state);
    },
};

const actions = {

    async loginOauth({ commit }) {
      commit('EMPTY');
      // return `https://fhir.epic.com/interconnect-fhir-oauth/oauth2/authorize?response_type=code&redirect_uri=${this.redirect}&client_id=${this.clientId}&state=1234&scope=patient.read, patient.search`;

      const baseUrl = 'https://fhir.epic.com/interconnect-fhir-oauth/oauth2/authorize?response_type=code&client_id=e31e0f8f-cb86-4291-8954-504e5dab3b48&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2F%23%2Fsuccess&state=abc123&scope=patient.read, patient.search&aud=https://fhir.epic.com/interconnect-fhir-oauth/api/fhir/r4';

      // const baseUrl = 'https://fhir.epic.com/interconnect-fhir-oauth/oauth2/authorize?response_type=code&client_id=e31e0f8f-cb86-4291-8954-504e5dab3b48&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2F%23%2Fsuccess&state=abc123&scope=fhirUser&aud=https://fhir.epic.com/interconnect-fhir-oauth/api/fhir/r4';
      // const rootUrl = 'http://localhost:8080/#/launch?iss=https://fhir.epic.com/interconnect-fhir-oauth/api/fhir/r4&aud=';￼
      window.location = baseUrl;
      // this is result: use qs to parse from location on success callback url
      // http://localhost:8080/?code=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJ1cm46b2lkOmZoaXIiLCJjbGllbnRfaWQiOiJlMzFlMGY4Zi1jYjg2LTQyOTEtODk1NC01MDRlNWRhYjNiNDgiLCJlcGljLmVjaSI6InVybjplcGljOk9wZW4uRXBpYy1jdXJyZW50IiwiZXBpYy5tZXRhZGF0YSI6IktGOWNhVUlEV3FIOWVhRS1YbENxSkJlTlJBZEVtRi1neFFxeUJiVDRHX0IyUE0ybUNPWm1nMVRMeVUzVGpwSlhlamhxRGJtd21ROHRzak5LLVgzSFY3M1FpWVdoQkRMT0wxaW5VMzdqaW9QYXRMQWRCMi1hVmZtZlgxTWYweXUyIiwiZXBpYy50b2tlbnR5cGUiOiJjb2RlIiwiZXhwIjoxNjQxNDM3NTk3LCJpYXQiOjE2NDE0MzcyOTcsImlzcyI6InVybjpvaWQ6ZmhpciIsImp0aSI6ImE1MjI2OGI4LTFmNDgtNDFlZS1iYWEwLTlmYjVhMTcwMTc1YyIsIm5iZiI6MTY0MTQzNzI5Nywic3ViIjoiZWI0R2lhN0Z5aWp0UG1Ya3J0alJwUHczIn0.C9gV0aQ8iuHEwJQCdlksISY5UeSGvrgGh8LI9QxooCkUyce4Z9xjWLK2c_hUqmXGyo-a3AP_a-KjLEwJPRPu9a-lCh2KFxaCj0zgybnrQvnLMzWDSntHf-1_CiUspv9iNwCdbttl9bCOhGWkDWvGd8UTX89SruzaImNDKSaib1zL0hrMnDo5MVgI-ODAZLxwSqX8zqBVGbsymSmhZDZGPbX7YUJHKVaa_3IMRtfcXwaf_xKcMQLerkyvbtpPB3IvJII5tFTXnEXNbBI6XouuL4pdH7dQpLYXA-1jEH7sWES745H_Rgw8Rx12oNhWSc6zJ3wJ55-98a2Fk7kEWWD8Nw&state=abc123#/success
      // window.location = `${BASE_URL}?${qs.stringify(query)}`
    },


    parseLocation({ commit }, str) {
      commit('EMPTY');
      // const q = qs.parse(hash.replace('?', ''));
      let parsedURL = qs.parse(str);
      return parsedURL;
    },

    // https://fhir.epic.com/Documentation?docId=oauth2&section=Standalone-Oauth2-Launch_Access-Token-Request_No-Refresh-Tokens
    loginOauthFinal({ commit  }, str) {
      console.log('parsed: ', str);
      // const obj = dispatch('parseLocation', str);
      // const obj = qs.parse(str);
      const obj = qs.parse(str.replace('?', ''));
      console.log('obj: ', obj);
      // const q = {
      //   "grant_type": 'authorization_code',
      //   "code": obj.code,
      //   "redirect_uri": "http%3A%2F%2Flocalhost%3A8080%2F%23%2Fsuccess",
      //   "client_id": "e31e0f8f-cb86-4291-8954-504e5dab3b48",
      //   "state": obj.state
      // }
      const params = new URLSearchParams();
      params.append("grant_type", "authorization_code");
      params.append("redirect_uri", "http://localhost:8080/#/success");
      // params.append("redirect_uri", "http%3A%2F%2Flocalhost%3A8080%2F%23%2Fsuccess");
      params.append("code", obj.code);
      params.append("client_id", "e31e0f8f-cb86-4291-8954-504e5dab3b48");
      params.append("state", "abc123");

      // fhircamila
      const BASE_URL = `https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token`
      const url = BASE_URL;
      // console.log('url post: ', url)
      // return new Promise((resolve, reject) => {
      axios.post(
        url, 
        params, 
        { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } } )
          .then(resp => {
            console.log('login: ', resp);
            commit('SET_JWT', resp);
            // resolve(resp);


            axios.get(`https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/Patient/${resp.data.patient}`,
              { headers: { 'Authorization': `Bearer ${resp.data.accessToken}` } })
              .then(response => {
                commit('SET_PATIENT', response);
              // this.patientdata = response.data;
              console.log('get pateint: ', response);
            }).catch((error) => { console.log('error: ', error); });



          }).catch(err => {
            console.log('err: ', err)
            // reject(err)
          })
      // })
    },






    // finalizeForgotPassword({ }, hash) {
    //   console.log('finalize login hash: --', hash)
    //   const q = qs.parse(hash.replace('?', ''));
    //   return q.toString()
    //   // console.log('finalize login q: --', q);
    //   // const query = new URLSearchParams();

    //   // query.append("code", q.code);
    //   // query.append("state", q.state);
    //   // query.append('realmId', q.realmId)
    //   // const url = "/oauth/callback?" + query.toString();

    //   // axios
    //   // .get(`/get_auth_token/${q.code}/${q.realmId}/${q.state}`)
    //   // .then(resp => {
    //   //   console.log('tokens: --', resp)
    //   //   commit('SET_OAUTH_TOKENS', resp)
    //   // });
    // },



}

  export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions
  };
  