import { createStore } from 'vuex'
// import { createLogger } from 'vuex'
// import authJWT from './modules/authJWT'
import authStandAlone from './modules/authStandAlone'
import authBackend from './modules/authBackend'

// https://github.com/vuejs/vuex/blob/4.0/examples/classic/shopping-cart/store/index.js
// const debug = process.env.NODE_ENV !== 'production'
export default createStore({
  modules: {
    authStandAlone: authStandAlone,
    authBackend: authBackend
  },
  // strict: debug,
  // plugins: debug ? [createLogger()] : []
});