export default {

    //merge these two --. colorBoolean()?
    colorActive(item) {
      if (item == 1) return 'green'
      else return 'secondary'
    },
    floatToFixed(float) {
      return parseFloat(float).toFixed(2);
    },
    formatDate(value) {
      return new Date(value).toISOString().substr(0, 10);
    },
}


