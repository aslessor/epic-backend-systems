

export const loginOauth1 = {
    ROOT_URL: 'https://fhir.epic.com/interconnect-fhir-oauth/oauth2/authorize',
    response_type: "code",
    client_id: "e31e0f8f-cb86-4291-8954-504e5dab3b48",
    redirect_uri: "http%3A%2F%2Flocalhost%3A8080%2F%23%2Fsuccess",
    state: 'abc123',
    scope: "fhirUser",
    aud: "https://fhir.epic.com/interconnect-fhir-oauth/api/fhir/r4"

};

// "scope": "fhirUser",
// const query = {
//   "response_type": "code",
//   "redirect_uri": "http%3A%2F%2Flocalhost%3A8080%2F%23%2Fsuccess",
//   "client_id": process.env.VUE_CLIENT_ID,
//   "scope": "patient/*.read",
//   "redirect_url": "http://localhost:8080/#/success",
//   "aud": "https://fhir.epic.com/interconnect-fhir-oauth/api/fhir/r4",
// };

export const navLinks = {
    links: [
        {
            to: "/shop",
            icon: "mdi-cart-arrow-down",
            text: "Shop",
            divider: true,
        },
        {
            to: "/dashboard",
            icon: "mdi-desktop-mac-dashboard",
            text: "Dashboard",
            divider: true,
        },
        {
            to: "/order_history",
            icon: "mdi-align-horizontal-left",
            text: "Order History",
            divider: true,
        },
        // {
        //     // to: "/order_history",
        //     icon: "mdi-apps",
        //     text: "Dashboard",
        //     divider: true,
        //     subLinks: [
        //         {
        //             to: "wholesale/dashboard",
        //             icon: "mdi-desktop-mac-dashboard",
        //             text: "Wholesale Dashboard",
        //             divider: true,
        //         },
        //     ]
        // },
    ],
}

export const formConstants = {
    country_list: ['U.S.A.', 'CANADA'],
    states_list: [
        { abbr: 'AK', state: 'Alaska' },
        { abbr: 'AL', state: 'Alabama' },
        { abbr: 'AR', state: 'Arkansas' },
        { abbr: 'AS', state: 'American Samoa' },
        { abbr: 'AZ', state: 'Arizona' },
        { abbr: 'CA', state: 'California' },
        { abbr: 'CO', state: 'Colorado' },
        { abbr: 'CT', state: 'Connecticut' },
        { abbr: 'DC', state: 'District of Columbia' },
        { abbr: 'DE', state: 'Delaware' },
        { abbr: 'FL', state: 'Florida' },
        { abbr: 'GA', state: 'Georgia' },
        { abbr: 'GU', state: 'Guam' },
        { abbr: 'HI', state: 'Hawaii' },
        { abbr: 'IA', state: 'Iowa' },
        { abbr: 'ID', state: 'Idaho' },
        { abbr: 'IL', state: 'Illinois' },
        { abbr: 'IN', state: 'Indiana' },
        { abbr: 'KS', state: 'Kansas' },
        { abbr: 'KY', state: 'Kentucky' },
        { abbr: 'LA', state: 'Louisiana' },
        { abbr: 'MA', state: 'Massachusetts' },
        { abbr: 'MD', state: 'Maryland' },
        { abbr: 'ME', state: 'Maine' },
        { abbr: 'MI', state: 'Michigan' },
        { abbr: 'MN', state: 'Minnesota' },
        { abbr: 'MO', state: 'Missouri' },
        { abbr: 'MP', state: 'Northern Mariana Islands' },
        { abbr: 'MS', state: 'Mississippi' },
        { abbr: 'MT', state: 'Montana' },
        { abbr: 'NA', state: 'National' },
        { abbr: 'NC', state: 'North Carolina' },
        { abbr: 'ND', state: 'North Dakota' },
        { abbr: 'NE', state: 'Nebraska' },
        { abbr: 'NH', state: 'New Hampshire' },
        { abbr: 'NJ', state: 'New Jersey' },
        { abbr: 'NM', state: 'New Mexico' },
        { abbr: 'NV', state: 'Nevada' },
        { abbr: 'NY', state: 'New York' },
        { abbr: 'OH', state: 'Ohio' },
        { abbr: 'OK', state: 'Oklahoma' },
        { abbr: 'OR', state: 'Oregon' },
        { abbr: 'PA', state: 'Pennsylvania' },
        { abbr: 'PR', state: 'Puerto Rico' },
        { abbr: 'RI', state: 'Rhode Island' },
        { abbr: 'SC', state: 'South Carolina' },
        { abbr: 'SD', state: 'South Dakota' },
        { abbr: 'TN', state: 'Tennessee' },
        { abbr: 'TX', state: 'Texas' },
        { abbr: 'UT', state: 'Utah' },
        { abbr: 'VA', state: 'Virginia' },
        { abbr: 'VI', state: 'Virgin Islands' },
        { abbr: 'VT', state: 'Vermont' },
        { abbr: 'WA', state: 'Washington' },
        { abbr: 'WI', state: 'Wisconsin' },
        { abbr: 'WV', state: 'West Virginia', },
        { abbr: 'WY', state: 'Wyoming' },
        { abbr: 'ON', state: 'Ontario' },
        { abbr: 'AB', state: 'Alberta' },
        { abbr: 'BC', state: 'British Columbia' },
        { abbr: 'SK', state: 'Saskatchewan' },
        { abbr: 'MB', state: 'Manitoba' },
        { abbr: 'QC', state: 'Quebec' },
        { abbr: 'NL', state: 'Newfoundland Labrador' },
        { abbr: 'YT', state: 'Yukon' },
        { abbr: 'NU', state: 'Nunavut' },
        { abbr: 'NT', state: 'Northwest Territories' }
    ]
}

