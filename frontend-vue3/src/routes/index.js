import Base from '@/views/Base';
import LoginForm from '@/components/auth/LoginForm';
// import LoginFormNaive from '@/components/auth/LoginFormNaive';
import RegisterForm from '@/components/auth/RegisterForm';
import AdminLogin from '@/components/auth/AdminLogin';
import Success from '@/components/auth/Success';
import launch from '@/components/auth/standalone/launch';
// const Home = { template: `<div style='color: black'>{{ $route }}</div>` }

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Base,
    children: [
        {
          path: 'login',
          name: 'Login',
          // component: LoginFormNaive
          component: LoginForm,
        },
        {
          path: 'register',
          name: 'Register',
          component: RegisterForm
        },
        {
          path: 'launch',
          name: 'standalone-launch',
          component: launch
        },
        {
          path: 'success',
          name: 'success-redirect',
          component: Success,
          // props: route => ({ code: route.query.code }),
        },
    ]
  },
  {
    path: '/epic',
    name: 'Epic',
    component: Base,
    children: [
        {
          path: 'login',
          name: 'EpicLogin',
          component: AdminLogin
        },
    ]
  },
];

export default routes;



// const routes = [
//   {
//     path: '/',
//     name: 'Home',
//     component: Home,
//     // children: [
//     //   {
//     //     path: 'chat',
//     //     name: 'Chat',
//     //     component: ChatV1,
//     //   },
//     // ]
//   },
//   {
//     path: '/auth',
//     name: 'AuthPage',
//     component: AuthPage,
    // children: [
    //   {
    //     path: 'login',
    //     name: 'Login',
    //     component: LoginForm,
    //   },
    //   {
    //     path: 'signup',
    //     name: 'SignUp',
    //     component: SignUpForm
    //   },
    //   {
    //     path: 'reset',
    //     name: 'ChangePassword',
    //     component: ChangePassword,
    //   },
    //   {
    //     path: 'forgot-password',
    //     name: 'ForgotPassword',
    //     component: ForgotPassword,
    //   },
    //   {
    //     path: 'forgot-password-final',
    //     name: 'ForgotPasswordFinal',
    //     props: route => ({ query: route.query.q }),
    //     component: ForgotPasswordFinal,
    //     meta: {protectedRoute: true},
    //   },
    // ]
//   },
//   {
//     path: '/account',
//     name: 'Account',
//     component: Account,
//     meta: {protectedRoute: true},
//   },
//   {
//     path: '/dashboard',
//     name: 'Dashboard',
//     component: DashboardPage,
//     meta: {protectedRoute: true},
//     beforeEnter: (to, from, next) => {
//       if (from.name == 'Login') {
//         const newUser = JSON.parse( localStorage.getItem("isFirstLogin") );
//         // console.log('dashboard before enter: ', newUser);
//         if ( newUser ) {
//           next( { path: '/reset' } );
//         } else {
//           next();
//         }
//       } else {
//         next();
//       }
//     },
    
//   },

