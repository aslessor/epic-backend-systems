
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
import httpx
# from jose import jwt as jose_jwt, jws
# from jose.utils import base64url_encode, base64url_decode
from datetime import datetime, timedelta, timezone
from jwcrypto import jwk, jwt
# from jwt.utils import get_int_from_datetime
# from jwt import (
#     JWT,
#     jwk_from_dict,
#     jwk_from_pem)

class BackendAuth:

    def __init__(self):
        self.ALGO = 'RS384'
        self.exp = 1
        self.jwt_header = {
            'alg': self.ALGO,
            'typ': 'JWT'
        }
        self.headers = {}
        self.headers['Content-Type'] = 'application/x-www-form-urlencoded'

    def read_pem(self):
        file = '/home/hauwei/Dropbox/APPS/medic/fastapi-fhir-api/privatekey.pem'
        with open(file, "rb") as pemfile:
            key = jwk.JWK.from_pem(pemfile.read())
        # print(key)
        return key

    def get_token_expiry(self):
        now = datetime.now()
        current_time = int(datetime.timestamp(now))
        expired = int(datetime.timestamp(now + timedelta(minutes = self.exp)))
        return expired

    def _create_jwt(self):

        payload = {
            # Client ID for non-production
            'iss': 'ece9d0d8-ce38-4430-8b69-c00c0d065f3f',
            'sub': 'ece9d0d8-ce38-4430-8b69-c00c0d065f3f',
            'aud': 'https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token',
            'jti': 'f9eaafba-2e49-11ea-8830-5ce0c5aee612',
            'iat': int(datetime.timestamp(datetime.now())),
            'exp': self.get_token_expiry(),
        }

        token = jwt.JWT(header=self.jwt_header, claims=payload)
        key = self.read_pem()
        token.make_signed_token(key)
        tok = token.serialize()
        return tok

    def get_access_token(self):
        data = {
            'grant_type': 'client_credentials',
            'client_assertion_type': 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
            'client_assertion': self._create_jwt()}
        x = httpx.post(
            'https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token', 
            headers=self.headers, 
            data=data)
        # print(x.json())
        return x.json()

# class AuthEPIC(object):

#     def __init__(self, expiry):
#         self.expiry = expiry

#     def get_access_token(self):

#         instance = JWT()
        # payload = {
        #     # Client ID for non-production
        #     'iss': 'ece9d0d8-ce38-4430-8b69-c00c0d065f3f',
        #     'sub': 'ece9d0d8-ce38-4430-8b69-c00c0d065f3f',
        #     'aud': 'https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token',
        #     'jti': 'f9eaafba-2e49-11ea-8830-5ce0c5aee612',
        #     'iat': get_int_from_datetime(datetime.now(timezone.utc)),
        #     'exp': get_int_from_datetime(datetime.now(timezone.utc) + timedelta(minutes=self.expiry)),
        # }
        
#         with open('/home/hauwei/Dropbox/APPS/medic/fastapi-fhir-api/privatekey.pem', 'rb') as fh:
#             signing_key = jwk_from_pem(fh.read())

#         compact_jws = instance.encode(payload, signing_key, alg='RS384')
#         # print(compact_jws)

#         # headers = CaseInsensitiveDict()
        # headers = {}
        # headers['Content-Type'] = 'application/x-www-form-urlencoded'
        # # headers = {'Content-Type': 'application/x-www-form-urlencoded'}

        # data = {
        #     'grant_type': 'client_credentials',
        #     'client_assertion_type': 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
        #     'client_assertion': compact_jws
        # }
        
        # x = httpx.post(
        #     'https://fhir.epic.com/interconnect-fhir-oauth/oauth2/token', 
        #     headers=headers, 
        #     data=data)

        # return x.json()


#     def decode_jwt(self, token):
#         with open('/home/hauwei/Dropbox/APPS/medic/fastapi-fhir-api/privatekey.pem', 'rb') as fh:
#             signing_key = jwk_from_pem(fh.read())
        
#         s = jwt.decode(token, signing_key, algorithms=['RS384'])

#         # s = jws.verify(token, signing_key, algorithms=['RS384'])
#         # s = jose_jwt.decode(token, signing_key, algorithm='RS384')
#         return s