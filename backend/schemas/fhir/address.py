from fhir.resources.organization import Organization
from fhir.resources.address import Address

from fhir.resources.STU3.address import Address as Stu3Address


from pprint import pprint

data = {
    "id": "f001",
    "active": True,
    "name": "Acme Corporation",
    "address": [{"country": "Switzerland"}]
}
org = Organization(**data)

addr1 = Stu3Address()

pprint(addr1.schema())
# pprint(org.schema())
# pprint(org.json())
# pprint(org.dict())
