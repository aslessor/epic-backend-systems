from deta import Deta
from ..settings import get_settings

settings = get_settings()

conn = Deta(settings.DATABASE_URL)

def get_user_db() -> Deta.Base:
    yield conn.Base("users")