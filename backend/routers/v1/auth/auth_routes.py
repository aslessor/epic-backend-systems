from fastapi import (
    Depends, 
    HTTPException, 
    APIRouter,
    Request)

# from fastapi.security import (
    # OAuth2PasswordBearer, 
    # OAuth2PasswordRequestForm)

from starlette.status import (
    HTTP_401_UNAUTHORIZED)

from ....settings import get_settings
from ....schemas.auth.users import *
from ....deps.auth.deps import *

settings = get_settings()
# ACCESS_TOKEN_EXPIRE_MINUTES = settings.ACCESS_TOKEN_EXPIRE_MINUTES
router = APIRouter()



@router.post("/token", 
)
async def epic_backend_token(
    access_token = Depends(get_epic_access_token)
    ):
    return {"access_token": access_token, "token_type": "bearer"}





# @router.post("/token", 
# response_model=Token,
# response_description='johndoe -- secret')
# async def login_for_access_token(
#     form_data: OAuth2PasswordRequestForm = Depends(),
#     user_db = Depends(get_user_db)):

#     user = authenticate_user(
#         user_db, 
#         form_data.username, 
#         form_data.password)

#     if not user:
#         raise HTTPException(
#             status_code=HTTP_401_UNAUTHORIZED,
#             detail="Incorrect username or password",
#             headers={"WWW-Authenticate": "Bearer"},
#         )
#     access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
#     access_token = create_access_token(
#         data={"sub": user.username}, 
#         expires_delta=access_token_expires
#     )
#     return {"access_token": access_token, "token_type": "bearer"}


# @router.get("/users/me/", 
# response_model=User
# )
# async def read_users_me(
#     current_user: User = Depends(get_current_active_user),
#     ) -> User:
#     return current_user


# @router.get("/users/me/items/"
# )
# async def read_own_items(
#     current_user: User = Depends(get_current_active_user)
#     ):
#     return [{"item_id": "Foo", "owner": current_user.username}]


