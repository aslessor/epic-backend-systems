from fastapi import APIRouter, Depends, Request, Response
from typing import Optional, List
from pydantic import BaseModel, HttpUrl
import httpx
from pprint import pprint
from fhir.resources.organization import Organization
from fhir.resources.address import Address
from fhir.resources.patient import Patient

from ....schemas.fhir.r4_schema import SinglePatient, SinglePractitioner
from ....deps.auth.deps import get_single_patient

router = APIRouter()

@router.get("/read/patient", 
response_model=Patient)
def single_patient(patient = Depends(get_single_patient)):
    return patient

@router.get("/read/patients", 
    response_model=List[SinglePatient]
    )
async def list_patients():
    r = httpx.get('https://hapi-fhir.marc-hi.ca:8443/fhir/Patient').json()['entry']
    return [SinglePatient(**i) for i in r]


@router.get("/read/practitoners", 
    response_model=List[SinglePractitioner]
    )
async def list_practitioner():
    r = httpx.get('https://hapi-fhir.marc-hi.ca:8443/fhir/Practitioner').json()['entry']
    return [SinglePractitioner(**i) for i in r]


# @router.get("/read/organization", 
# response_model=Organization)
# async def read_r4_organization() -> Organization:
#     return Read_R4_Organization(**organization_data)


# @router.get("/read/address", 
# response_model=Read_R4_Address)
# async def read_r4_address() -> Read_R4_Address:
#     return Read_R4_Address(**organization_data)
