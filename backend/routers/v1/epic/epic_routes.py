from fastapi import APIRouter, Depends, Request, Response
from typing import Optional
from pydantic import BaseModel, HttpUrl
from pprint import pprint
# from .....schemas.fhir.r4_schema import *
from ....deps.auth.deps import get_epic_data
from fhir.resources.patient import Patient

router = APIRouter()

# @router.get("/read/patient", 
# response_model=Patient
# )
# def read_r4_patient(patient = Depends(get_patient)):
#     return patient


@router.get("/read/patient",
    response_model=Patient
)
async def read_patient(request: Request):
    token = request.headers['Authorization']
    print('token: ', token)
    print('--' * 42)
    r = await get_data(token)
    print('r: ', r)
    return r
    # return Patient(**r)

    # headers = CaseInsensitiveDict()
    # headers["Accept"] = "application/json"
    # headers["Authorization"] = f"Bearer {token}"

    # r = httpx_get(
    #     'https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/Patient/erXuFYUfucBZaryVksYEcMg3',

    # pprint(r)
    # return Patient(**r)


# @router.get("/read/organization", 
# response_model=Read_R4_Organization)
# async def read_r4_organization() -> Read_R4_Organization:
#     return Read_R4_Organization(**organization_data)


# @router.get("/read/address", 
# response_model=Read_R4_Address)
# async def read_r4_address() -> Read_R4_Address:
#     return Read_R4_Address(**organization_data)

