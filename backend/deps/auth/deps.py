from datetime import datetime, timedelta
from typing import Optional
import httpx
from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from starlette.status import (
    HTTP_401_UNAUTHORIZED,
    HTTP_400_BAD_REQUEST
)
from jose import JWTError, jwt
from passlib.context import CryptContext

from ...utils import *
from ...schemas.auth.users import *
from ...db.deta_db import get_user_db
from ...settings import get_settings
from fhir.resources.patient import Patient


settings = get_settings()
SECRET = settings.SECRET
ALGORITHM = settings.ALGORITHM

def get_epic_access_token():
    auth = BackendAuth()
    tok = auth.get_access_token()
    print('tok:  ', tok)
    return tok['access_token']


async def get_patient(
    token: str = Depends(get_epic_access_token)):
    
    headers = {
        'Accept': 'application/json', 
        'Authorization': f"Bearer {token}"}
    
    # epic_on_fhor_r4 = f'https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/{model}/{uid}'

    r = httpx.get(
        'https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/Patient/erXuFYUfucBZaryVksYEcMg3',
        headers=headers)
    return r.json()


async def get_epic_data(token: str):
    headers = {
        'Accept': 'application/json', 
        'Authorization': f"{token}"}
    r = httpx.get(
        'https://fhir.epic.com/interconnect-fhir-oauth/api/FHIR/R4/Patient/erXuFYUfucBZaryVksYEcMg3',
        headers=headers)
    return r.json()


async def get_single_patient():
    r = httpx.get('https://hapi-fhir.marc-hi.ca:8443/fhir/Patient/227394').json()
    return Patient(**r)
